<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NotificationPreference extends Model
{
    use HasFactory;

    protected $fillable = [
        'app_user_id',
        'pref_one_week',
        'pref_four_days',
        'pref_one_day',
        'pref_two_hours',
    ];


    public function user()
    {
        return $this->belongsTo(AppUser::class, 'app_user_id');
    }
}
