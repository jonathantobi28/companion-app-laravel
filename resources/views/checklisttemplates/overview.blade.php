@extends('layout')

@section('title', 'Checklist Templates')

@section('breadcrumbs')
    @parent
    <li class="breadcrumb-item active" aria-current="page">Checklist Templates</li>
@endsection

@section('content')
<h2 class="intro-y text-lg font-medium mt-10">
    Checklist Templates
</h2>
<div class="grid grid-cols-12 gap-6 mt-5 mb-10">
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
        <a href="{{ route('checklisttemplates.create') }}" class="btn btn-primary shadow-md mr-2">New Checklist Template</a>
    </div>
</div>

<div class="grid grid-cols-12 gap-6 mt-5">
    @if ($checklisttemplates->isEmpty())
        <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
            <div class="font-normal">No Checklist Templates Currently</div>
        </div>
    @else
    <!-- BEGIN: Data List -->
    <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
        <table class="table table-report -mt-2">
            <thead>
                <tr>
                    <th class="whitespace-nowrap">Name</th>
                    <th class="text-center whitespace-nowrap">ACTIONS</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($checklisttemplates as $checklisttemplate)
                    <tr class="intro-x">
                        <td>
                            <span class="font-medium whitespace-nowrap">{{ $checklisttemplate->name }}</span> 
                        </td>
                        <td class="table-report__action w-56">
                            <div class="flex justify-center items-center">
                                <a class="flex items-center mr-3" href="{{ route('checklisttemplates.edit', $checklisttemplate->id) }}"> 
                                    <i data-lucide="check-square" class="w-4 h-4 mr-1"></i> Edit 
                                </a>
                                <button class="flex items-center text-danger" data-tw-toggle="modal" data-tw-target="#deleteModal" onclick="prepareDeletion(event, {{$checklisttemplate->id}})"> 
                                    <i data-lucide="trash-2" class="w-4 h-4 mr-1"></i> Delete
                                </button>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    @endif
</div>
<!-- END: Content -->

  <!-- BEGIN: Delete Confirmation Modal -->
  <div id="deleteModal" class="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body p-0">
                <div class="p-5 text-center">
                    <i data-lucide="x-circle" class="w-16 h-16 text-danger mx-auto mt-3"></i> 
                    <div class="text-3xl mt-5">Are you sure?</div>
                    <div class="text-slate-500 mt-2">
                        Are you sure you want to delete this template?
                        <br>
                        This action cannot be undone
                    </div>
                </div>
                <div class="px-5 pb-8 text-center">
                    <button type="button" data-tw-dismiss="modal" class="btn btn-outline-secondary w-24 mr-1">Cancel</button>
                    <form method="post" action="#" id="deleteForm" class="w-24 inline-flex">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Delete Confirmation Modal --> 

<script>
    function prepareDeletion(event, id){
        event.preventDefault();
        document.getElementById('deleteForm').action =`checklisttemplates/${id}/delete`;
    }
</script>


@endsection