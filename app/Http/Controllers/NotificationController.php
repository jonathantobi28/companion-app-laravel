<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use Carbon\Carbon;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    //
    public function getNotifications()
    {
        $user = auth()->user();

        $notifications = Notification::where('is_read', false)
                             ->where('app_user_id', $user->id)
                             ->get()
                             ->map(function ($notification) {
                                 return [
                                     'id' => $notification->id,
                                     'title' => $notification->title,
                                     'body' => $notification->body,
                                     'type' => $notification->type,
                                     'is_read' => $notification->is_read,
                                     'sent' => $notification->created_at->diffForHumans()
                                 ];
                             })
                             ->values()
                             ->toArray();

        $readNotifications = Notification::where('is_read', true)
                                        ->where('app_user_id', $user->id)
                                        ->get()
                                        ->map(function ($notification) {
                                            return [
                                                'id' => $notification->id,
                                                'title' => $notification->title,
                                                'body' => $notification->body,
                                                'type' => $notification->type,
                                                'is_read' => $notification->is_read,
                                                'sent' => $notification->created_at->diffForHumans()
                                            ];
                                        })
                                        ->values()
                                        ->toArray();
    
        return response()->json([
            'notifications' => $notifications,
            'readNotifications' => $readNotifications
        ]);
    }

    public function setNotificationRead($id)
    {
        // Find the notification by ID
        $notification = Notification::find($id);

        if (!$notification) {
            return response()->json(['message' => 'Notification not found'], 404);
        }

        // Mark the notification as read
        $notification->is_read = true;
        $notification->save();

        return response()->json(['message' => 'Notification marked as read']);
    }
}
