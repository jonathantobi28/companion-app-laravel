@extends('layout')

@section('title', 'New Flight')

@section('styles')
    @parent
    <style>
        .file-input-container{
            position: relative;
            width: 100%;
            border: 1px dashed #B1B3B6;
            display: flex;
            padding: 2em;
            align-items: flex-end;
            cursor: pointer;
            border-radius: 4px;
            height: 270px;
            justify-content: center;
            overflow: hidden;
            max-width: 480px;
        }
        .file-input-container.dragover{
            border-color: var(--primary);
        }
        .file-input-container .file-preview{
            position: absolute;
            width: 100%;
            height: 100%;
            z-index: 1;
            top: 0;
        }
        .file-input-container .file-preview img{
            width: 100%;
            height: 100%;
            object-fit: cover;
        }
        .file-input-container .file-preview .delete-button{
            position: absolute;
            top: 10px;
            right: 10px;
        }
        .file-input-container .file-preview .delete-button svg{
            stroke: #fefefe;
        }
    </style>
@endsection

@section('breadcrumbs')
    @parent
    <li class="breadcrumb-item"><a href="{{route('flights.overview')}}">Flights</a></li>
    <li class="breadcrumb-item active" aria-current="page">New</li>
@endsection

@section('content')
<form id="flight-form" method="post" action="{{route('flights.store')}}" enctype="multipart/form-data">
    @csrf
    <div class="grid grid-cols-12 gap-6 mt-10">
        <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
            <h2 class="intro-y text-lg font-medium">
                Add New Flight
            </h2>
            <button class="btn btn-primary shadow-md ml-auto" type="submit">Save</button>
        </div>                      
    </div>
    <div class="grid grid-cols-12 gap-5 mt-5">
        <!-- BEGIN: Post Content -->

        <div class="col-span-4 grid grid-cols-12">
            <div class="col-span-12 flex flex-column box p-5">
                <div class="mb-5">
                    <label for="type" class="form-label">User</label> 
                    <select class="tom-select" aria-label="User select" id="user-select" name="app_user">
                        @foreach ($users as $user)
                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                        @endforeach
                    </select>
                    @error('user')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="mb-5">
                    <label for="type" class="form-label">Flight Type</label> 
                    <select class="form-select form-control block mb-1" aria-label="Flight Type select" id="flight-type-select" name="flight_type">
                        <option value="one-way">One Way</option>
                        <option value="round-trip">Round Trip</option>
                    </select>
                    @error('flight_type')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="mb-5">
                    <label for="type" class="form-label">Airline</label> 
                    <select class="form-select form-control block mb-1" aria-label="Airline select" id="airline-select" name="airline">
                        @foreach ($airlines as $airline)
                            <option value="{{ $airline->id }}">{{ $airline->name }}</option>
                        @endforeach
                    </select>
                    @error('airline')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>

            {{-- <div class="col-span-12 flex flex-column box p-5 mt-5">
                <label for="type" class="form-label">Ticket</label> 
                <input type="file" id="image-input" name="ticket" style="display: none;" value="{{ old('ticket') }}" accept="image/*">
                <div id="image-dropzone" class="file-input-container">
                    <div id="preview-container" class="file-preview"></div>
                    <span class="text-slate-500">Drop file here or click to upload</span>
                </div>
                @error('ticket')
                    <div class="text-danger">{{ $message }}</div>
                @enderror
            </div> --}}

        </div>

        <div class="col-span-8 grid grid-cols-12">    
            <div class="col-span-12 box p-5" id="departing-flight-fields">
                <p class="font-medium mt-2 mb-3">Departing Flight</p>
                <div class="grid grid-cols-12 gap-4 gap-y-3">
                    <div class="col-span-6"> 
                        <div class="col-span-12 mb-3">
                            <label for="date" class="form-label">From</label>
                            <select name="departure_city" id="departure_city" data-placeholder="Select Departing City" class="tom-select w-full">
                                @foreach ($countries as $country)
                                    <optgroup label="{{ $country->name }}">
                                        @foreach ($country->cities as $city)
                                            <option value="{{ $city->id }}">{{ $city->name }}</option>
                                        @endforeach
                                    </optgroup>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-span-12 mb-3"> 
                            <label for="date" class="form-label">Departure Date</label> 
                            <input type="text" class="datepicker form-control block" data-single-mode="true" name="departure_date">  
                        </div>

                        <div class="col-span-12 mb-3">
                            <label for="date" class="form-label">Departure Time</label> 
                            <div class="relative timepicker" id="departure_time" data-te-input-wrapper-init>
                                <input type="text" class="form-control block rounded border bg-transparent px-3 py-[0.32rem] leading-[1.6] outline-none [&:not([data-te-input-placeholder-active])]:placeholder:opacity-0" id="form2" name="departure_time"/>
                            </div>
                        </div>
                    </div>

                    <div class="col-span-6"> 
                        <div class="col-span-12 mb-3">
                            <label for="date" class="form-label">To</label>
                            <select name="arrival_city" id="arrival_city" data-placeholder="Select Arrival City" class="tom-select w-full">
                                @foreach ($countries as $country)
                                    <optgroup label="{{ $country->name }}">
                                        @foreach ($country->cities as $city)
                                            <option value="{{ $city->id }}">{{ $city->name }}</option>
                                        @endforeach
                                    </optgroup>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-span-12 mb-3"> 
                            <label for="date" class="form-label">Arrival Date</label> 
                            <input type="text" class="datepicker form-control block" data-single-mode="true" name="arrival_date">  
                        </div>

                        <div class="col-span-12 mb-3">
                            <label for="date" class="form-label">Arrival Time</label> 
                            <div class="relative timepicker" id="arrival_time" data-te-input-wrapper-init>
                                <input type="text" class="form-control block rounded border bg-transparent px-3 py-[0.32rem] leading-[1.6] outline-none [&:not([data-te-input-placeholder-active])]:placeholder:opacity-0" id="form2" name="arrival_time"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-span-4 grid grid-cols-12">
            <div class="col-span-12 flex flex-column box p-5">
                <label for="type" class="form-label">Ticket</label> 
                <input type="file" id="image-input" name="ticket" style="display: none;" value="{{ old('ticket') }}" accept="image/*">
                <div id="image-dropzone" class="file-input-container">
                    <div id="preview-container" class="file-preview"></div>
                    <span class="text-slate-500">Drop file here or click to upload</span>
                </div>
                @error('ticket')
                    <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>

        <div class="col-span-8 grid grid-cols-12">    
            <div class="col-span-12 hidden box p-5" id="return-flight-fields">
                <p class="font-medium mt-2 mb-3">Return Flight</p>
                <div class="grid grid-cols-12 gap-4 gap-y-3">
                    <div class="col-span-6"> 
                        <div class="col-span-12 mb-3">
                            <label for="date" class="form-label">From</label>
                            <select name="return_departure_city" id="return_departure_city" data-placeholder="Select Departing City" class="tom-select w-full">
                                @foreach ($countries as $country)
                                    <optgroup label="{{ $country->name }}">
                                        @foreach ($country->cities as $city)
                                            <option value="{{ $city->id }}">{{ $city->name }}</option>
                                        @endforeach
                                    </optgroup>
                                @endforeach
                            </select>
                        </div>
    
                        <div class="col-span-12 mb-3"> 
                            <label for="date" class="form-label">Departure Date</label> 
                            <input type="text" class="datepicker form-control block" data-single-mode="true" name="return_departure_date">  
                        </div>
    
                        <div class="col-span-12 mb-3">
                            <label for="date" class="form-label">Departure Time</label> 
                            <div class="relative timepicker" id="return_departure_time" data-te-input-wrapper-init>
                                <input type="text" class="form-control block rounded border bg-transparent px-3 py-[0.32rem] leading-[1.6] outline-none [&:not([data-te-input-placeholder-active])]:placeholder:opacity-0" id="form2" name="return_departure_time"/>
                            </div>
                        </div>
                    </div>
    
                    <div class="col-span-6"> 
                        <div class="col-span-12 mb-3">
                            <label for="date" class="form-label">To</label>
                            <select name="return_arrival_city" id="return_arrival_city" data-placeholder="Select Arrival City" class="tom-select w-full">
                                @foreach ($countries as $country)
                                    <optgroup label="{{ $country->name }}">
                                        @foreach ($country->cities as $city)
                                            <option value="{{ $city->id }}">{{ $city->name }}</option>
                                        @endforeach
                                    </optgroup>
                                @endforeach
                            </select>
                        </div>
    
                        <div class="col-span-12 mb-3"> 
                            <label for="date" class="form-label">Arrival Date</label> 
                            <input type="text" class="datepicker form-control block" data-single-mode="true" name="return_arrival_date">  
                        </div>
    
                        <div class="col-span-12 mb-3">
                            <label for="date" class="form-label">Arrival Time</label> 
                            <div class="relative timepicker" id="return_arrival_time" data-te-input-wrapper-init>
                                <input type="text" class="form-control block rounded border bg-transparent px-3 py-[0.32rem] leading-[1.6] outline-none [&:not([data-te-input-placeholder-active])]:placeholder:opacity-0" id="form2" name="return_arrival_time"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        



        </div>
        <!-- END: Post Content -->
    </div>  
</form>
@endsection

@section('scripts')
    @parent
    <script>
        const csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
        const fileContainer = document.getElementById('image-dropzone');
        const fileInput = document.getElementById('image-input');
        const previewContainer = document.getElementById('preview-container');
        let temporaryFileUrl = null;
        let prevTempURL = null;


        function adjustAspectRatio() {
            fileContainer.style.height = `${fileContainer.offsetWidth * (9 / 16)}px`;
        }
        // Call the function on initial load and whenever the window is resized
        window.addEventListener('load', adjustAspectRatio);
        window.addEventListener('resize', adjustAspectRatio);

        fileContainer.addEventListener('click', () => {
            fileInput.click();
        });

        fileContainer.addEventListener('dragenter', (e) => {
            e.preventDefault();
            fileContainer.classList.add('dragover');
        });

        fileContainer.addEventListener('dragover', (e) => {
            e.preventDefault();
        });

        fileContainer.addEventListener('dragleave', () => {
            fileContainer.classList.remove('dragover');
        });

        fileContainer.addEventListener('drop', (e) => {
            e.preventDefault();
            fileContainer.classList.remove('dragover');
            // Handle the dropped file
            const file = e.dataTransfer.files[0];
            handleFile(file);
        });

        fileInput.addEventListener('change', (e) => {
            e.preventDefault();
            // Handle the selected file
            const file = e.target.files[0];
            handleFile(file);
        });

        function handleFile(file) {
            handleDelete(temporaryFileUrl);

            // Perform desired actions with the file
            if (file) {
                const reader = new FileReader();
                reader.onload = (event) => {
                    // Create a new FileList object with the selected or dropped file
                    const fileList = new DataTransfer();
                    fileList.items.add(file);

                    // Set the newly created FileList object as the value of the file input
                    fileInput.files = fileList.files;
                }
                reader.readAsDataURL(file);

                const formData = new FormData();
                formData.append('file', file);

                // Make an AJAX request to upload the file and get the temporary URL
                fetch("{{route('banners.temporary.store')}}", {
                    method: 'POST',
                    body: formData,
                    headers: {
                        'X-CSRF-TOKEN': csrfToken,
                    },
                })
                .then((response) => response.json())
                .then((data) => {
                    // Store the temporary URL
                    temporaryFileUrl = data.url;

                    // Display the preview using the temporary URL
                    const filePreview = document.createElement('div');
                    filePreview.classList.add('preview-item');
                    filePreview.innerHTML = `
                        <img src="${temporaryFileUrl}" class="preview-image">
                        <button class="delete-button"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="lucide lucide-trash-2"><path d="M3 6h18"/><path d="M19 6v14c0 1-1 2-2 2H7c-1 0-2-1-2-2V6"/><path d="M8 6V4c0-1 1-2 2-2h4c1 0 2 1 2 2v2"/><line x1="10" x2="10" y1="11" y2="17"/><line x1="14" x2="14" y1="11" y2="17"/></svg></button>
                    `;
                    previewContainer.innerHTML = '';
                    previewContainer.appendChild(filePreview);

                    const deleteButton = filePreview.querySelector('.delete-button');
                    deleteButton.addEventListener('click', (e) => {
                        e.stopPropagation();
                        handleDelete(temporaryFileUrl);

                        // Remove the preview and reset the temporary URL to make it feel instant
                        temporaryFileUrl = null;
                        previewContainer.innerHTML = '';
                    });
                })
                .catch((error) => {
                    console.error('Error uploading file:', error);
                });
            }
        }

        async function handleDelete(file){
            try {
                const response = await fetch("{{route('banners.temporary.delete')}}", {
                    method: 'DELETE',
                    body: JSON.stringify({ url: file }),
                    headers: {
                    'Content-Type': 'application/json',
                    'X-CSRF-TOKEN': csrfToken,
                    },
                });
                
            } catch (error) {
                console.error('An error occurred while deleting the file:', error);  
            }
        }

        document.addEventListener('DOMContentLoaded', function() {
            const flightTypeSelect = document.getElementById('flight-type-select');
            const returnFlightFields = document.getElementById('return-flight-fields');

            const departureTime = document.querySelector("#departure_time");
            const departureTimeMaxMin = new te.Timepicker(departureTime, { format12: true,
            inline: true, });

            const arrivalTime = document.querySelector("#arrival_time");
            const arrivalTimeMaxMin = new te.Timepicker(arrivalTime, { format12: true,
            inline: true, });

            const returnDepartureTime = document.querySelector("#return_departure_time");
            const returnDepartureTimeMaxMin = new te.Timepicker(returnDepartureTime, { format12: true,
            inline: true, });

            const returnArrivalTime = document.querySelector("#return_arrival_time");
            const retrurnArrivalTimeMaxMin = new te.Timepicker(returnArrivalTime, { format12: true,
            inline: true, });

            // Function to toggle visibility of return flight fields
            function toggleReturnFlightFields() {
                if (flightTypeSelect.value === 'round-trip') {
                    returnFlightFields.classList.remove('hidden');
                } else {
                    returnFlightFields.classList.add('hidden');
                }
            }

            // Add event listener to flight type select
            flightTypeSelect.addEventListener('change', toggleReturnFlightFields);

        });

    </script>
@endsection