@extends('backoffice.layout')

@section('title', 'Nieuwe Vacature')

@section('breadcrumbs')
    @parent
    <li class="breadcrumb-item"><a href="{{route('backoffice.vacancies.overview')}}">Vacatures</a></li>
    <li class="breadcrumb-item active" aria-current="page">Nieuw</li>
@endsection

@section('content')
<form method="post" action="{{route('backoffice.vacancies.create')}}">
    @csrf
    <div class="intro-y flex flex-col sm:flex-row items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">
            Voeg Nieuwe Vacature Toe
        </h2>
        <div class="w-full sm:w-auto flex mt-4 sm:mt-0">
            <button class="dropdown-toggle btn btn-primary shadow-md flex items-center" aria-expanded="false" data-tw-toggle="dropdown" type="submit"> Save </button>
        </div>
    </div>
    <div class="pos intro-y grid grid-cols-12 gap-5 mt-5">
        <!-- BEGIN: Post Content -->
        <div class="intro-y col-span-12 box p-5">
            <div>
                <label for="title" class="form-label">Titel</label>
                <input type="text" id="title" name="title" class="form-control w-full" placeholder="Titel" value="{{ old('title') }}" required >
                @error('title')
                    <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="mt-3">
                <label for="slug" class="form-label">Slug</label>
                <input type="text" id="slug" name="slug" class="form-control w-full" placeholder="Slug" value="{{ old('slug') }}" required>
                @error('slug')
                    <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="mt-5">
                <label>Actieve Status</label>
                <div class="form-switch mt-2">
                    <input type="checkbox" name="active" class="form-check-input" @if(old('active')) checked @endif>
                </div>
            </div>

            <div class="mt-5">
                <label>Intro</label>
                <div class="mt-2">
                    <textarea id="intro" class="editor" name="intro">
                        {!! old('intro') !!}
                    </textarea>
                    @error('intro')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>

            <div class="mt-5">
                <label>Body</label>
                <div class="mt-2">
                    <textarea id="body" class="editor" name="body">
                        {!! old('body') !!}
                    </textarea>
                    @error('body')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            
            
        </div>
        <!-- END: Post Content -->
    </div>  
</form>
@endsection

@section('scripts')
    @parent
    <script src="{{asset('dist/js/ckeditor-classic.js')}}"></script>

    <script>
        document.querySelector('input#title').onkeyup = function(){
            const kebabCase = this.value
                    .replace(/[^a-zA-Z ]/g, "")
                    .replace(/([a-z])([A-Z])/g, "$1-$2")
                    .replace(/[\s_]+/g, '-')
                    .toLowerCase();
            
                    console.log(kebabCase);

            document.querySelector('input#slug').value = kebabCase;
        }
    </script>
@endsection