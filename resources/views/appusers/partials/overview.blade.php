@foreach ($appusers as $appuser)
    <tr class="intro-x">
        <td>
            <span class="font-medium whitespace-nowrap">{{$appuser->name}}</span>   
        </td>
        <td>
            <span class="font-medium whitespace-nowrap">{{$appuser->email}}</span>   
        </td>
        <td class="w-40">
            @if ($appuser->active)
                <div class="flex items-center justify-center text-success"> 
                        Active 
                </div>
            @else
                <div class="flex items-center justify-center text-danger"> 
                    Inactive 
                </div>
            @endif
        </td>
        <td class="table-report__action w-56">
            <div class="flex justify-center items-center">
                <a class="flex items-center mr-3" href="{{ route('appusers.show', $appuser->id) }}"> 
                    <i data-lucide="eye" class="w-4 h-4 mr-1"></i> View 
                </a>
            </div>
        </td>
    </tr>
@endforeach