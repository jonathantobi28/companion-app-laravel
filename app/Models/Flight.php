<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Flight extends Model
{
    use HasFactory;

    protected $fillable = [
        'app_user_id',
        'flight_type',
        'airline_id',

        'departure_city',
        'departure_date',
        'departure_time',

        'arrival_city',
        'arrival_date',
        'arrival_time',

        'return_departure_city',
        'return_departure_date',
        'return_departure_time',

        'return_arrival_city',
        'return_arrival_date',
        'return_arrival_time',
    ];

    public function app_user()
    {
        return $this->belongsTo(AppUser::class);
    }

    public function airline()
    {
        return $this->belongsTo(Airline::class);
    }

    public function departure_city()
    {
        return $this->belongsTo(City::class, 'departure_city_id');
    }

    public function arrival_city()
    {
        return $this->belongsTo(City::class, 'arrival_city_id');
    }

    public function return_departure_city()
    {
        return $this->belongsTo(City::class, 'return_departure_city_id');
    }

    public function return_arrival_city()
    {
        return $this->belongsTo(City::class, 'return_arrival_city_id');
    }

    public function appointments()
    {
        return $this->hasMany(Appointment::class);
    }

    public function checklists()
    {
        return $this->morphMany(Checklist::class, 'checklistable');
    }

    public function scopeUpcoming($query)
    {
        return $query->where(function ($query) {
            $query->where('departure_date', '>', now())
                ->orWhere(function ($query) {
                    $query->where('departure_date', '=', now())
                        ->where('departure_time', '>', now()->format('H:i:s'));
                });
        });
    }

    public function scopePast($query)
    {
        return $query->where(function ($query) {
            $query->where('departure_date', '<', now())
                ->orWhere(function ($query) {
                    $query->where('departure_date', '=', now())
                        ->where('departure_time', '<=', now()->format('H:i:s'));
                });
        });
    }
}
