@extends('emails.layout')

@section('content')

<h1 style="text-align: center;color:#1d1d1d">Your Password has changed</h1>
<p style="text-align: center;color:#1d1d1d">Remember to use your new password the next time you want to log in to your account.</p>

<p><small style="text-align: center;color:#1d1d1d;">If you didn't make this change, please get in touch straight away</small></p>
@endsection