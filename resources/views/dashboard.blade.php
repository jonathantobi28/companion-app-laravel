@extends('layout')

@section('title', 'Dashboard')

@section('breadcrumbs')
    @parent
    <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
@endsection

@section('content')
<h2 class="intro-y text-lg font-medium mt-10">Dashboard</h2>
@endsection