// Load plugins
import helper from "./helper";
import * as Popper from "@popperjs/core";
import dom from "@left4code/tw-starter/dist/js/dom";
import Toastify from "toastify-js";
import TomSelect from 'tom-select';

// Set plugins globally
window.helper = helper;
window.Popper = Popper;
window.$ = dom;
window.Toastify = Toastify;
window.TomSelect = TomSelect;
