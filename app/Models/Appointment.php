<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    use HasFactory;

    protected $fillable = [
        'app_user_id',
        'location_id',
        'date',
        'time',
        'flight_id',
    ];

    public function flight()
    {
        return $this->belongsTo(Flight::class);
    }

    public function appUser()
    {
        return $this->belongsTo(AppUser::class);
    }

    public function location()
    {
        return $this->belongsTo(Location::class, 'location_id');
    }


    public function checklists()
    {
        return $this->morphMany(Checklist::class, 'checklistable');
    }

    public function scopeUpcoming($query)
    {
        return $query->where(function ($query) {
            $query->where('date', '>', now())
                ->orWhere(function ($query) {
                    $query->where('date', '=', now())
                        ->where('time', '>', now()->format('H:i:s'));
                });
        });
    }

    public function scopePast($query)
    {
        return $query->where(function ($query) {
            $query->where('date', '<', now())
                ->orWhere(function ($query) {
                    $query->where('date', '=', now())
                        ->where('time', '<=', now()->format('H:i:s'));
                });
        });
    }
}
