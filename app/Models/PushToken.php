<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PushToken extends Model
{
    use HasFactory;

    protected $fillable = ['app_user_id', 'token'];

    public function appUser()
    {
        return $this->belongsTo(AppUser::class);
    }

    // Update the last response timestamp
    public function updateLastResponse()
    {
        $this->update(['last_response' => now()]);
    }
}
