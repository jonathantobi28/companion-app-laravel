@extends('emails.layout')

@section('content')

<h1 style="text-align: center;color:#1d1d1d">Your Password Reset Code</h1>
<p style="text-align: center;color:#1d1d1d">Please use the following verification code to reset your password:</p>
<h2 style="text-align: center;color:#1d1d1d;font-size:16px;">{{ $code }}</h2>

<p><small style="text-align: center;color:#1d1d1d;">This code will expire in 60 minutes. If you did not request a password reset you can ignore this mail. Do not send this code to anyone. We will never ask you for any code you received.</small></p>
@endsection