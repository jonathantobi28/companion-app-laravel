@extends('layout')

@section('title', 'Flight - ' . $flight->app_user->name)

@section('breadcrumbs')
    @parent
    <li class="breadcrumb-item"><a href="{{route('flights.overview')}}">Flights</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ $flight->app_user->name }}</li>
@endsection

@section('content')

<div class="grid grid-cols-12 gap-6 mt-5">
    <div class="col-span-12 lg:col-span-8 2xl:col-span-9">
        <div>
            <div class="intro-y mt-5 mb-10 lg:mt-0">
                <div class="intro-y flex items-center mt-5 mb-3">
                    <h2 class="text-lg font-medium mr-auto">
                        Appointments
                    </h2>
                    <a href="#" class="ml-auto text-primary truncate flex items-center" data-tw-toggle="modal" data-tw-target="#appointmentModal" >
                        <i data-lucide="plus" class="w-4 h-4 mr-1"></i> New Appointment
                    </a>
                </div>
                @if ($appointments->isEmpty())
                    <div class="p-5 box mb-3">
                        <p>No appointments scheduled</p>
                    </div>
                @else
                    @foreach ($appointments as $appointment)
                        <div class="py-3 px-5 box flex flex-row justify-between mb-3">
                            <div>
                                <p class="font-medium">{{ $appointment->location->name }}</p>
                                <div class="text-slate-500">{{ $appointment->location->address }}</div>
                            </div>
                            <div class="text-right">
                                <div class="font-medium">{{ date('j M Y', strtotime($appointment->date)) }}</div>
                                <div class="text-slate-500">{{ date('h:i A', strtotime($appointment->time)) }}</div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>

        <div>
            <div class="intro-y flex items-center mt-5 mb-3">
                <h2 class="text-lg font-medium mr-auto">
                    Checklists
                </h2>
                <a href="#" class="ml-auto text-primary truncate flex items-center" data-tw-toggle="modal" data-tw-target="#checklistModal" >
                    <i data-lucide="plus" class="w-4 h-4 mr-1"></i> New Checklist
                </a>
            </div>
            @if ($checklists->isEmpty())
                <div class="p-5 box mb-3">
                    <p>No checklists added</p>
                </div>
            @else
                @foreach ($checklists as $checklist)
                    <a href="{{ route('checklists.edit', $checklist->id) }}" class="py-3 px-5 box flex flex-row justify-between zoom-in mb-3">
                        <div class="">
                            <div class="font-medium">{{ $checklist->name }}</div>
                        </div>
                    </a>
                @endforeach
            @endif
        </div>
        
    </div>

    <div class="col-span-12 lg:col-span-4 2xl:col-span-3">
        <div class="intro-ymt-5 mb-5 lg:mt-0">
            <div class="intro-y flex items-center mt-5 mb-3">
                <h2 class="text-lg font-medium mr-auto">
                    Personal information
                </h2>
            </div>
            <div class="p-5 box">
                <div class="relative flex items-center mb-5">
                    <div class="w-12 h-12 image-fit">
                        @if ($flight->app_user->thumbnail_URI)
                            <img src="{{ asset( $flight->app_user->thumbnail_URI) }}" class="rounded-full" alt="User Thumbnail">
                        @else
                            <img src="{{ asset('/dist/images/avatar.png') }}" class="rounded-full" alt="Thumbnail Placeholder">
                        @endif
                    </div>
                    <div class="ml-4 mr-auto">
                        <div class="font-medium text-base">{{ $flight->app_user->name }}</div>
                        @if ($flight->app_user->active)
                            <span class="text-success">Active</span>
                        @else
                            <span class="text-danger">Inactive</span>
                        @endif
                    </div>
                </div>
                <div>
                    <div class="font-medium text-center lg:text-left lg:mt-3 mb-1">Contact Details</div>
                    <p>{{ $flight->app_user->email }}</p>
                </div>
            </div>
        </div>
        
        <div class="intro-ymt-5 lg:mt-0">
            <div class="intro-y flex items-center mt-5 mb-3">
                <h2 class="text-lg font-medium mr-auto">
                    Flight information
                </h2>
            </div>
            <div class="p-5 box">
                <div class="flex justify-between mb-5">
                    <div>
                        <small class="text-slate-500">Airline</small>
                        <div class="font-medium">{{ $flight->airline->name }}</div>
                        <div class="text-slate-500">{{ $flight->type}}</div>
                    </div>
                    {{-- <div>
                        <a href="#" class="btn btn-sm btn-primary shadow-md">Edit</a>
                    </div> --}}
                </div>

                <div class="mb-2 flex justify-between">
                    <div class="text-left">
                        <small class="text-slate-500">Departure</small>
                        <div class="font-medium">{{ date('j M Y', strtotime($flight->departure_date)) }}</div>
                        <div class="text-slate-500">{{ date('H:i A', strtotime($flight->departure_time)) }}</div>
                        <small class="text-slate-500">{{ $flight->departure_city->name}}</small>
                    </div>

                    <div class="text-right">
                        <small class="text-slate-500">Arrival</small>
                        <div class="font-medium">{{ date('j M Y', strtotime($flight->arrival_date)) }}</div>
                        <div class="text-slate-500">{{ date('H:i A', strtotime($flight->arrival_time)) }}</div>
                        <small class="text-slate-500">{{ $flight->arrival_city->name}}</small>
                    </div>
                </div>

                @if($flight->return_departure_date)
                    <div class="mb-2 flex justify-between">
                        <div class="text-left">
                            <small class="text-slate-500">Return Departure</small>
                            <div class="font-medium">{{ date('j M Y', strtotime($flight->return_departure_date)) }}</div>
                            <div class="text-slate-500">{{ date('H:i A', strtotime($flight->return_departure_time)) }}</div>
                            <small class="text-slate-500">{{ $flight->return_departure_city->name}}</small>
                        </div>

                        <div class="text-right">
                            <small class="text-slate-500">Return Arrival</small>
                            <div class="font-medium">{{ date('j M Y', strtotime($flight->return_arrival_date)) }}</div>
                            <div class="text-slate-500">{{ date('H:i A', strtotime($flight->return_arrival_time)) }}</div>
                            <small class="text-slate-500">{{ $flight->return_arrival_city->name}}</small>
                        </div>
                    </div>
                @endif

                <div class="mt-3">
                    <div class="font-medium text-center lg:text-left lg:mt-3 mb-1">Ticket</div>
                    <div>
                        <img src="{{ Storage::url($flight->ticket_image_path) }}" alt="Ticket image" class="rounded-none">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 

  <!-- BEGIN: Appointment Modal -->
  <div id="appointmentModal" class="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <form class="modal-content" id="appointment-form" method="post" action="{{ route('appointments.store') }}" enctype="multipart/form-data">
            @csrf
            <!-- BEGIN: Modal Header -->
            <input type="hidden" name="app_user" value="{{ $flight->app_user->id }}">
            <input type="hidden" name="flight_id" value="{{ $flight->id }}">

            <div class="modal-header">
                <h2 class="font-medium text-base mr-auto">New Appointment</h2> 
            </div> <!-- END: Modal Header -->
            <!-- BEGIN: Modal Body -->
            <div class="modal-body grid grid-cols-12 gap-4 gap-y-3">
                <div class="col-span-12"> 
                    <label for="location" class="form-label">Location</label> 
                    <select class="form-select form-control block mb-1" aria-label="Location select" id="location-select" name="location">
                        @foreach ($locations as $location)
                            <option value="{{ $location->id }}">{{ $location->name}}</option>
                        @endforeach
                    </select>  
                    <a href="javascript:;" data-tw-toggle="modal" data-tw-target="#location-modal" class="text-secondary underline">Add location</a>
                </div>
                <div class="col-span-12"> 
                    <label for="date" class="form-label">Date</label> 
                    <input type="text" class="datepicker form-control block" data-single-mode="true" name="date">  
                </div>
                <div class="col-span-12"> 
                    <label for="date" class="form-label">Time</label> 
                    <div class="relative timepicker" id="timepicker-inline-12" data-te-input-wrapper-init>
                        <input type="text" class="form-control block rounded border bg-transparent px-3 py-[0.32rem] leading-[1.6] outline-none [&:not([data-te-input-placeholder-active])]:placeholder:opacity-0" id="form2" name="time"/>
                    </div>
                </div>
            </div> <!-- END: Modal Body -->
            <!-- BEGIN: Modal Footer -->
            <div class="modal-footer"> 
                <button type="button" data-tw-dismiss="modal" class="btn btn-outline-secondary w-20 mr-1">Cancel</button> 
                <button type="submit" class="btn btn-primary w-20">Schedule</button> 
            </div> <!-- END: Modal Footer -->
        </form>
    </div>
</div>
<!-- END: Appointment Modal -->

<div id="location-modal" class="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{ route('locations.store') }}" enctype="multipart/form-data" method="post" id="location-form">
                @csrf
                <div class="modal-header">
                    <h2 class="font-medium text-base mr-auto">New Location</h2> 
                </div>
                <div class="modal-body grid grid-cols-12 gap-4 gap-y-3">
                    <input type="hidden" name="response" value="json">

                    <div class="col-span-12"> 
                        <label for="name" class="form-label">Name</label> 
                        <input type="text" class="form-control block" data-single-mode="true" name="name" placeholder="Embassy of Suriname" required value="{{ old('name') }}">
                        <span class="text-danger hidden" id="location-name-error"></span>
                    </div>

                    <div class="col-span-12"> 
                        <label for="address" class="form-label">Address</label> 
                        <input type="text" class="form-control block" data-single-mode="true" name="address" placeholder="Kleine Water St 44" required value="{{ old('address') }}">
                    </div>

                    <div class="col-span-12"> 
                        <label for="phone" class="form-label">Phone</label> 
                        <input type="tel" class="form-control block" data-single-mode="true" name="phone" placeholder="+597 481-223" required value="{{ old('phone') }}">
                    </div>
                </div>
                <div class="modal-footer"> 
                    <button type="button" data-tw-dismiss="modal" class="btn btn-outline-secondary w-20 mr-1">Cancel</button> 
                    <button type="submit" class="btn btn-primary w-20">Save</button> 
                </div>
            </form>
            
        </div>
    </div>
</div>


<div id="checklistModal" class="modal modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <form class="modal-content" id="flight-form" method="post" action="{{ route('checklists.store') }}" enctype="multipart/form-data">
            @csrf
            <!-- BEGIN: Modal Header -->
            <input type="hidden" name="checklistable_type" value="flight">
            <input type="hidden" name="checklistable_id" value="{{ $flight->id }}">

            <div class="modal-header">
                <h2 class="font-medium text-base mr-auto">New Checklist</h2> 
            </div> <!-- END: Modal Header -->
            <!-- BEGIN: Modal Body -->
            <div class="modal-body grid grid-cols-12 gap-4 gap-y-3">
                <div class="col-span-12"> 
                    <label for="type" class="form-label">Checklist Template</label> 
                    <select class="form-select form-control block mb-1" aria-label="Checlist Template Select" id="checklist-template-select" name="checklisttemplate_id">
                        @foreach ($checklisttemplates as $checklisttemplate)
                            <option value="{{ $checklisttemplate->id }}">{{ $checklisttemplate->name }}</option>
                        @endforeach
                    </select>
                </div>
                
            </div> <!-- END: Modal Body -->
            <!-- BEGIN: Modal Footer -->
            <div class="modal-footer"> 
                <button type="button" data-tw-dismiss="modal" class="btn btn-outline-secondary w-20 mr-1">Cancel</button> 
                <button type="submit" class="btn btn-primary w-20">Save</button> 
            </div> <!-- END: Modal Footer -->
        </form>
    </div>
</div>

@endsection

@section('scripts')
    @parent
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            const locationForm = document.getElementById('location-form');
            const submitButton = locationForm.querySelector('button[type="submit"]');
            
            const flightTypeSelect = document.getElementById('flight-type-select');
            const returnFlightFields = document.getElementById('return-flight-fields');

            const locationModal = tailwind.Modal.getOrCreateInstance(document.querySelector("#location-modal"));

            const pickerInline = document.querySelector("#timepicker-inline-12");
            const timepickerMaxMin = new te.Timepicker(pickerInline, { format12: true,
            inline: true, });

            const departureTime = document.querySelector("#departure_time");
            const departureTimeMaxMin = new te.Timepicker(departureTime, { format12: true,
            inline: true, });

            const arrivalTime = document.querySelector("#arrival_time");
            const arrivalTimeMaxMin = new te.Timepicker(arrivalTime, { format12: true,
            inline: true, });

            const returnDepartureTime = document.querySelector("#return_departure_time");
            const returnDepartureTimeMaxMin = new te.Timepicker(returnDepartureTime, { format12: true,
            inline: true, });

            const returnArrivalTime = document.querySelector("#return_arrival_time");
            const retrurnArrivalTimeMaxMin = new te.Timepicker(returnArrivalTime, { format12: true,
            inline: true, });

            // Function to toggle visibility of return flight fields
            function toggleReturnFlightFields() {
                if (flightTypeSelect.value === 'round-trip') {
                    returnFlightFields.classList.remove('hidden');
                } else {
                    returnFlightFields.classList.add('hidden');
                }
            }

            // Add event listener to flight type select
            flightTypeSelect.addEventListener('change', toggleReturnFlightFields);

            locationForm.addEventListener('submit', function(event) {
                event.preventDefault();

                document.getElementById('location-name-error').classList.add('hidden');

                // Disable the submit button
                submitButton.disabled = true;

                var form = event.target; // Get the form element
                var formData = new FormData(form); // Create a new FormData object

                // Create a new XMLHttpRequest object
                var xhr = new XMLHttpRequest();
                xhr.open('POST', form.action); // Set the request method and URL

                // Set up the onload and onerror event handlers
                xhr.onload = function() {
                    if (xhr.status === 200) {
                    // Handle a successful response
                    var response = JSON.parse(xhr.responseText);
                    // console.log(response);
                    var locationSelect = document.getElementById('location-select');
                    var option = document.createElement("option");
                    option.text = response.name;
                    option.value = response.id;
                    option.selected = true;
                    locationSelect.appendChild(option);
                    locationModal.hide();

                    } else {
                        // Handle an error response
                        var response = JSON.parse(xhr.responseText);
                        // console.log(response);

                        if(response.name){
                            var locationNameError = document.getElementById('location-name-error');
                            locationNameError.innerHTML = response.name;
                            locationNameError.classList.remove("hidden");
                            submitButton.disabled = false;
                        }
                    }
                };

                xhr.onerror = function() {
                    // Handle an error during the request
                    alert("something went wrong, please try again");
                    submitButton.disabled = false;
                    locationModal.hide();
                };

                // Send the request with the form data
                xhr.send(formData);

                //Avoids default form submit
                return false;
            });
        });
        
    </script>
@endsection

