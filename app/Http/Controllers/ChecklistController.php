<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use App\Models\AppUser;
use App\Models\Checklist;
use App\Models\ChecklistTemplate;
use App\Models\Flight;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ChecklistController extends Controller
{
    //
    public function index(){
        return view('checklists.overview', [
            'checklists' => Checklist::paginate(20)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $checklistableType = $request->input('checklistable_type'); // e.g., 'flight' or 'appointment'
        $checklistableId = $request->input('checklistable_id'); // e.g., the ID of the flight or appointment
        
        $request->validate([
            'checklistable_type' => 'required|in:flight,appointment',
            'checklistable_id' => 'required|numeric',
            'checklisttemplate_id' => 'required|exists:checklist_templates,id',
        ]);

        // Find the checklistable model
        $checklistable = null;
        if ($checklistableType === 'flight') {
            $checklistable = Flight::find($checklistableId);
        } elseif ($checklistableType === 'appointment') {
            $checklistable = Appointment::find($checklistableId);
        }

        // Ensure the checklistable model was found
        if (!$checklistable) {
            return redirect()->back()->withErrors(['checklistable_id' => 'Invalid ID for checklistable type.'])->withInput();
        }

        // Find the checklist template
        $checklisttemplate = ChecklistTemplate::findOrFail($request->input('checklisttemplate_id'));

        // Create the checklist
        $checklist = new Checklist([
            'name' => $checklisttemplate->name,
            'items' => $checklisttemplate->items,
        ]);

        // Associate the checklist with the checklistable model
        $checklistable->checklists()->save($checklist);

        return redirect()->back()->with('success', 'Checklist added successfully.');
        
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $checklist = Checklist::findOrFail($id);
        $checklistable  = $checklist->checklistable;

        $appuser = null;
        if ($checklistable instanceof Flight) {
            $appuser = $checklistable->app_user;
        } elseif ($checklistable instanceof Appointment) {
            $appuser = $checklistable->appUser;
        }

        $checklist->items = json_decode($checklist->items, true);
        return view('checklists.edit', compact('checklist', 'checklistable', 'appuser'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {

        $checklist = Checklist::findOrFail($id);

        $checklist->name = $request->input('name');
        $checklist->items = $request->input('items');
        $checklist->save();

        // Redirect back with a success message
        return response()->json(['message' => 'Checklist updated successfully'], 200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id){
        $checklist = Checklist::findOrFail($id);

        $checklistableType = class_basename($checklist->checklistable);
        $checklistableId = $checklist->checklistable->id;

        $checklist->delete();

        // Determine the redirect URL based on the checklistable type
        switch ($checklistableType) {
            case 'Flight':
                $redirectUrl = route('flights.show', $checklistableId);
                break;
            case 'Appointment':
                $redirectUrl = route('appointments.show', $checklistableId);
                break;
            default:
                return response()->json(['message' => 'Unknown checklistable type: ' . $checklistableType], 400);
        }

        // Redirect to the determined URL with a success message
        return redirect($redirectUrl)->with('message', 'Checklist deleted successfully');
    }

    public function upcomingChecklists(Request $request)
    {
        $user = auth()->user();

        $upcomingChecklists = collect();

        //fetch all flights of the user from yesterday and the future 
        $upcomingFlights = Flight::where('app_user_id', $user->id)
        ->where('departure_date', '>=', Carbon::now()->subDay()->toDateString())
        ->get();
        foreach ($upcomingFlights as $flight) {
            //convert flight datetime to utc
            $flightTimezone = $flight->departure_city->timezone;
            $flightDateTime = Carbon::createFromFormat('Y-m-d H:i:s', $flight->departure_date . ' ' . $flight->departure_time, $flightTimezone);
            $flightDateTimeUTC = $flightDateTime->setTimezone('UTC');

            // Check if the flightDateTimeUTC is greater than the current UTC time
            if($flightDateTimeUTC->gt(Carbon::now('UTC'))) {
                // Fetch the checklists and add them to the collection
                $upcomingChecklists = $upcomingChecklists->concat($flight->checklists);
            }
        }

        //fetch all appointments of the user from yesterday and the future 
        $upcomingAppointments = Appointment::where('app_user_id', $user->id)
        ->where('date', '>=', Carbon::now()->subDay()->toDateString())
        ->get();
        foreach ($upcomingAppointments as $appointment) {
            //convert appointment datetime to utc
            $appointmentTimezone = $appointment->location->city->timezone;
            $appointmentDateTime = Carbon::createFromFormat('Y-m-d H:i:s', $appointment->date . ' ' . $appointment->time, $appointmentTimezone);
            $appointmentDateTimeUTC = $appointmentDateTime->setTimezone('UTC');

            // Check if the flightDateTimeUTC is greater than the current UTC time
            if($appointmentDateTimeUTC->gt(Carbon::now('UTC'))) {
                // Fetch the checklists and add them to the collection
                $upcomingChecklists = $upcomingChecklists->concat($appointment->checklists);
            }
        }

        return response()->json($upcomingChecklists);
    }

    public function updateChecklistData(Request $request, $id){
        $checklist = Checklist::findOrFail($id);

        $relatedModel = $checklist->checklistable;

        // Ensure the authenticated user owns the related model (appointment or flight)
        if ($relatedModel->app_user_id !== auth()->user()->id) {
            return response()->json(['message' => 'Unauthorized'], 403);
        }

        $checklist->items = $request->input('items');
        $checklist->save();

        return response()->json(['success' => true,
                                    'message' => 'Checklist updated successfully']);
    }
}
