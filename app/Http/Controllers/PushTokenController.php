<?php

namespace App\Http\Controllers;

use App\Models\PushToken;
use Illuminate\Http\Request;

class PushTokenController extends Controller
{

    public function store(Request $request)
    {
        $request->validate([
            'token' => 'required|string',
        ]);

        $user = auth()->user();

        // Check if the token already exists for the user
        $existingToken = PushToken::where('app_user_id', $user->id)
        ->where('token', $request->token)
        ->first();

        if ($existingToken) {
            // If the token exists but is associated with a different user, update the user ID
            $existingToken->app_user_id = $user->id;
            $existingToken->save();
            
        }else{
            // If the token doesn't exist, create a new one
            PushToken::create([
                'app_user_id' => $user->id,
                'token' => $request->token,
            ]);
        }

        return response()->json(['message' => 'Pushtoken saved successfully']);

    }
}
