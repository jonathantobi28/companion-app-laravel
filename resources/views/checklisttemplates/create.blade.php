@extends('layout')

@section('title', 'New Checklist Template')

@section('styles')
    @parent
    <style>
        .more-info{
            padding-left: 24px;
            padding-right: 56px
        }
        .more-info input{
            font-size: 0.7rem;
            padding: 0.2rem 0.5rem
        }
    </style>
@endsection

@section('breadcrumbs')
    @parent
    <li class="breadcrumb-item"><a href="{{route('checklisttemplates.overview')}}">Checklist Templates</a></li>
    <li class="breadcrumb-item active" aria-current="page">New</li>
@endsection

@section('content')
<form id="checklist-template-form" method="post" action="{{route('checklisttemplates.store')}}" enctype="multipart/form-data">
    @csrf
    <div class="grid grid-cols-12 gap-6 mt-10">
        <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
            <h2 class="intro-y text-lg font-medium">
                Add New Checklist Template
            </h2>
            <button class="btn btn-primary shadow-md ml-auto" type="submit">Save</button>
        </div>                      
    </div>
    <div class="pos intro-y grid grid-cols-12 gap-5 mt-5">
        <!-- BEGIN: Post Content -->
        <div id="dataset" class="intro-y col-span-12 p-5">
            
            <div class="pos intro-y grid grid-cols-12 gap-5 mt-5">

                <div class="col-span-12">
                    <div class="grid grid-cols-12 gap-5">
                        <div class="col-span-3"> 
                            <label for="options" class="block text-gray-700 font-normal mb-2">General options</label>
                            <select name="options" id="options" class="form-select-sm form-control block mb-1" aria-label="Option select" required>
                                <option value="none">None</option>
                                <option value="all_flights">All flights</option>
                                <option value="specific_departure_city">Specific departure city</option>
                                <option value="all_appointments">All appointments</option>
                                <option value="specific_location">Specific location</option>
                            </select>
                            @error('options')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
    
                        <div class="col-span-3 hidden" id="departure-city-container"> 
                            <label for="departure_city" class="block text-gray-700 font-normal mb-2">Departure city</label>
                            <select name="departure_city" id="departure_city" class="form-select-sm form-control mb-1 block" aria-label="Departure city select">
                                @foreach($cities as $city)
                                    <option value="{{ $city->id }}">{{ $city->name }}</option>
                                @endforeach
                            </select>
                            @error('departure_city')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
    
                        <div class="col-span-3 hidden" id="location-container"> 
                            <label for="location" class="block text-gray-700 font-normal mb-2">Location</label>
                            <select name="location" id="location" class="form-select-sm form-control block mb-1" aria-label="Departure city select">
                                @foreach($locations as $location)
                                    <option value="{{ $location->id }}">{{ $location->name }}</option>
                                @endforeach
                            </select>
                            @error('location')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="col-span-12"> 
                    <label for="name" class="block text-gray-700 font-normal mb-2">Name</label>
                    <input type="text" id="name" name="name" class="w-full border-gray-300 rounded-md p-2 text-sm" value="{{ old('name') }}" required>
                    @error('name')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div id="items" class="intro-y col-span-6">
                    <small class="block mb-3">Items</small>
                </div>
                
                <div class="intro-y col-span-12">
                    <div class="dropdown inline-block" data-tw-placement="right-start"> 
                        <button type="button" class="dropdown-toggle btn btn-sm btn-primary mr-1 mb-2" aria-expanded="false" data-tw-toggle="dropdown" id="add-item-dropdown">
                            <i data-lucide="plus" class="w-5 h-5"></i></button>
                        <div class="dropdown-menu w-40">
                            <ul class="dropdown-content">
                                <li> <a href="#" data-type="title" class="add-item dropdown-item"><i data-lucide="align-left" class="w-5 h-5 mr-2"></i>Title</a> </li>
                                <li> <a href="#" data-type="choice" class="add-item dropdown-item"><i data-lucide="toggle-left" class="w-5 h-5 mr-2"></i>Choice</a> </li>
                                <li> <a href="#" data-type="checklist" class="add-item dropdown-item"><i data-lucide="check" class="w-5 h-5 mr-2"></i>Checklist</a> </li>
                            </ul>
                        </div>
                    </div>
                </div>
                
            </div>
            
        </div>

        <!-- END: Post Content -->
    </div>  
</form>

@endsection

@section('scripts')
    @parent
    <script>
        const csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
        const datasetContainer = document.getElementById('dataset');
        const itemsContainer = document.getElementById('items');
        const checkDataBtn = document.getElementById('checkdata');
        const optionSelect = document.getElementById('options');
        const departureCitySelect = document.getElementById('departure_city');
        const departureCitySelectContainer = document.getElementById('departure_city').parentElement;
        const locationSelect = document.getElementById('location');
        const locationSelectContainer = document.getElementById('location').parentElement;

        optionSelect.addEventListener('change', function() {
            // Show the relevant condition value input based on the selected condition
            switch(this.value) {
                case 'specific_departure_city':
                    departureCitySelect.required = true;
                    departureCitySelectContainer.classList.remove('hidden');
                    locationSelectContainer.classList.add('hidden');
                    locationSelect.required = false;
                    break;
                case 'specific_location':
                    locationSelect.required = true;
                    locationSelectContainer.classList.remove('hidden');
                    departureCitySelectContainer.classList.add('hidden');
                    departureCitySelect.required = false;
                    break;
                default:
                    departureCitySelect.required = false;
                    locationSelect.required = false;
                    departureCitySelectContainer.classList.add('hidden');
                    locationSelectContainer.classList.add('hidden');
                    break;
            }
        });

        function capitalizeFirstLetter(string) {
            return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
        }

        const itemDropdown = tailwind.Dropdown.getOrCreateInstance(document.querySelector("#add-item-dropdown"));

        const checklistDataForm = document.getElementById('checklist-template-form');
        checklistDataForm.addEventListener('submit', function(e){
            e.preventDefault();

            const isValidTitleItem = (item) => {
                return item.type === 'title' && item.title && item.title.trim() !== "" && item.content && item.content.trim() !== "";
            };

            const isValidChoiceItem = (item) => {
                return item.type === 'choice' && item.title && item.title.trim() !== "";
            };

            const isValidChecklistItem = (item) => {
                if (item.type !== 'checklist' || !item.title || item.title.trim() === "") return false;
                if (!item.items || item.items.length === 0) return false;

                // Filter out sub-items without a title
                item.items = item.items.filter(subItem => subItem.title && subItem.title.trim() !== "");

                // Ensure we still have valid sub-items after filtering
                return item.items.length > 0;
            };

            const hasChecklistItem = checklistData.some(item => item.type === 'checklist');

            const filteredChecklistData = checklistData.filter(item => {
                return isValidTitleItem(item) || isValidChoiceItem(item) || isValidChecklistItem(item);
            });

            if (filteredChecklistData.length === 0) {
                // Handle the error
                // console.error("Cannot save checklist template without at least one item.");
                alert("Checklists must have at least one item. Make sure all your items have titles.\n\n- Information items should have content.\n- Checklist items should have at least 1 item with a title.");
                return;
            }

            // Only check for a valid checklist item if there's a checklist item in the original data
            if (hasChecklistItem) {
                const hasValidChecklist = filteredChecklistData.some(isValidChecklistItem);

                if (!hasValidChecklist) {
                    alert("Checklist items should have at least 1 item with a title.");
                    return;
                }
            }

            const submitButton = checklistDataForm.querySelector('button[type="submit"]');
            const nameInput = checklistDataForm.querySelector('input[name="name"]');

            let condition = optionSelect.value;
            let conditionValue = null;

            if (condition === 'specific_departure_city') {
                conditionValue = departureCitySelect.value;
            } else if (condition === 'specific_location') {
                conditionValue = locationSelect.value;
            }
            
            // Disable the submit button
            submitButton.disabled = true;

            var form = e.target; // Get the form element

            var formData = new FormData(); // Create a new FormData object
            formData.append('name', nameInput.value);
            formData.append('items', JSON.stringify(checklistData));
            formData.append('condition', condition);
            if (conditionValue) {
                formData.append('condition_value', conditionValue);
            }

            var xhr = new XMLHttpRequest();
            xhr.open('POST', form.action); // Set the request method and URL

            xhr.setRequestHeader('X-CSRF-TOKEN', csrfToken);

            // Set up the onload and onerror event handlers
            xhr.onload = function() {
                if (xhr.status === 200) {
                    window.location.href = "{{ route('checklisttemplates.overview') }}";
                } else {
                    throw new Error('Request failed with status ' + xhr.status);
                }
            };

            xhr.onerror = function() {
                // Handle an error during the request
                alert("something went wrong, please try again");
                submitButton.disabled = false;
            };

            // Send the request with the form data
            xhr.send(formData);

            //Avoids default form submit
            return false;
        });

        const addItem = document.querySelectorAll('.add-item');
        addItem.forEach( (button) => {
            button.addEventListener('click', (e) => {
                e.preventDefault();

                const type = button.dataset.type;

                let item = {};

                const index = checklistData.length;

                if(type == "title"){
                    item = {
                        "type": "title",
                        "title": "",
                        "content": "",
                        "info": ""
                    };   
                }

                if(type == "choice"){
                    item = {
                        "type": "choice",
                        "title": "",
                        "checked": false,
                        "info": ""
                    };   
                }

                if(type == "checklist"){
                    item = {
                        "type": "checklist",
                        "title": "",
                        "items": [],
                        "info": ""
                    };   
                }

                checklistData.push(item);
                createItem(item, index, itemsContainer);

                itemDropdown.hide();
            });
        }); 

        const checklistAddItem = document.querySelectorAll('.checklist-add-item');
        checklistAddItem.forEach( (button) => {
            button.addEventListener('click', (e) => {
                e.preventDefault();

                const parentChecklistContainer = button.closest('.checklist-items');

                const item = {
                    "title": "",
                    "checked": false,
                    "info": ""
                };

                const index = checklistData[boxIndex]["items"].length;

                checklistData.push(item);

                createCheck(boxIndex, index, item, parentChecklistContainer);
            });
        }); 

        const checklistData = [];

        function createInput(index, property, value, itemContainer){
            const inputContainer = document.createElement('div');
            const capitalizedProperty = capitalizeFirstLetter(property);
            inputContainer.classList.add("mb-3");
            const inputLabel = document.createElement('label');
            inputLabel.setAttribute("for", `${property}-${index}`);
            inputContainer.appendChild(inputLabel);

            // input creation
            const input = document.createElement('input');
            input.setAttribute("type", "text");
            input.setAttribute("name", `${property}-${index}`);
            input.setAttribute("data-item-property", property);
            input.setAttribute('required', true);
            input.setAttribute("placeholder", capitalizedProperty);
            input.classList.add("form-control", "form-control-sm", "item-input");
            input.value = value;
            input.addEventListener('change', (event) => {
                const newValue = event.target.value;
                checklistData[index][property] = newValue;
            });
            inputContainer.appendChild(input);
            itemContainer.appendChild(inputContainer);
        }

        function createRadio(index, choice, checked, itemContainer){
            const container = document.createElement('div');
            container.classList.add("form-check", "mr-2");

            const label = document.createElement('label');
            label.classList.add("form-check-label", "mr-1");
            label.setAttribute("for", `${choice}-${index}`);
            label.innerText = choice
            container.appendChild(label);

            const input = document.createElement('input');
            input.setAttribute("type", "radio");
            input.setAttribute("name", `choice-${index}`);
            input.setAttribute("value", choice);
            input.setAttribute("id", `${choice}-${index}`);
            input.classList.add("form-check-input", "radio-input");
            input.checked = checked;
            input.addEventListener('change', (event) => {
                const newValue = event.target.value == "Yes";
                checklistData[index]["checked"] = newValue;
            });
            container.appendChild(input);
            itemContainer.appendChild(container);
        }

        function createRadioGroup(index, state, itemContainer){
            const container = document.createElement('div');
            container.classList.add("mb-3");
            
            const radioGroup = document.createElement('div');
            radioGroup.classList.add("flex", "flex-row", "mt-2", "radiogroup");

            createRadio(index, "Yes", state, radioGroup);
            createRadio(index, "No", !state, radioGroup);

            container.appendChild(radioGroup);
            itemContainer.appendChild(container);
        }

        function createCheck(boxIndex, index, item, itemContainer){
            const container = document.createElement('div');
            container.classList.add("mb-3");

            const formContainer = document.createElement('div');
            formContainer.classList.add("form-check");

            const checkInput = document.createElement('input');
            checkInput.setAttribute("type", "checkbox");
            checkInput.setAttribute("id", `checklist-${boxIndex}-item-${index}`);
            checkInput.setAttribute("name", `checklist-${boxIndex}-item-${index}`);
            checkInput.classList.add("form-check-input", "mr-2", "checklist-checkbox");
            checkInput.checked = item.checked;
            checkInput.addEventListener('change', (event) => {
                const newValue = event.target.checked;
                checklistData[boxIndex]["items"][index]["checked"] = newValue;
            });
            formContainer.appendChild(checkInput);

            const checkInputDescription = document.createElement('input');
            checkInputDescription.setAttribute("type", "text");
            checkInputDescription.setAttribute('required', true);
            checkInputDescription.setAttribute("id", `checklist-${boxIndex}-item-${index}-title`);
            checkInputDescription.setAttribute("name", `checklist-${boxIndex}-item-${index}-title`);
            checkInputDescription.classList.add("form-control", "form-control-sm", "checklist-description");
            checkInputDescription.value = item.title;
            checkInputDescription.addEventListener('change', (event) => {
                const newValue = event.target.value;
                checklistData[boxIndex]["items"][index]["title"] = newValue;
            });
            formContainer.appendChild(checkInputDescription);

            const deleteButton = document.createElement('button');
            deleteButton.classList.add("checklist-delete-button", "btn", "btn-sm", "btn-danger-soft", "ml-2");
            deleteButton.innerText = "x";
            deleteButton.addEventListener('click', (e) => {
                e.preventDefault();

                container.remove();
                if(index >= 0 && index < checklistData[boxIndex]["items"].length){
                    checklistData[boxIndex]["items"].splice(index, 1);
                }
            });

            formContainer.appendChild(deleteButton);

            container.appendChild(formContainer);

            const inputContainer = document.createElement('div');
            inputContainer.classList.add("mt-2", "more-info");

            const moreInfoInput = document.createElement('input');
            moreInfoInput.setAttribute("type", "text");
            moreInfoInput.setAttribute("name", `info-${index}`);
            moreInfoInput.setAttribute("data-item-property", "info");
            moreInfoInput.setAttribute("placeholder", "More info url or leave empty to discard");
            moreInfoInput.classList.add("form-control", "form-control-sm", "checklist-info-input");
            moreInfoInput.value = item.info;
            moreInfoInput.addEventListener('change', (event) => {
                const newValue = event.target.value;
                checklistData[boxIndex]["items"][index]["info"] = newValue;
            });

            inputContainer.appendChild(moreInfoInput);
            container.appendChild(inputContainer);
            itemContainer.appendChild(container);
        }

        function createCheckGroup(boxIndex, items, itemContainer){
            const container = document.createElement('div');
            container.classList.add("px-4");

            const title = document.createElement('small');
            title.classList.add("block", "mb-2");
            title.innerText = "Checklist items";
            container.appendChild(title);

            const checklistItemsContainer = document.createElement('div');
            checklistItemsContainer.classList.add("checklist-items");

            items.forEach( (item, index) => {
                createCheck(boxIndex, index, item, checklistItemsContainer);
            });

            container.appendChild(checklistItemsContainer);

            const addButton = document.createElement('button');
            addButton.classList.add("checklist-add-item", "btn", "btn-sm", "btn-primary");
            addButton.innerText = "+ Add";
            addButton.addEventListener('click', (e) => {
                e.preventDefault();

                const itemAdd = {
                    "title": "",
                    "checked": false,
                    "info": ""
                };

                const itemIndex = checklistData[boxIndex]["items"].length;
                checklistData[boxIndex]["items"].push(itemAdd);

                createCheck(boxIndex, itemIndex, itemAdd, checklistItemsContainer);
            });

            container.appendChild(addButton);
            itemContainer.appendChild(container);
        }


        function createInfo(index, value, itemContainer){
            const inputContainer = document.createElement('div');
            inputContainer.classList.add("mt-5");

            // input creation
            const input = document.createElement('input');
            input.setAttribute("type", "text");
            input.setAttribute("name", `info-${index}`);
            input.setAttribute("data-item-property", "info");
            input.setAttribute("placeholder", "Extra info or leave empty to discard");
            input.classList.add("form-control", "form-control-sm", "item-input");
            input.value = value;
            input.addEventListener('change', (event) => {
                const newValue = event.target.value;
                checklistData[index]["info"] = newValue;
            });
            inputContainer.appendChild(input);
            itemContainer.appendChild(inputContainer);
        }


        function createItem(item, index, itemsContainer){

            // container creation
            const itemContainer = document.createElement('div');
            itemContainer.classList.add("mb-4", "box", "p-5");

            // container header creation
            const itemHeaderContainer = document.createElement('div');
            itemHeaderContainer.classList.add("flex", "flex-row", "justify-between", "align-middle", "mb-3");
            itemContainer.appendChild(itemHeaderContainer);

            // container description creation
            const itemDescription = document.createElement('small');
            let itemDescriptionContent = "Information";
            if(item.type == "choice"){
                itemDescriptionContent = "Choice (Yes/No)";
            }
            if(item.type == "checklist"){
                itemDescriptionContent = "Checklist";
            }
            
            itemDescription.innerText = itemDescriptionContent;
            itemHeaderContainer.appendChild(itemDescription);

            // delete button creation
            const deleteButton = document.createElement('button');
            deleteButton.classList.add("delete-button", "btn", "btn-sm", "w-24", "btn-danger-soft");
            deleteButton.innerText = "remove";
            deleteButton.addEventListener('click', (e) => {
                e.preventDefault();

                itemContainer.remove();
                if(index >= 0 && index < checklistData.length){
                    checklistData.splice(index, 1);
                }
            });
            itemHeaderContainer.appendChild(deleteButton);
            itemContainer.appendChild(itemHeaderContainer);

            if(item.type == "title"){
                createInput(index, "title", checklistData[index].title, itemContainer);
                createInput(index, "content", checklistData[index].content, itemContainer);
                createInfo(index, checklistData[index].info, itemContainer);
            }

            if(item.type == "choice"){
                createInput(index, "title", checklistData[index].title, itemContainer);
                createRadioGroup(index, checklistData[index].checked, itemContainer);
                createInfo(index, checklistData[index].info, itemContainer);
            }

            if(item.type == "checklist"){
                createInput(index, "title", checklistData[index].title, itemContainer);
                createCheckGroup(index, checklistData[index].items, itemContainer);
                createInfo(index, checklistData[index].info, itemContainer);
            }

            itemsContainer.appendChild(itemContainer);
        }

        document.addEventListener('DOMContentLoaded', function() {
            checklistData.forEach( (item, index) => {
                createItem(item, index, itemsContainer);
            });
        })
    </script>
@endsection