@extends('layout')

@section('title', 'New Banner')

@section('styles')
    @parent
    <style>
        .file-input-container{
            position: relative;
            width: 100%;
            border: 1px dashed #B1B3B6;
            display: flex;
            padding: 2em;
            align-items: flex-end;
            cursor: pointer;
            border-radius: 4px;
            height: 270px;
            justify-content: center;
            overflow: hidden;
            max-width: 480px;
        }
        .file-input-container.dragover{
            border-color: var(--primary);
        }
        .file-input-container .file-preview{
            position: absolute;
            width: 100%;
            height: 100%;
            z-index: 1;
            top: 0;
        }
        .file-input-container .file-preview img{
            width: 100%;
            height: 100%;
            object-fit: cover;
        }
        .file-input-container .file-preview .delete-button{
            position: absolute;
            top: 10px;
            right: 10px;
        }
        .file-input-container .file-preview .delete-button svg{
            stroke: #fefefe;
        }
    </style>
@endsection

@section('breadcrumbs')
    @parent
    <li class="breadcrumb-item"><a href="{{route('banners.overview')}}">Banners</a></li>
    <li class="breadcrumb-item active" aria-current="page">New</li>
@endsection

@section('content')
<form id="banner-form" method="post" action="{{route('banners.store')}}" enctype="multipart/form-data">
    @csrf
    <div class="grid grid-cols-12 gap-6 mt-10">
        <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
            <h2 class="intro-y text-lg font-medium">
                Add New Banner
            </h2>
            <button class="btn btn-primary shadow-md ml-auto" type="submit">Save</button>
        </div>                      
    </div>
    <div class="pos intro-y grid grid-cols-12 gap-5 mt-5">
        <!-- BEGIN: Post Content -->
        <div class="intro-y col-span-12 box p-5">
            <label for="title" class="block text-gray-700 font-normal mb-2">Title</label>
            <input type="text" id="title" name="title" placeholder="Banner Title" class="w-full border-gray-300 rounded-md p-2 text-sm" value="{{ old('title') }}" required>
            @error('title')
            <div class="text-danger">{{ $message }}</div>
            @enderror

            <div class="mt-5">
                <input type="file" id="image-input" name="image" style="display: none;" value="{{ old('image') }}" accept="image/*">
                <div id="image-dropzone" class="file-input-container">
                    <div id="preview-container" class="file-preview"></div>
                    <span class="text-slate-500">Drop file here or click to upload</span>
                </div>
                <div class="text-slate-500 mt-2">Recommended size 1280x720px</div>
                @error('image')
                    <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="mt-5">
                <label for="title" class="block text-gray-700 font-normal mb-2">Active period</label>
                <input type="text" data-daterange="true" class="datepicker form-control w-56 block" name="date-range" value="{{ old('date-range') }}" required> 
                @error('active-period')
                    <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <!-- END: Post Content -->
    </div>  
</form>
@endsection

@section('scripts')
    @parent
    <script>
        const csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
        const fileContainer = document.getElementById('image-dropzone');
        const fileInput = document.getElementById('image-input');
        const previewContainer = document.getElementById('preview-container');
        let temporaryFileUrl = null;
        let prevTempURL = null;


        function adjustAspectRatio() {
            fileContainer.style.height = `${fileContainer.offsetWidth * (9 / 16)}px`;
        }
        // Call the function on initial load and whenever the window is resized
        window.addEventListener('load', adjustAspectRatio);
        window.addEventListener('resize', adjustAspectRatio);

        fileContainer.addEventListener('click', () => {
            fileInput.click();
        });

        fileContainer.addEventListener('dragenter', (e) => {
            e.preventDefault();
            fileContainer.classList.add('dragover');
        });

        fileContainer.addEventListener('dragover', (e) => {
            e.preventDefault();
        });

        fileContainer.addEventListener('dragleave', () => {
            fileContainer.classList.remove('dragover');
        });

        fileContainer.addEventListener('drop', (e) => {
            e.preventDefault();
            fileContainer.classList.remove('dragover');
            // Handle the dropped file
            const file = e.dataTransfer.files[0];
            handleFile(file);
        });

        fileInput.addEventListener('change', (e) => {
            e.preventDefault();
            // Handle the selected file
            const file = e.target.files[0];
            handleFile(file);
        });

        function handleFile(file) {
            handleDelete(temporaryFileUrl);

            // Perform desired actions with the file
            if (file) {
                const reader = new FileReader();
                reader.onload = (event) => {
                    // Create a new FileList object with the selected or dropped file
                    const fileList = new DataTransfer();
                    fileList.items.add(file);

                    // Set the newly created FileList object as the value of the file input
                    fileInput.files = fileList.files;
                }
                reader.readAsDataURL(file);

                const formData = new FormData();
                formData.append('file', file);

                // Make an AJAX request to upload the file and get the temporary URL
                fetch("{{route('banners.temporary.store')}}", {
                    method: 'POST',
                    body: formData,
                    headers: {
                        'X-CSRF-TOKEN': csrfToken,
                    },
                })
                .then((response) => response.json())
                .then((data) => {
                    // Store the temporary URL
                    temporaryFileUrl = data.url;

                    // Display the preview using the temporary URL
                    const filePreview = document.createElement('div');
                    filePreview.classList.add('preview-item');
                    filePreview.innerHTML = `
                        <img src="${temporaryFileUrl}" class="preview-image">
                        <button class="delete-button"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="lucide lucide-trash-2"><path d="M3 6h18"/><path d="M19 6v14c0 1-1 2-2 2H7c-1 0-2-1-2-2V6"/><path d="M8 6V4c0-1 1-2 2-2h4c1 0 2 1 2 2v2"/><line x1="10" x2="10" y1="11" y2="17"/><line x1="14" x2="14" y1="11" y2="17"/></svg></button>
                    `;
                    previewContainer.innerHTML = '';
                    previewContainer.appendChild(filePreview);

                    const deleteButton = filePreview.querySelector('.delete-button');
                    deleteButton.addEventListener('click', (e) => {
                        e.stopPropagation();
                        handleDelete(temporaryFileUrl);

                        // Remove the preview and reset the temporary URL to make it feel instant
                        temporaryFileUrl = null;
                        previewContainer.innerHTML = '';
                    });
                })
                .catch((error) => {
                    console.error('Error uploading file:', error);
                });
            }
        }

        async function handleDelete(file){
            try {
                const response = await fetch("{{route('banners.temporary.delete')}}", {
                    method: 'DELETE',
                    body: JSON.stringify({ url: file }),
                    headers: {
                    'Content-Type': 'application/json',
                    'X-CSRF-TOKEN': csrfToken,
                    },
                });
                
            } catch (error) {
                console.error('An error occurred while deleting the file:', error);  
            }
        }

    </script>
@endsection