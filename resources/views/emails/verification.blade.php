@extends('emails.layout')

@section('content')

<h1 style="text-align: center;color:#1d1d1d">Verify Your Email</h1>
<p style="text-align: center;color:#1d1d1d">Please use the following verification code to verify your email:</p>
<h2 style="text-align: center;color:#1d1d1d;font-size:16px;">{{ $appUser->verification_code }}</h2>

@endsection