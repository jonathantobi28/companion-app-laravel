<?php

namespace Database\Seeders;

use App\Models\Airline;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AirlineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $airlines = [
            ['name' => 'KLM'],
            ['name' => 'SLM'],
        ];

        foreach ($airlines as $airline) {
            Airline::create($airline);
        }
    }
}
