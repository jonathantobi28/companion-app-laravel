<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    //
    /**
     * Handle account login request
     * 
     * @param LoginRequest $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function login(){

        $credentials = request()->validate([
             'email' => 'required',
             'password' => 'required',
         ]);
 
         if(auth()->attempt($credentials)){
             return redirect()->route('dashboard')->with('success', 'Welcome back '.auth()->user()->name.'!');
         }
   
         throw ValidationException::withMessages(([
             'email' => 'Your provided credentials could not be verified.'
         ]));
     }

     /**
     * Handle response after user authenticated
     * 
     * @param Request $request
     * @param Auth $user
     * 
     * @return \Illuminate\Http\Response
     */
    protected function authenticated(Request $request, $user) 
    {
        return redirect()->intended();
    }

    public function logout(){
        Session::flush();
        Auth::logout();

        return redirect()->route('login');
    }

    public function index(){
        // load the view and pass the administrators
        return view('administrators.overview', [
            'administrators' => User::paginate(10)
        ]);
    }
}
