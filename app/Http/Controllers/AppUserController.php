<?php

namespace App\Http\Controllers;

use App\Models\AppUser;
use App\Models\Passport;
use App\Models\Appointment;
use App\Models\Location;
use App\Models\Country;
use App\Models\City;
use App\Models\Checklist;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

use App\Mail\PasswordResetCodeMail;
use App\Mail\PasswordResetMail;
use App\Mail\RegistrationMail;
use App\Models\Airline;
use App\Models\Flight;
use App\Models\NotificationPreference;

class AppUserController extends Controller
{

    public function index(){
        return view('appusers.overview', [
            'appusers' => AppUser::paginate(10)
        ]);
    }

    public function liveSearch(Request $request){
        $query = AppUser::query();

        if ($request->has('query')) {
            $searchTerm = $request->input('query');
            $query->where('name', 'like', '%' . $searchTerm . '%')
                ->orWhere('email', 'like', '%' . $searchTerm . '%');
        }

        $appusers = $query->get();

        $htmlContent = view('appusers.partials.overview', ['appusers' => $appusers])->render();

        return response($htmlContent);
    }

    public function show($id)
    {
        $appuser = AppUser::findOrFail($id);
        $locations = Location::all();
        $airlines = Airline::all();
        $countries = Country::with('cities')->get();
        $cities = City::all();
        $upcomingAppointments = $appuser->appointments()->upcoming()->orderBy('date', 'desc')->get();
        $pastAppointments = $appuser->appointments()->past()->orderBy('date', 'desc')->get();
        $_upcomingFlights = $appuser->flights()->upcoming()->orderBy('departure_date', 'asc')->get();
        $_pastFlights = $appuser->flights()->past()->orderBy('departure_date', 'asc')->get();

        $upcomingFlights = $_upcomingFlights->map(function ($flight) {
            // Modify the ticket image path to include the full web URL
            $flight['storage_ticket_image_path'] = Storage::url($flight->ticket_image_path);
            return $flight;
        });

        $pastFlights = $_pastFlights->map(function ($flight) {
            $flight['storage_ticket_image_path'] = Storage::url($flight->ticket_image_path);
            return $flight;
        });
        
        return view('appusers.show', compact('appuser', 'locations', 'airlines',  'upcomingAppointments', 'pastAppointments', 'countries', 'cities', 'upcomingFlights', 'pastFlights'));
    }

    public function create()
    {
        return view('appusers.create');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $appuser = AppUser::findOrFail($id);
        return view('appusers.edit', compact('appuser'));
    }

    public function store(Request $request)
    {
        // Validate the incoming request data
        $validatedData = $request->validate([
            'name' => 'required|string',
            'email' => 'required|email|unique:app_users,email',
        ]);

        // Create the app user
        $appUser = AppUser::create([
            'name' => $validatedData['name'],
            'email' => $validatedData['email'],
        ]);

        // Return a response or redirect as needed
        return redirect()->route('appusers.overview')->with('success', 'App user created successfully.');
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        // Validate the request data
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|email|unique:app_users,email,' . $id
        ]);

        // Retrieve the appuser from the database
        $appuser = AppUser::findOrFail($id);

        // Update the appuser properties
        $appuser->active = $request->has('active');

        if($appuser->name != $request->input('name')){
            $appuser->name = $request->input('name');
        }

        if($appuser->email != $request->input('email')){
            $appuser->email = $request->input('email');
        }

        // Save the updated appuser
        $appuser->save();

        // Redirect back with a success message
        return redirect()->route('appusers.overview')->with('success', 'Appuser updated successfully');
    }


    public function deactivate($id){
        $appuser = AppUser::findOrFail($id);
        $appuser->active = false;

        $appuser->save();

        return redirect()->back()->with('success', 'App user deactivated successfully.');
    }

    public function activate($id){
        $appuser = AppUser::findOrFail($id);
        $appuser->active = true;

        $appuser->save();

        return redirect()->back()->with('success', 'App user activated successfully.');
    }
    
    public function register(Request $request){
        // Validate the incoming request data
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:100',
            'passport_number' => 'required|string|max:10|unique:passports',
            'email' => 'required|email|unique:app_users',
            'password' => 'required|min:8',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        }

        // Create a new appuser record
        $appUser = AppUser::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
        ]);

        // Create the Passport for the AppUser
        $passport = new Passport([
            'passport_number' => $request->input('passport_number'),
        ]);

        $appUser->passports()->save($passport);
        $appUser->save();

        // Send the registration email to the user's email address
        Mail::to($appUser->email)->send(new RegistrationMail($appUser));

        $accessToken = $appUser->createToken('AuthToken')->plainTextToken;
        $appUser->makeHidden('password');

        // Return a response indicating successful registration
        return response()->json([
            'message' => 'Registration successfull!',
            'user' => $appUser,
            'access_token' => $accessToken,
        ], 201);
    }

    public function guest(Request $request){
        // Validate the incoming request data
        $validator = Validator::make($request->all(), [
            'passport_number' => 'required|string|max:10',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        }

        $passportNumber = $request->input('passport_number');

        // Check if the passport number exists in the Passport model
        $passport = Passport::where('passport_number', $passportNumber)->first();

        $guestName = "Guest | ".$passportNumber;
        $appUser = null;

        if(!$passport) {
            // If the passport number doesn't exist, register new guest user

            // Create a new appuser record
            $appUser = AppUser::create([
                'name' => $guestName,
            ]);

            // Create the Passport for the AppUser
            $passport = new Passport([
                'passport_number' => $request->input('passport_number'),
            ]);

            $appUser->passports()->save($passport);
            $appUser->save();

        }else{
            // If the passport number exists, return user
            $appUser = $passport->appUser;
        }

        $accessToken = $appUser->createToken('AuthToken')->plainTextToken;
        $appUser->makeHidden('password');

        // Return a response indicating successful registration
        return response()->json([
            'message' => 'Welcome, '.$guestName,
            'user' => $appUser,
            'access_token' => $accessToken,
        ], 201);
    }

    // Helper method to generate a verification token
    private function generateVerificationToken(){
        return random_int(1000, 9999); // Generate a random 4-digit number
    }

    public function verifyEmail(Request $request){
        $appUser = appUser::where('email', $request->email)->first();

        if ($appUser && $appUser->verification_code === $request->verification_code) {
            // Verification code is correct
            $appUser->email_verified_at = now();
            $appUser->save();

            $accessToken = $appUser->createToken('AuthToken')->plainTextToken;

            $appUser->makeHidden('password');

            return response()->json([
                'message' => 'Verification successful',
                'user' => $appUser->makeVisible('api_token'),
                'access_token' => $accessToken,
            ]);

        } else {
            // Verification code is incorrect
            return response()->json(['message' => 'Invalid verification code'], 400);
        }
    }

    public function login(Request $request){
        $credentials = $request->only('email', 'password');

        $user = AppUser::where('email', $credentials['email'])->first();

        if ($user && $user->active && Auth::guard('app_user')->attempt($credentials)) {
            // Authentication passed, return a success response
            $user = Auth::guard('app_user')->user();
            $accessToken = $user->createToken('AuthToken')->plainTextToken;
            
            // Hide the 'password' attribute from the JSON serialization
            $user->makeHidden('password');
            
            return response()->json([
                'message' => 'Login successful',
                'user' => $user->makeVisible('api_token'),
                'access_token' => $accessToken,
            ]);
        }

        // Authentication failed, return an error response
        return response()->json(['message' => 'Invalid credentials'], 401);
    }

    public function sendResetCode(Request $request){
        $request->validate(['email' => 'required|email']);

        $user = AppUser::where('email', $request->email)->first();

        if (!$user) {
            return response()->json(['message' => 'Invalid credentials'], 401);
        }

        // Generate a random verification code
        $code = $this->generateVerificationToken();

        // Save the code and its expiration time in the database
        $user->password_reset_code = $code;
        $user->password_reset_code_expires_at = Carbon::now()->addHour();
        $user->save();

        // Send the email with the verification code
        Mail::to($user->email)->send(new PasswordResetCodeMail($code));

        return response()->json(['success' => true]);
    }

    public function verifyResetCode(Request $request){
        $request->validate([
            'email' => 'required|email',
            'code' => 'required|string',
        ]);

        $user = AppUser::where('email', $request->email)->first();

        if (!$user || $user->password_reset_code !== $request->code) {
            return response()->json(['message' => 'Invalid credentials'], 401);
        }

        if (Carbon::now()->isAfter($user->password_reset_code_expires_at)) {
            return response()->json(['message' => 'Verification code has expired'], 401);
        }

        return response()->json(['success' => true]);
    }

    public function resetPassword(Request $request){
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|string|min:8',
        ]);

        $user = AppUser::where('email', $request->email)->first();

        if (!$user) {
            return response()->json(['message' => 'Invalid credentials'], 401);
        }

        // Update the user's password
        $user->password = Hash::make($request->password);
        $user->password_reset_code = null;
        $user->password_reset_code_expires_at = null;
        $user->save();

        // Send the email with the verification code
        Mail::to($user->email)->send(new PasswordResetMail());

        return response()->json(['success' => true]);
    }

    public function getPreferences(Request $request)
    {
        $user = auth()->user();
        $preferences = $user->notificationPreference;

        return response()->json($preferences);
    }

    public function updatePreferences(Request $request, AppUser $user)
    {
        $user = auth()->user();

        // Validate the incoming request data
        $validatedData = $request->validate([
            'pref_one_week' => 'required|boolean',
            'pref_four_days' => 'required|boolean',
            'pref_one_day' => 'required|boolean',
            'pref_two_hours' => 'required|boolean',
        ]);

        $preferences = $user->notificationPreference;

        if (!$preferences) {
            $preferences = new NotificationPreference(['app_user_id' => $user->id]);
        }

        $preferences->fill($validatedData);
        $preferences->save();

        return response()->json(['success' => true,
                                    'message' => 'Preferences updated successfully']);
    }






}
