@extends('emails.layout')

@section('content')

<h1 style="color:#1d1d1d;margin-bottom:10px">Appointment Request Received!</h1>
<p style="color:#1d1d1d">Below are the details of the requested appointment.</p>

<div style="margin-bottom:5px;padding-bottom:5px;border-bottom:1px solid #eeeeee;">
    <p style="color:#1d1d1d"><b>Appointment information:</b></p>
    <p style="color:#1d1d1d">Date: {{ date('j M, Y', strtotime($appointmentData['date'])) }}</p>
    <p style="color:#1d1d1d">Time: {{ date('h:i A', strtotime($appointmentData['time'])) }}</p>
</div>

<div style="margin-bottom:5px;padding-bottom:5px;border-bottom:1px solid #eeeeee;">
    <p style="color:#1d1d1d"><b>Personal information:</b></p>
    <p style="color:#1d1d1d">Name: {{ $appointmentData['name'] }}</p>
    <p style="color:#1d1d1d">Phone: {{ $appointmentData['phone'] }}</p>
    <p style="color:#1d1d1d">E-mail: {{ $appointmentData['email'] }}</p>
</div>

<div style="margin-bottom:5px;padding-bottom:5px;border-bottom:1px solid #eeeeee;">
    <p style="color:#1d1d1d"><b>Flight information:</b></p>
    <p style="color:#1d1d1d">From: {{ $fromCountry->name }}</p>
    <p style="color:#1d1d1d">To: {{ $toCountry->name }}</p>
    <p style="color:#1d1d1d">Departure Date: {{ date('j M, Y', strtotime($appointmentData['departure_date'])) }}</p>
    <p style="color:#1d1d1d">Return Date: {{ date('j M, Y', strtotime($appointmentData['return_date'])) }}</p>
    <p style="color:#1d1d1d">Airline: {{ $airline->name }}</p>
</div>

<p><small style="text-align: center;color:#1d1d1d;">If you didn't request an appointment, please get in touch straight away</small></p>
@endsection