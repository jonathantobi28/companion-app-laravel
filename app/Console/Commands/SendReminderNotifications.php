<?php

namespace App\Console\Commands;

use App\Models\Appointment;
use App\Models\AppUser;
use App\Models\Flight;
use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Notifications\PushNotification;
use Illuminate\Support\Facades\Notification;

class SendReminderNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:send-reminder-notifications';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send notifications based on user preferences and upcoming events';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        // Fetch all users
        $appusers = AppUser::all();

        foreach ($appusers as $user) {
            // Handle Appointments
            $this->handleEvents(Appointment::class, 'date', 'time', $user);

            // Handle Flights - Departure
            $this->handleEvents(Flight::class, 'departure_date', 'departure_time', $user);

            // Handle Flights - Return Departure
            $this->handleEvents(Flight::class, 'return_departure_date', 'return_departure_time', $user);
        }
    }

    private function getTimezoneForEvent($event, $dateField) {
        if ($event instanceof Appointment) {
            return $event->location->city->timezone;
        } elseif ($event instanceof Flight) {
            $city = null;

            if($dateField == 'departure_date'){
                $city = $event->departure_city;
            }

            if($dateField == 'return_departure_date'){
                $city = $event->return_departure_city;
            }

            return $city->timezone;
        }

        return null;
    }

    private function handleEvents($model, $dateField, $timeField, $user){
        $upcomingEvents = $model::where('app_user_id', $user->id)->whereBetween($dateField, [Carbon::now()->subDay()->toDateString(), Carbon::now()->addWeek()->addDay()->toDateString()])->get();

        foreach ($upcomingEvents as $event) {
            $this->info("Handeling event id: ". $event->id . " from user id: ".  $user->id);

            // Combine date and time to create a Carbon instance
            $eventTimezone = $this->getTimezoneForEvent($event, $dateField);
            $this->info("Event timezone: ". $eventTimezone);

            $eventDateTime = Carbon::createFromFormat('Y-m-d H:i:s', $event->{$dateField} . ' ' . $event->{$timeField}, $eventTimezone);
            $this->info("Event time: ". $eventDateTime);

            // Convert to UTC
            $eventDateTimeUTC = $eventDateTime->setTimezone('UTC');

            // Calculate the time difference between now and the event
            // $diffInHours = Carbon::now()->diffInHours($eventDateTimeUTC);
            $diffInMinutes = Carbon::now()->diffInMinutes($eventDateTimeUTC);

            $this->info("Server time: ". Carbon::now() . " Timezone: " . Carbon::now()->timezone);
            $this->info("Event datetime (UTC): ".  $eventDateTimeUTC . " Timezone: " . $eventDateTimeUTC->timezone);
            // $this->info("Difference in hours: ".  $diffInHours);
            $this->info("Difference in minutes: ".  $diffInMinutes);

            $this->info("User preference one week: ".  $user->notificationPreference->pref_one_week);
            $this->info("User preference 4 days: ".  $user->notificationPreference->pref_four_days);
            $this->info("User preference 1 day: ".  $user->notificationPreference->pref_one_day);
            $this->info("User preference 2 hours: ".  $user->notificationPreference->pref_two_hours);

            // Check user preferences and send notifications accordingly
            // if ($user->notificationPreference->pref_one_week && $diffInHours == 168) {
            //     $this->info("Sending week notification");
            //     $this->sendNotification($user, $event, '1 week');
            // }
            
            // if ($user->notificationPreference->pref_four_days && $diffInHours == 96) {
            //     $this->info("Sending 4 days notification");
            //     $this->sendNotification($user, $event, '4 days');
            // }
            
            // if ($user->notificationPreference->pref_one_day && $diffInHours == 24) {
            //     $this->info("Sending 1 day notification");
            //     $this->sendNotification($user, $event, '1 day');
            // }
            
            // if ($user->notificationPreference->pref_two_hours && $diffInHours == 2) {
            //     $this->info("Sending 2 hours notification");
            //     $this->sendNotification($user, $event, '2 hours');
            // }

            if ($user->notificationPreference->pref_one_week && $diffInMinutes == 10080) {
                $this->info("Sending week notification");
                $this->sendNotification($user, $event, '1 week');
            }
            
            if ($user->notificationPreference->pref_four_days && $diffInMinutes == 5760) {
                $this->info("Sending 4 days notification");
                $this->sendNotification($user, $event, '4 days');
            }
            
            if ($user->notificationPreference->pref_one_day && $diffInMinutes == 1440) {
                $this->info("Sending 1 day notification");
                $this->sendNotification($user, $event, '1 day');
            }
            
            if ($user->notificationPreference->pref_two_hours && $diffInMinutes == 120) {
                $this->info("Sending 2 hours notification");
                $this->sendNotification($user, $event, '2 hours');
            }
        }
    }

    private function sendNotification($user, $event, $timeFrame){
        $eventType = $event instanceof Flight ? 'flight' : 'appointment';

        $title = "Reminder!";
        $body = "You have an upcoming {$eventType} in {$timeFrame}.";

        // Log the notification being sent
        $this->info("Sending {$eventType} reminder to user {$user->id} for {$timeFrame}.");

        sendAndStoreNotification($user, $title, $body, $eventType);
    }
}