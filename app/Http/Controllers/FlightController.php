<?php

namespace App\Http\Controllers;

use App\Models\Airline;
use App\Models\Appointment;
use App\Models\AppUser;
use App\Models\Checklist;
use App\Models\Flight;
use App\Models\Location;
use App\Models\ChecklistTemplate;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Carbon;
use App\Models\TemporaryFile;
use App\Notifications\PushNotification;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class FlightController extends Controller
{
    public function index(){
        return view('flights.overview', [
            'flights' => Flight::paginate(20)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $users = AppUser::all();
        $locations = Location::all();
        $airlines = Airline::all();
        $countries = Country::with('cities')->get();
        return view('flights.create', compact('users', 'locations', 'airlines', 'countries'));
    }

    public function edit($id)
    {
        $flight = Flight::findOrFail($id);
        
        $locations = Location::all();
        $airlines = Airline::all();
        $countries = Country::with('cities')->get();

        return view('flights.edit', compact('flight, '));
    }

    public function store(Request $request){

        $flightType = $request->input('flight_type');
        
        // Perform validations based on the flight type
        if ($flightType === 'one-way') {
            // Perform validations for one-way flights

            // dd($request);

            // Example: Validate departure and arrival information
            $request->validate([
                'app_user' => 'required',
                'airline' => 'required',
                'departure_city' => 'required',
                'departure_date' => 'required',
                'departure_time' => 'required',
                'arrival_city' => 'required',
                'arrival_date' => 'required',
                'arrival_time' => 'required',
            ]);

            $flight = new Flight();
            $flight->app_user_id = $request->input('app_user');
            $flight->type = $request->input('flight_type');
            $flight->airline_id = $request->input('airline');

            $flight->departure_city_id = $request->input('departure_city');
            $flight->departure_date = date('Y-m-d', strtotime($request->input('departure_date')));
            $flight->departure_time = date('H:i:s', strtotime($request->input('departure_time')));

            $flight->arrival_city_id = $request->input('arrival_city');
            $flight->arrival_date = date('Y-m-d', strtotime($request->input('arrival_date')));
            $flight->arrival_time = date('H:i:s', strtotime($request->input('arrival_time')));

            $flight->save();

        } elseif ($flightType === 'round-trip') {

            $validatedData = $request->validate([
                'app_user' => 'required', Rule::exists('app_users', 'id'),

                'departure_city' => 'required', Rule::exists('cities', 'id'),
                'departure_date' => 'required|date',
                'departure_time' => 'required',

                'arrival_city' => 'required', Rule::exists('cities', 'id'),
                'arrival_date' => 'required|date',
                'arrival_time' => 'required',

                'return_departure_city' => 'required', Rule::exists('cities', 'id'),
                'return_departure_date' => 'required|date',
                'return_departure_time' => 'required',
                
                'return_arrival_city' => 'required', Rule::exists('cities', 'id'),
                'return_arrival_date' => 'required|date',
                'return_arrival_time' => 'required',
            ]);

            $flight = Flight::create([
                'app_user_id' => $validatedData['app_user'],
                'flight_type' => 'one-way',

                'departure_city' => $validatedData['departure_city'],
                'departure_date' => date('Y-m-d', strtotime($validatedData['departure_date'])),
                'departure_time' => date('H:i:s', strtotime($validatedData['departure_time'])),
                
                'arrival_city' => $validatedData['arrival_city'],
                'arrival_date' => date('Y-m-d', strtotime($validatedData['arrival_date'])),
                'arrival_time' => date('H:i:s', strtotime($validatedData['arrival_time'])),

                'return_departure_city' => $validatedData['return_departure_city'],
                'return_departure_date' => date('Y-m-d', strtotime($validatedData['return_departure_date'])),
                'return_departure_time' => date('H:i:s', strtotime($validatedData['return_departure_time'])),
                
                'return_arrival_city' => $validatedData['return_arrival_city'],
                'return_arrival_date' => date('Y-m-d', strtotime($validatedData['return_arrival_date'])),
                'return_arrival_time' => date('H:i:s', strtotime($validatedData['return_arrival_time'])),
            ]);
        }

        // Redirect or return a response as needed
        return redirect()->route('flights.overview')->with('success', 'Flight created successfully.');
    }

    public function storeFromAppusers(Request $request){

        // $formData = $request->all(); // Retrieve form data
        // dd($formData); // Inspect the form data 

        $flightType = $request->input('flight_type');

        $flight = null;
        
        // Perform validations based on the flight type
        if ($flightType === 'one-way') {
            // Perform validations for one-way flights

            // Example: Validate departure and arrival information
            $validator = Validator::make($request->all(), [
                'app_user' => 'required', Rule::exists('app_users', 'id'),
                'airline' => 'required',
                'departure_city' => 'required', Rule::exists('cities', 'id'),
                'departure_date' => 'required',
                'departure_time' => 'required',
                'arrival_city' => 'required', Rule::exists('cities', 'id'),
                'arrival_date' => 'required',
                'arrival_time' => 'required',
                'ticket' => 'sometimes|required|image|mimes:jpeg,png,jpg|max:2048'
            ]);

            $data = $validator->validated();

            $flight = new Flight();
            $flight->app_user_id = $request->input('app_user');
            $flight->type = $request->input('flight_type');
            $flight->airline_id = $request->input('airline');

            $flight->departure_city_id = $request->input('departure_city');
            $flight->departure_date = date('Y-m-d', strtotime($request->input('departure_date')));
            $flight->departure_time = date('H:i:s', strtotime($request->input('departure_time')));

            $flight->arrival_city_id = $request->input('arrival_city');
            $flight->arrival_date = date('Y-m-d', strtotime($request->input('arrival_date')));
            $flight->arrival_time = date('H:i:s', strtotime($request->input('arrival_time')));

            if(isset($data['ticket'])){
                $imagePath = $data['ticket']->store('public/tickets');
                $flight->ticket_image_path = $imagePath;
            }

            $flight->save();

        } elseif ($flightType === 'round-trip') {

            $validator = Validator::make($request->all(), [
                'app_user' => 'required', Rule::exists('app_users', 'id'),
                'airline' => 'required',
                'departure_city' => 'required', Rule::exists('cities', 'id'),
                'departure_date' => 'required',
                'departure_time' => 'required',
                'arrival_city' => 'required', Rule::exists('cities', 'id'),
                'arrival_date' => 'required',
                'arrival_time' => 'required',
                'return_departure_city' => 'required', Rule::exists('cities', 'id'),
                'return_departure_date' => 'required',
                'return_departure_time' => 'required',
                'return_arrival_city' => 'required', Rule::exists('cities', 'id'),
                'return_arrival_date' => 'required',
                'return_arrival_time' => 'required',
                'ticket' => 'sometimes|required|image|mimes:jpeg,png,jpg|max:2048'
            ]);

            $data = $validator->validated();

            $flight = new Flight();
            $flight->app_user_id = $request->input('app_user');
            $flight->type = $request->input('flight_type');
            $flight->airline_id = $request->input('airline');

            $flight->departure_city_id = $request->input('departure_city');
            $flight->departure_date = date('Y-m-d', strtotime($request->input('departure_date')));
            $flight->departure_time = date('H:i:s', strtotime($request->input('departure_time')));

            $flight->arrival_city_id = $request->input('arrival_city');
            $flight->arrival_date = date('Y-m-d', strtotime($request->input('arrival_date')));
            $flight->arrival_time = date('H:i:s', strtotime($request->input('arrival_time')));

            $flight->return_departure_city_id = $request->input('return_departure_city');
            $flight->return_departure_date = date('Y-m-d', strtotime($request->input('return_departure_date')));
            $flight->return_departure_time = date('H:i:s', strtotime($request->input('return_departure_time')));

            $flight->return_arrival_city_id = $request->input('return_arrival_city');
            $flight->return_arrival_date = date('Y-m-d', strtotime($request->input('return_arrival_date')));
            $flight->return_arrival_time = date('H:i:s', strtotime($request->input('return_arrival_time')));

            if(isset($data['ticket'])){
                $imagePath = $data['ticket']->store('public/tickets');
                $flight->ticket_image_path = $imagePath;
            }

            $flight->save();
        }

        if($flight) {

            $checklistTemplates = ChecklistTemplate::where(function($query) use ($flight) {
                $query->where('condition', 'all_flights')
                      ->orWhere(function($query) use ($flight) {
                          $query->where('condition', 'specific_departure_city')
                                ->where('condition_value', $flight->departure_city_id);
                      });
            })->get();
    
            // Attach checklists to the flight
            foreach ($checklistTemplates as $template) {
                $checklist = new Checklist([
                    'name' => $template->name,
                    'items' => $template->items,
                ]);
        
                $flight->checklists()->save($checklist);
            }
        }

        $appuser = $flight->app_user;

        if($appuser){
            $title = 'Flight scheduled';
            $body = 'A flight has been scheduled for you to go to '.$flight->arrival_city->name.' on '.date('j M Y', strtotime($flight->departure_date)).' at '.date('h:i A', strtotime($flight->departure_time));

            sendAndStoreNotification($appuser, $title, $body, 'flight');
        }

        // Redirect or return a response as needed
        return response()->json([
            'success' => 'Flight created successfully',
        ]);
    }

    public function updateFromAppusers(Request $request, $id){
        $formData = $request->all(); // Retrieve form data
        // dd($formData); // Inspect the form data 
        // dd($request);

        $validator = null;

        if($request->input('flight_type') == 'one-way'){
            $validator = Validator::make($request->all(), [
                'app_user' => 'required', Rule::exists('app_users', 'id'),
                'airline' => 'required',
                'departure_city' => 'required', Rule::exists('cities', 'id'),
                'departure_date' => 'required',
                'departure_time' => 'required',
                'arrival_city' => 'required', Rule::exists('cities', 'id'),
                'arrival_date' => 'required',
                'arrival_time' => 'required',
                'ticket' => 'sometimes|required|image|mimes:jpeg,png,jpg|max:2048'
            ]);
        }

        if($request->input('flight_type') == 'round-trip'){
            $validator = Validator::make($request->all(), [
                'app_user' => 'required', Rule::exists('app_users', 'id'),
                'airline' => 'required',
                'departure_city' => 'required', Rule::exists('cities', 'id'),
                'departure_date' => 'required',
                'departure_time' => 'required',
                'arrival_city' => 'required', Rule::exists('cities', 'id'),
                'arrival_date' => 'required',
                'arrival_time' => 'required',
                'return_departure_city' => 'sometimes|required', Rule::exists('cities', 'id'),
                'return_departure_date' => 'sometimes|required',
                'return_departure_time' => 'sometimes|required',
                'return_arrival_city' => 'sometimes|required', Rule::exists('cities', 'id'),
                'return_arrival_date' => 'sometimes|required',
                'return_arrival_time' => 'sometimes|required',
                'ticket' => 'sometimes|required|image|mimes:jpeg,png,jpg|max:2048'
            ]);
        } 

        $data = $validator->validated();  

        $flight = Flight::findOrFail($id);

        $flight->type = $request->input('flight_type');
        $flight->airline_id = $request->input('airline');
        $flight->departure_city_id = $request->input('departure_city');
        $flight->departure_date = date('Y-m-d', strtotime($request->input('departure_date')));
        $flight->departure_time = date('H:i:s', strtotime($request->input('departure_time')));
        $flight->arrival_city_id = $request->input('arrival_city');
        $flight->arrival_date = date('Y-m-d', strtotime($request->input('arrival_date')));
        $flight->arrival_time = date('H:i:s', strtotime($request->input('arrival_time')));

        if(isset($data['ticket'])){
            $imagePath = $data['ticket']->store('public/tickets');
            $flight->ticket_image_path = $imagePath;
        }

        //make adjustments for one-way flight
        if($request->input('flight_type') == 'one-way'){
            //reset all return fields
            $flight->return_departure_city_id = null;
            $flight->return_departure_date = null;
            $flight->return_departure_time = null;
            $flight->return_arrival_city_id = null;
            $flight->return_arrival_date = null;
            $flight->return_arrival_time = null;
        }

        //make adjustments for round trip flight
        if($request->input('flight_type') == 'round-trip'){
            $flight->return_departure_city_id = $request->input('return_departure_city');
            $flight->return_departure_date = date('Y-m-d', strtotime($request->input('return_departure_date')));
            $flight->return_departure_time = date('H:i:s', strtotime($request->input('return_departure_time')));

            $flight->return_arrival_city_id = $request->input('return_arrival_city');
            $flight->return_arrival_date = date('Y-m-d', strtotime($request->input('return_arrival_date')));
            $flight->return_arrival_time = date('H:i:s', strtotime($request->input('return_arrival_time')));
        }

        $flight->save();

        // Return a response or redirect as needed
        return response()->json([
            'success' => 'Flight edited successfully',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroyFromAppusers($id){
        $flight = Flight::findOrFail($id);

        // Delete all appointments related to the flight
        $appointments = Appointment::where('flight_id', $id)->get();
        foreach ($appointments as $appointment) {
            $appointment->delete();
        }

        // Delete related checklists
        $flight->checklists()->delete();

        // Delete the flight's image if it exists
        if ($flight->ticket_image_path) {
            Storage::delete($flight->ticket_image_path);
        }

        $flight->delete();

        return redirect()->back()->with('success', 'Flight deleted successfully');
    }

    public function showFromAppusers($id){
        $flight = Flight::findOrFail($id);
        if($flight->ticket_image_path){
            $flight['storage_ticket_image_path'] = Storage::url($flight->ticket_image_path);
        }

        return response()->json([
            'flight' => $flight,
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show($id){
        $flight = Flight::with('app_user')->findOrFail($id);
        $checklists = $flight->checklists;
        $appointments = $flight->appointments;
        $locations = Location::all();
        $checklisttemplates = ChecklistTemplate::all();
        return view('flights.show', compact('flight', 'checklists', 'appointments', 'locations', 'checklisttemplates'));
    }

    public function temporaryTicketUpload(Request $request){
        $file = $request->file('file');

        if (!$file) {
            return response()->json(['error' => 'No file uploaded'], 400);
        }

        // Store the file in the temporary directory
        $temporaryUrl = $file->store('temporary', 'public');

        // Save the temporary file metadata to the database
        TemporaryFile::create([
            'filename' => $temporaryUrl, // The filename of the temporary file
        ]);

        return response()->json(['url' => Storage::url($temporaryUrl)], 200);
    }

    public function deleteTemporaryTicketUpload(Request $request){
        $url = $request->input('url');

        // Delete the temporary file from the server
        $temporaryUrl = str_replace(Storage::url(''), '', $url);
        Storage::disk('public')->delete($temporaryUrl);

        return response()->json(['message' => 'File deleted successfully'], 200);
    }

    public function upcomingFlights(){
        $user = auth()->user();

        $upcomingFlights = $user->flights()
            ->where('departure_date', '>=', Carbon::today()) // Filter appointments for today and beyond
            ->orWhere(function ($query) {
                $query->where('departure_date', '=', Carbon::today()) // Include today's appointments with time after the current time
                    ->where('departure_time', '>=', Carbon::now()->format('H:i:s'));
            })
            ->with('departure_city.country')
            ->with('arrival_city.country')
            ->with('return_departure_city.country')
            ->with('return_arrival_city.country')
            ->get();
        
        $formattedFlights = $upcomingFlights->map(function ($flight) {
            // Format date as "day-month-year" (e.g., "10-Jun-2023")
            $departure_date = date('d M Y', strtotime($flight->departure_date));
            $departure_time = date('h:i A', strtotime($flight->departure_time));
            $arrival_date = date('d M Y', strtotime($flight->arrival_date));
            $arrival_time = date('h:i A', strtotime($flight->arrival_time));

            $departure_country = $flight->departure_city->country();
            $arrival_country = $flight->arrival_city->country();
    
            // Merge the formatted date and time back into the appointment object
            $flight->departure_city->country = $departure_country;
            $flight->departure_date = $departure_date;
            $flight->departure_time = $departure_time;
            $flight->arrival_city->country = $arrival_country;
            $flight->arrival_date = $arrival_date;
            $flight->arrival_time = $arrival_time;
    
            return $flight;
        });

        return response()->json([
            'upcoming_flights' => $formattedFlights,
        ]);
    }
    
    public function pastFlights(){
        $user = auth()->user();

        $pastFlights = $user->flights()
            ->where('departure_date', '<=', Carbon::today()) 
            ->orWhere(function ($query) {
                $query->where('departure_date', '=', Carbon::today()) 
                    ->where('departure_time', '<=', Carbon::now()->format('H:i:s'));
            })
            ->with('departure_city.country')
            ->with('arrival_city.country')
            ->with('return_departure_city.country')
            ->with('return_arrival_city.country')
            ->get();
        
        $formattedFlights = $pastFlights->map(function ($flight) {
            // Format date as "day-month-year" (e.g., "10-Jun-2023")
            $departure_date = date('d M Y', strtotime($flight->departure_date));
            $departure_time = date('h:i A', strtotime($flight->departure_time));
            $arrival_date = date('d M Y', strtotime($flight->arrival_date));
            $arrival_time = date('h:i A', strtotime($flight->arrival_time));

            $departure_country = $flight->departure_city->country();
            $arrival_country = $flight->arrival_city->country();
    
            // Merge the formatted date and time back into the appointment object
            $flight->departure_city->country = $departure_country;
            $flight->departure_date = $departure_date;
            $flight->departure_time = $departure_time;
            $flight->arrival_city->country = $arrival_country;
            $flight->arrival_date = $arrival_date;
            $flight->arrival_time = $arrival_time;
    
            return $flight;
        });

        return response()->json([
            'past_flights' => $formattedFlights,
        ]);
    }

    public function upcomingFlightsChecklists(){
        $user = auth()->user();

        $upcomingChecklists = $user->flights()
        ->with('checklists', 'departure_city')
        ->whereRaw("CONVERT_TZ(CONCAT(departure_date, ' ', departure_time), departure_city.timezone, '+00:00') >= ?", [Carbon::now()])
        ->get()
        ->flatMap(function ($flight) {
            return $flight->checklists; // Flatten the array of checklists
        });

        return response()->json([
            'upcoming_checklists' => $upcomingChecklists,
        ]);
    }

    public function getFlightWithDetails($id){
        $flight = Flight::with('checklists', 'departure_city.country', 'arrival_city.country', 'return_departure_city.country', 'return_arrival_city.country')->find($id);

        $appointments = $flight->appointments()->with('location')->get();

        $formattedAppointments = $appointments->map(function ($appointment) {
            // Format date as "day-month-year" (e.g., "10-Jun-2023")
            // $date = $appointment->date->format('d-M-Y');
            $day = date('d', strtotime($appointment->date));
            $month = date('M', strtotime($appointment->date));
            $year = date('Y', strtotime($appointment->date));
            $time = date('h:i A', strtotime($appointment->time));
    
            // Format time as "hh:mm A" (e.g., "08:00 AM")
            // $time = Carbon::createFromFormat('H:i:s', $appointment->time)->format('h:i A');
    
            // Merge the formatted date and time back into the appointment object
            $appointment->day = $day;
            $appointment->month = $month;
            $appointment->year = $year;
            $appointment->time = $time;
    
            // Remove the original date and time properties
            unset($appointment->date);
    
            return $appointment;
        });

        $flight['appointments'] = $formattedAppointments;

        // Modify the ticket image path to include the full web URL
        if($flight['ticket_image_path']){
            $flight['ticket_image_path'] = url(Storage::url($flight->ticket_image_path));
        }

        if (!$flight) {
            return response()->json(['error' => 'Flight not found'], 404);
        }

        // Check if the authenticated user owns the flight
        if (auth()->user()->id !== $flight->app_user_id) {
            return response()->json(['error' => 'Unauthorized'], 403);
        }

        return response()->json($flight);
    }
}
