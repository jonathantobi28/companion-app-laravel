<?php

namespace App\Http\Controllers;

use App\Models\Airline;
use App\Models\Appointment;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Carbon;

use Illuminate\Support\Facades\Mail;
use App\Mail\AppointmentConfirmation;
use App\Models\AppUser;
use App\Models\Checklist;
use App\Models\ChecklistTemplate;
use App\Models\Location;
use App\Notifications\PushNotification;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;

class AppointmentController extends Controller
{

    public function index(){
        return view('appointments.overview', [
            'appointments' => Appointment::paginate(20)
        ]);
    }

    public function store(Request $request){
        // dd($request);

        // Validate the incoming request data
        $validatedData = $request->validate([
            'app_user' => 'required', Rule::exists('app_users', 'id'),
            'location' => 'required', Rule::exists('locations', 'id'),
            'date' => 'required',
            'time' => 'required',
            'flight_id' => 'sometimes|exists:flights,id',
        ]);

        $appointmentData = [
            'app_user_id' => $validatedData['app_user'],
            'location_id' => $validatedData['location'],
            'date' => date('Y-m-d', strtotime($validatedData['date'])),
            'time' => date('H:i:s', strtotime($validatedData['time'])),
        ];

        if (isset($validatedData['flight_id'])) {
            $appointmentData['flight_id'] = $validatedData['flight_id'];
        }

        $appointment = Appointment::create($appointmentData);

        $appuser = AppUser::find($validatedData['app_user']);
        
        if($appuser){
            $title = 'Appointment scheduled';
            $body = 'An appointment has been created for you to go to '.$appointment->location->name.' on '.date('j M Y', strtotime($appointment->date)).' at '.date('h:i A', strtotime($appointment->time));

            sendAndStoreNotification($appuser, $title, $body, 'appointment');
        }

        // Return a response or redirect as needed
        return redirect()->back()->with('success', 'Appointment created successfully.');
    }

    public function show($id){
        $appointment = Appointment::with('appUser')->findOrFail($id);
        $checklists = $appointment->checklists;
        $checklisttemplates = ChecklistTemplate::all();
        return view('appointments.show', compact('appointment', 'checklists', 'checklisttemplates'));
    }

    private function getAppointments($type, $user){

        $appointments = [];

        if($type == 'upcoming'){
            $appointments = $user->appointments()
            ->where('date', '>=', Carbon::today()) // Filter appointments for today and beyond
            ->orWhere(function ($query) {
                $query->where('date', '=', Carbon::today()) // Include today's appointments with time after the current time
                    ->where('time', '>=', Carbon::now()->format('H:i:s'));
            })
            ->with('location')
            ->get();
        }else{
            $appointments = $user->appointments()
            ->where('date', '<=', Carbon::today()) // Filter appointments for today and beyond
            ->orWhere(function ($query) {
                $query->where('date', '=', Carbon::today()) // Include today's appointments with time after the current time
                    ->where('time', '<=', Carbon::now()->format('H:i:s'));
            })
            ->with('location')
            ->get();
        }

        return $appointments;
    }

    public function showFromAppusers($id){
        $appointment = Appointment::findOrFail($id);

        return response()->json([
            'appointment' => $appointment,
        ]);
    }

    public function storeFromAppusers(Request $request){
        // Validate the incoming request data
        $validatedData = $request->validate([
            'app_user' => 'required', Rule::exists('app_users', 'id'),
            'location' => 'required', Rule::exists('locations', 'id'),
            'date' => 'required',
            'time' => 'required',
            'flight_id' => 'sometimes|exists:flights,id',
        ]);

        $appointmentData = [
            'app_user_id' => $validatedData['app_user'],
            'location_id' => $validatedData['location'],
            'date' => date('Y-m-d', strtotime($validatedData['date'])),
            'time' => date('H:i:s', strtotime($validatedData['time'])),
        ];

        if (isset($validatedData['flight_id'])) {
            $appointmentData['flight_id'] = $validatedData['flight_id'];
        }

        $appointment = Appointment::create($appointmentData);

        if($appointment) {

            $checklistTemplates = ChecklistTemplate::where(function($query) use ($appointment) {
                $query->where('condition', 'all_appointments')
                      ->orWhere(function($query) use ($appointment) {
                          $query->where('condition', 'specific_location')
                                ->where('condition_value', $appointment->location->id);
                      });
            })->get();
    
            // Attach checklists to the flight
            foreach ($checklistTemplates as $template) {
                $checklist = new Checklist([
                    'name' => $template->name,
                    'items' => $template->items,
                ]);
        
                $appointment->checklists()->save($checklist);
            }
        }

        $appuser = AppUser::find($validatedData['app_user']);

        if($appuser){
            $title = 'Appointment scheduled';
            $body = 'An appointment has been created for you to go to '.$appointment->location->name.' on '.date('j M Y', strtotime($appointment->date)).' at '.date('h:i A', strtotime($appointment->time));
            
            sendAndStoreNotification($appuser, $title, $body, 'appointment');
        }

        // Redirect or return a response as needed
        return response()->json([
            'success' => 'Appointment created successfully',
        ]);
    }

    public function updateFromAppusers(Request $request, $id){
        // dd($request);

        // Validate the incoming request data
        $validatedData = $request->validate([
            'app_user' => 'required', Rule::exists('app_users', 'id'),
            'location' => 'required', Rule::exists('locations', 'id'),
            'date' => 'required',
            'time' => 'required',
        ]);

        $appointment = Appointment::findOrFail($id);

        $appointment->location_id = $validatedData['location'];
        $appointment->date = date('Y-m-d', strtotime($validatedData['date']));
        $appointment->time = date('H:i:s', strtotime($validatedData['time']));
        $appointment->save();

        // Return a response or redirect as needed
        return response()->json([
            'success' => 'Appointment edited successfully',
        ]);
    }

    public function destroyFromAppusers($id){
        $appointment = Appointment::findOrFail($id);

        // Delete related checklists
        $appointment->checklists()->delete();

        $appointment->delete();

        return redirect()->back()->with('success', 'Appointment deleted successfully');
    }

    public function upcomingAppointments(){
        $appuser = auth()->user();

        if (!$appuser) {
            return response()->json(['message' => 'Unauthorized'], 401);
        }

        $upcomingAppointments = $appuser->appointments()
        ->where(function ($query) {
            $query->where('date', '>', Carbon::today()) // Filter appointments for beyond today
                ->orWhere(function ($query) {
                    $query->where('date', '=', Carbon::today()) // Include today's appointments with time after the current time
                        ->where('time', '>=', Carbon::now()->format('H:i:s'));
                });
        })
        ->with('location')
        ->get();
        
        $formattedAppointments = $upcomingAppointments->map(function ($appointment) {
            // Format date as "day-month-year" (e.g., "10-Jun-2023")
            // $date = $appointment->date->format('d-M-Y');
            $day = date('d', strtotime($appointment->date));
            $month = date('M', strtotime($appointment->date));
            $year = date('Y', strtotime($appointment->date));
            $time = date('h:i A', strtotime($appointment->time));
    
            // Format time as "hh:mm A" (e.g., "08:00 AM")
            // $time = Carbon::createFromFormat('H:i:s', $appointment->time)->format('h:i A');
    
            // Merge the formatted date and time back into the appointment object
            $appointment->day = $day;
            $appointment->month = $month;
            $appointment->year = $year;
            $appointment->time = $time;
    
            // Remove the original date and time properties
            unset($appointment->date);
    
            return $appointment;
        });

        return response()->json([
            'upcoming_appointments' => $formattedAppointments,
        ]);
    }

    public function pastAppointments(){
        $user = auth()->user();

        if (!$user) {
            return response()->json(['message' => 'Unauthorized'], 401);
        }

        $pastAppointments = $user->appointments()
            ->where('date', '<=', Carbon::today()) // Filter appointments for today and beyond
            ->orWhere(function ($query) {
                $query->where('date', '=', Carbon::today()) // Include today's appointments with time after the current time
                    ->where('time', '<=', Carbon::now()->format('H:i:s'));
            })
            ->with('location')
            ->get();
        
        $formattedAppointments = $pastAppointments->map(function ($appointment) {
            // Format date as "day-month-year" (e.g., "10-Jun-2023")
            // $date = $appointment->date->format('d-M-Y');
            $day = date('d', strtotime($appointment->date));
            $month = date('M', strtotime($appointment->date));
            $year = date('Y', strtotime($appointment->date));
            $time = date('h:i A', strtotime($appointment->time));
    
            // Format time as "hh:mm A" (e.g., "08:00 AM")
            // $time = Carbon::createFromFormat('H:i:s', $appointment->time)->format('h:i A');
    
            // Merge the formatted date and time back into the appointment object
            $appointment->day = $day;
            $appointment->month = $month;
            $appointment->year = $year;
            $appointment->time = $time;
    
            // Remove the original date and time properties
            unset($appointment->date);
    
            return $appointment;
        });

        return response()->json([
            'past_appointments' => $formattedAppointments,
        ]);
    }

    public function getAppointmentRequestData(){
        $airlines = Airline::all();
        $countries = Country::all();

        return response()->json([
            'airlines' => $airlines,
            'countries' => $countries,
        ]);
    }

    public function requestAppointment(Request $request){

        $fromCountry = Country::find($request->from_country);
        $toCountry = Country::find($request->to_country);
        $airline = Airline::find($request->airline);

        // Send email
        Mail::to($request->email)
            ->cc('dev@e-gostudio.com')
            ->send(new AppointmentConfirmation($request, $fromCountry, $toCountry, $airline));
        
        // Return a response
        return response()->json(['success' => true]);
    }

    public function getAppointmentWithDetails($id){
        $appointment = Appointment::with('location', 'checklists')->find($id);

        if (!$appointment) {
            return response()->json(['error' => 'Appointment not found'], 404);
        }

        // Check if the authenticated user owns the appointment
        if (auth()->user()->id !== $appointment->app_user_id) {
            return response()->json(['error' => 'Unauthorized'], 403);
        }

        // Format date as "day-month-year" (e.g., "10-Jun-2023")
        // $date = $appointment->date->format('d-M-Y');
        $appointment->day= date('d', strtotime($appointment->date));
        $appointment->month = date('M', strtotime($appointment->date));
        $appointment->year = date('Y', strtotime($appointment->date));
        $appointment->time = date('h:i A', strtotime($appointment->time));

        // Remove the original date and time properties
        unset($appointment->date);

        return response()->json($appointment);
    }
}
