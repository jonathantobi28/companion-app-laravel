<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Sanctum\HasApiTokens;

class AppUser extends Authenticatable
{
    use HasFactory, HasApiTokens;

    protected $table = 'app_users';

    protected $fillable = [
        'name',
        'email',
        'password',
        'verification_code',
    ];

    protected static function boot()
    {
        parent::boot();

        // After a user is created, set their notification preferences to true
        static::created(function ($user) {
            $preferences = new NotificationPreference([
                'pref_one_week' => true,
                'pref_four_days' => true,
                'pref_one_day' => true,
                'pref_two_hours' => true,
            ]);

            $user->notificationPreference()->save($preferences);
        });
    }

    public function appointments()
    {
        return $this->hasMany(Appointment::class);
    }

    public function flights()
    {
        return $this->hasMany(Flight::class);
    }

    public function passports()
    {
        return $this->hasMany(Passport::class);
    }

    public function pushTokens()
    {
        return $this->hasMany(PushToken::class);
    }

    public function notificationPreference()
    {
        return $this->hasOne(NotificationPreference::class);
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class, 'app_user_id');
    }
}
