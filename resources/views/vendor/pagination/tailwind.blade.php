@if ($paginator->hasPages())
    <nav role="navigation" aria-label="{{ __('Pagination Navigation') }}" class="w-full sm:w-auto sm:mr-auto">

        <ul class="pagination relative z-0 inline-flex shadow-sm rounded-md">
            {{-- First Page Link --}}
            @if ($paginator->onFirstPage())
                <span aria-disabled="true" aria-label="{{ __('pagination.first') }}" class="page-item">
                    <span class="page-link" aria-hidden="true">
                        <i class="w-4 h-4" data-lucide="chevrons-left"></i>
                    </span>
                </span>
            @else
                <li class="page-item">
                    <a href="{{ $paginator->url(1) }}" rel="first" class="page-link" aria-label="{{ __('pagination.first') }}">
                        <i class="w-4 h-4" data-lucide="chevrons-left"></i>
                    </a>
                </li>
            @endif

            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <span aria-disabled="true" aria-label="{{ __('pagination.previous') }}" class="page-item">
                    <span class="page-link" aria-hidden="true">
                        <i class="w-4 h-4" data-lucide="chevron-left"></i>
                    </span>
                </span>
            @else
                <li class="page-item">
                    <a href="{{ $paginator->previousPageUrl() }}" rel="prev" class="page-link" aria-label="{{ __('pagination.previous') }}">
                        <i class="w-4 h-4" data-lucide="chevron-left"></i>
                    </a>
                </li>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li aria-disabled="true" class="page-item">
                        <span class="page-link">{{ $element }}</span>
                    </li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li aria-current="page" class="page-item active">
                                <span class="page-link">{{ $page }}</span>
                            </li>
                        @else
                            <li class="page-item">
                                <a href="{{ $url }}" class="page-link" aria-label="{{ __('Go to page :page', ['page' => $page]) }}">
                                    {{ $page }}
                                </a>
                            </li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li class="page-item">
                    <a href="{{ $paginator->nextPageUrl() }}" rel="next" class="page-link" aria-label="{{ __('pagination.next') }}">
                        <i class="w-4 h-4" data-lucide="chevron-right"></i>
                    </a>
                </li>
            @else
                <span aria-disabled="true" aria-label="{{ __('pagination.next') }}" class="page-item">
                    <span class="page-link" aria-hidden="true">
                        <i class="w-4 h-4" data-lucide="chevron-right"></i>
                    </span>
                </span>
            @endif

            {{-- Last Page Link --}}
            @if ($paginator->onLastPage())
                <span aria-disabled="true" aria-label="{{ __('pagination.last') }}" class="page-item">
                    <span class="page-link" aria-hidden="true">
                        <i class="w-4 h-4" data-lucide="chevrons-right"></i>
                    </span>
                </span>
            @else
                <li class="page-item">
                    <a href="{{ $paginator->url( $paginator->lastPage() ) }}" rel="last" class="page-link" aria-label="{{ __('pagination.last') }}">
                        <i class="w-4 h-4" data-lucide="chevrons-right"></i>
                    </a>
                </li>
            @endif
        </ul>
    </nav>
@endif
