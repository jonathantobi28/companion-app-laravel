@extends('layout')

@section('title', 'Edit Banner')

@section('styles')
    @parent
    <style>
        .file-input-container{
            position: relative;
            width: 100%;
            border: 1px dashed #B1B3B6;
            display: flex;
            padding: 2em;
            align-items: flex-end;
            cursor: pointer;
            border-radius: 4px;
            height: 270px;
            justify-content: center;
            overflow: hidden;
            max-width: 480px;
        }
        .file-input-container.dragover{
            border-color: var(--primary);
        }
        .file-input-container .file-preview{
            position: absolute;
            width: 100%;
            height: 100%;
            z-index: 1;
            top: 0;
        }
        .file-input-container .file-preview .delete-button{
            position: absolute;
            top: 10px;
            right: 10px;
        }
        .file-input-container .file-preview .delete-button svg{
            stroke: #fefefe;
        }
    </style>
@endsection

@section('breadcrumbs')
    @parent
    <li class="breadcrumb-item"><a href="{{route('banners.overview')}}">Banners</a></li>
    <li class="breadcrumb-item active" aria-current="page">Edit - {{ $banner->title }}</li>
@endsection

@section('content')
<form id="banner-form" method="post" action="{{route('banners.update', $banner->id)}}" enctype="multipart/form-data">
    @csrf
    @method('PUT')

    <div class="grid grid-cols-12 gap-6 mt-10">
        <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
            <h2 class="intro-y text-lg font-medium">
                Edit - {{ $banner->title }}
            </h2>
            <button class="btn btn-primary shadow-md ml-auto" type="submit">Save</button>
        </div>                      
    </div>

    <div class="pos intro-y grid grid-cols-12 gap-5 mt-5">
        <!-- BEGIN: Post Content -->
        <div class="intro-y col-span-12 box p-5">
            <div>
                <label for="title" class="block text-gray-700 font-normal mb-2">Active Status</label>
                <div class="form-switch mt-2">
                    <input type="checkbox" name="is_active" class="form-check-input" @if(old('is_active') || $banner->is_active == 1) checked @endif>
                </div>
            </div>

            <div class="mt-5">
                <label for="title" class="block text-gray-700 font-normal mb-2">Title</label>
                <input type="text" id="title" name="title" placeholder="Banner Title" class="w-full border-gray-300 rounded-md p-2 text-sm" value="{{ old('title', $banner->title) }}" required>
                @error('title')
                    <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="mt-5">
                <input type="file" id="image-input" name="image" style="display: none;" value="{{ old('image') }}" accept="image/*">
                <div id="image-dropzone" class="file-input-container">
                    <div id="preview-container" class="file-preview"></div>
                    <span class="text-slate-500">Drop file here or click to upload</span>
                </div>
                <div class="text-slate-500 mt-2">Recommended size 1280x720px</div>
                @error('image')
                    <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="mt-5">
                <label for="date-range" class="block text-gray-700 font-normal mb-2">Active period</label>
                <input type="text" data-daterange="true" class="datepicker form-control w-56 block" name="date-range" id="date-range" value="{{ old('date-range', $banner->dateRange) }}" required> 
                @error('active-period')
                    <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <!-- END: Post Content -->
    </div>  
</form>

<div class="mt-10">
    <button class="btn btn-danger mr-2 mb-2" data-tw-toggle="modal" data-tw-target="#deleteModal"> 
        <i data-lucide="trash-2" class="w-4 h-4 mr-2"></i> Delete Banner 
    </button>
</div>

  <!-- BEGIN: Delete Confirmation Modal -->
  <div id="deleteModal" class="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body p-0">
                <div class="p-5 text-center">
                    <i data-lucide="x-circle" class="w-16 h-16 text-danger mx-auto mt-3"></i> 
                    <div class="text-3xl mt-5">Are you sure?</div>
                    <div class="text-slate-500 mt-2">
                        Are you sure you want to delete this banner?
                        <br>
                        This action cannot be undone
                    </div>
                </div>
                <div class="px-5 pb-8 text-center">
                    <button type="button" data-tw-dismiss="modal" class="btn btn-outline-secondary w-24 mr-1">Cancel</button>
                    <form method="post" action="{{ route('banners.destroy', $banner->id) }}" id="deleteForm" class="w-24 inline-flex">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Delete Confirmation Modal --> 

@endsection

@section('scripts')
    @parent
    <script>
        const fileContainer = document.getElementById('image-dropzone');
        const fileInput = document.getElementById('image-input');
        const previewContainer = document.getElementById('preview-container');
        let selectedFile = null;

        function adjustAspectRatio() {
            fileContainer.style.height = `${fileContainer.offsetWidth * (9 / 16)}px`;
        }
        // Call the function on initial load and whenever the window is resized
        window.addEventListener('load', adjustAspectRatio);
        window.addEventListener('resize', adjustAspectRatio);

        fileContainer.addEventListener('click', () => {
            fileInput.click();
        });

        fileContainer.addEventListener('dragenter', (e) => {
            e.preventDefault();
            fileContainer.classList.add('dragover');
        });

        fileContainer.addEventListener('dragover', (e) => {
            e.preventDefault();
        });

        fileContainer.addEventListener('dragleave', () => {
            fileContainer.classList.remove('dragover');
        });

        fileContainer.addEventListener('drop', (e) => {
            e.preventDefault();
            fileContainer.classList.remove('dragover');
            const file = e.dataTransfer.files[0];
            // Handle the dropped file
            handleFile(file);
            fileInput.value = file;
        });

        fileInput.addEventListener('change', (e) => {
            const file = e.target.files[0];
            // Handle the selected file
            handleFile(file);
        });

        function handleFile(file) {
            // Perform desired actions with the file
            console.log('Selected file:', file);

            if (file) {
                const reader = new FileReader();
                reader.onload = (event) => {
                    selectedFile = file;
                    const filePreview = document.createElement('div');
                    filePreview.classList.add('preview-item');
                    filePreview.innerHTML = `
                        <img src="${event.target.result}" class="preview-image">
                        <button class="delete-button"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="lucide lucide-trash-2"><path d="M3 6h18"/><path d="M19 6v14c0 1-1 2-2 2H7c-1 0-2-1-2-2V6"/><path d="M8 6V4c0-1 1-2 2-2h4c1 0 2 1 2 2v2"/><line x1="10" x2="10" y1="11" y2="17"/><line x1="14" x2="14" y1="11" y2="17"/></svg></button>
                    `;
                    previewContainer.innerHTML = '';
                    previewContainer.appendChild(filePreview);

                    const deleteButton = filePreview.querySelector('.delete-button');
                    deleteButton.addEventListener('click', (e) => {
                        e.stopPropagation();
                        selectedFile = null;
                        previewContainer.innerHTML = '';
                        fileInput.value = null; // Reset the file input value
                    });
                };
                reader.readAsDataURL(file);
            }
        }

        function renderImage(){
            let imageSrc = "{{ Storage::url($banner->image_path) }}";

            const filePreview = document.createElement('div');
            filePreview.classList.add('preview-item');
            filePreview.innerHTML = `
                <img src="${imageSrc}" class="preview-image">
                <button class="delete-button"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="lucide lucide-trash-2"><path d="M3 6h18"/><path d="M19 6v14c0 1-1 2-2 2H7c-1 0-2-1-2-2V6"/><path d="M8 6V4c0-1 1-2 2-2h4c1 0 2 1 2 2v2"/><line x1="10" x2="10" y1="11" y2="17"/><line x1="14" x2="14" y1="11" y2="17"/></svg></button>
            `;
            previewContainer.innerHTML = '';
            previewContainer.appendChild(filePreview);

            const deleteButton = filePreview.querySelector('.delete-button');
            deleteButton.addEventListener('click', (e) => {
                e.stopPropagation();
                selectedFile = null;
                previewContainer.innerHTML = '';
                fileInput.value = null; // Reset the file input value
            });
        };

        window.addEventListener('load', renderImage());

    </script>
@endsection