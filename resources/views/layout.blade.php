<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="light">
    <!-- BEGIN: Head -->
    <head>
        <meta charset="utf-8">
        <link href="{{ asset('favicon.ico') }}" rel="shortcut icon">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title') - Backoffice Companion App</title>

        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/toastify-js/src/toastify.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/tw-elements/dist/css/tw-elements.min.css" />

        <!-- BEGIN: CSS Assets-->
        @section('styles')
            <link rel="stylesheet" href="{{asset('dist/css/app.min.css')}}" />
            <link rel="stylesheet" href="{{asset('dist/css/multitravel.css')}}?id={{ md5(filemtime(public_path('dist/css/multitravel.css'))) }}" />
        @show
        <!-- END: CSS Assets-->
    </head>
    <!-- END: Head -->

    <body class="py-5 md:py-0">
        <!-- BEGIN: Mobile Menu -->
        <div class="mobile-menu md:hidden">
            <div class="mobile-menu-bar">
                <a href="" class="flex mr-auto">
                    <img alt="Multitravel" class="w-6" src="{{asset('/dist/images/logo_multitravel.png')}}">
                </a>
                <a href="javascript:;" class="mobile-menu-toggler"> <i data-lucide="bar-chart-2" class="w-8 h-8 text-white transform -rotate-90"></i> </a>
            </div>
            <div class="scrollable">
                <a href="javascript:;" class="mobile-menu-toggler"> <i data-lucide="x-circle" class="w-8 h-8 text-white transform -rotate-90"></i> </a>
                <ul class="scrollable__content py-2">
                    <li>
                        <a href="{{url('backoffice/dashboard')}}" class="menu">
                            <div class="menu__icon"> <i data-lucide="file-text"></i> </div>
                            <div class="menu__title"> Dashboard </div>
                        </a>
                    </li>
                    <li>
                        <a href="administrators.html" class="side-menu">
                            <div class="menu__icon"> <i data-lucide="user-plus"></i> </div>
                            <div class="menu__title"> Administrators </div>
                        </a>
                    </li>
                    <li>
                        <a href="app-users.html" class="side-menu">
                            <div class="menu__icon"> <i data-lucide="users"></i> </div>
                            <div class="menu__title"> App users </div>
                        </a>
                    </li>
                    <li>
                        <a href="banners.html" class="side-menu">
                            <div class="menu__icon"> <i data-lucide="image"></i> </div>
                            <div class="menu__title"> Banners </div>
                        </a>
                    </li>
                    <li>
                        <a href="appointments.html" class="side-menu">
                            <div class="menu__icon"> <i data-lucide="calendar"></i> </div>
                            <div class="menu__title"> Appointments </div>
                        </a>
                    </li>
                    <li>
                        <a href="flights.html" class="side-menu">
                            <div class="menu__icon"> <i data-lucide="plane"></i> </div>
                            <div class="menu__title"> Flights </div>
                        </a>
                    </li>
                    <li>
                        <a href="checklists.html" class="side-menu">
                            <div class="menu__icon"> <i data-lucide="list-checks"></i> </div>
                            <div class="menu__title"> Checklists </div>
                        </a>
                    </li>
                    <li>
                        <a href="accomodations.html" class="side-menu">
                            <div class="menu__icon"> <i data-lucide="building"></i> </div>
                            <div class="menu__title"> Accomodations </div>
                        </a>
                    </li>
                    <li>
                        <a href="transport.html" class="side-menu">
                            <div class="menu__icon"> <i data-lucide="car"></i> </div>
                            <div class="menu__title"> Transport </div>
                        </a>
                    </li>
                    <li>
                        <a href="faq.html" class="side-menu">
                            <div class="menu__icon"> <i data-lucide="help-circle"></i> </div>
                            <div class="menu__title"> FAQ </div>
                        </a>
                    </li>
                    <li>
                        <a href="settings.html" class="side-menu">
                            <div class="menu__icon"> <i data-lucide="settings"></i> </div>
                            <div class="menu__title"> Settings </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- END: Mobile Menu -->

        <!-- BEGIN: Top Bar -->
        <div class="top-bar-boxed h-[70px] md:h-[65px] z-[51] border-b border-white/[0.08] mt-12 md:mt-0 -mx-3 sm:-mx-8 md:-mx-0 px-3 md:border-b-0 relative md:fixed md:inset-x-0 md:top-0 sm:px-8 md:px-10 md:pt-10 md:bg-gradient-to-b md:from-slate-100 md:to-transparent dark:md:from-darkmode-700">
            <div class="h-full flex items-center">
                <!-- BEGIN: Logo -->
                <a href="" class="logo -intro-x hidden md:flex xl:w-[180px] block">
                    <img alt="logo" class="logo__image w-24" src="{{ asset('/dist/images/logo_multitravel.png') }}">
                </a>
                <!-- END: Logo -->
                <!-- BEGIN: Breadcrumb -->
                <nav aria-label="breadcrumb" class="-intro-x h-[45px] mr-auto">
                    <ol class="breadcrumb breadcrumb-light">
                        @section('breadcrumbs')
                            <li class="breadcrumb-item">Backoffice</li>
                        @show
                    </ol>
                </nav>
                <!-- END: Breadcrumb -->
                <!-- BEGIN: Notifications -->
                <div class="intro-x dropdown mr-4 sm:mr-6">
                    <div class="dropdown-toggle notification notification--bullet cursor-pointer" role="button" aria-expanded="false" data-tw-toggle="dropdown"> <i data-lucide="bell" class="notification__icon dark:text-slate-500"></i> </div>
                    <div class="notification-content pt-2 dropdown-menu">
                        <div class="notification-content__box dropdown-content">
                            <div class="notification-content__title">Notifications</div>
                            <div class="cursor-pointer relative flex items-center ">
                                <div class="w-12 h-12 flex-none image-fit mr-1">
                                    <img alt="Midone - HTML Admin Template" class="rounded-full" src="{{ asset('/dist/images/avatar.png') }}">
                                    <div class="w-3 h-3 bg-success absolute right-0 bottom-0 rounded-full border-2 border-white"></div>
                                </div>
                                <div class="ml-2 overflow-hidden">
                                    <div class="flex items-center">
                                        <a href="javascript:;" class="font-medium truncate mr-5">Robert De Niro</a> 
                                        <div class="text-xs text-slate-400 ml-auto whitespace-nowrap">01:10 PM</div>
                                    </div>
                                    <div class="w-full truncate text-slate-500 mt-0.5">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END: Notifications -->
                <!-- BEGIN: Account Menu -->
                <div class="intro-x dropdown w-8 h-8">
                    <div class="dropdown-toggle w-8 h-8 rounded-full overflow-hidden shadow-lg image-fit zoom-in scale-110" role="button" aria-expanded="false" data-tw-toggle="dropdown">
                        @if (auth()->user()->thumbnail_URI)
                            <img src="{{ asset( auth()->user()->thumbnail_URI) }}" alt="User Thumbnail">
                        @else
                            <img src="{{ asset('/dist/images/avatar.png') }}" alt="Thumbnail Placeholder">
                        @endif
                    </div>
                    <div class="dropdown-menu w-56">
                        <ul class="dropdown-content bg-primary/80 before:block before:absolute before:bg-black before:inset-0 before:rounded-md before:z-[-1] text-white">
                            <li class="p-2">
                                @if(auth())
                                    <div class="font-medium">{{ auth()->user()->name }}</div>
                                @endif
                            </li>
                            <li>
                                <hr class="dropdown-divider border-white/[0.08]">
                            </li>
                            <li>
                                <a href="{{route('logout')}}" class="dropdown-item hover:bg-white/5"> <i data-lucide="toggle-right" class="w-4 h-4 mr-2"></i> Logout </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- END: Account Menu -->
            </div>
        </div>
        <!-- END: Top Bar -->

        <div class="flex overflow-hidden">
            <!-- BEGIN: Side Menu -->
            <nav class="side-nav">
                <ul>
                    <li>
                        <a href="{{ route('dashboard') }}" class="side-menu {{ request()->is('dashboard') ? 'side-menu--active' : '' }}">
                            <div class="side-menu__icon"> <i data-lucide="home"></i> </div>
                            <div class="side-menu__title"> Dashboard </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('administrators.overview') }}" class="side-menu {{ request()->is('administrators', 'administrators/*') ? 'side-menu--active' : '' }}">
                            <div class="side-menu__icon"> <i data-lucide="user-plus"></i> </div>
                            <div class="side-menu__title"> Administrators </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('appusers.overview') }}" class="side-menu {{ request()->is('appusers', 'appusers/*') ? 'side-menu--active' : '' }}">
                            <div class="side-menu__icon"> <i data-lucide="users"></i> </div>
                            <div class="side-menu__title"> App users </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('banners.overview') }}" class="side-menu {{ request()->is('banners', 'banners/*') ? 'side-menu--active' : '' }}">
                            <div class="side-menu__icon"> <i data-lucide="image"></i> </div>
                            <div class="side-menu__title"> Banners </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('appointments.overview') }}" class="side-menu {{ request()->is('appointments', 'appointments/*') ? 'side-menu--active' : '' }}">
                            <div class="side-menu__icon"> <i data-lucide="calendar"></i> </div>
                            <div class="side-menu__title"> Appointments </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('flights.overview') }}" class="side-menu {{ request()->is('flights', 'flights/*') ? 'side-menu--active' : '' }}">
                            <div class="side-menu__icon"> <i data-lucide="plane"></i> </div>
                            <div class="side-menu__title"> Flights </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('checklisttemplates.overview') }}" class="side-menu {{ request()->is('checklisttemplates', 'checklisttemplates/*') ? 'side-menu--active' : '' }}">
                            <div class="side-menu__icon"> <i data-lucide="list-checks"></i> </div>
                            <div class="side-menu__title"> Checklists </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="side-menu">
                            <div class="side-menu__icon"> <i data-lucide="building"></i> </div>
                            <div class="side-menu__title"> Accomodations </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="side-menu">
                            <div class="side-menu__icon"> <i data-lucide="car"></i> </div>
                            <div class="side-menu__title"> Transport </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="side-menu">
                            <div class="side-menu__icon"> <i data-lucide="help-circle"></i> </div>
                            <div class="side-menu__title"> FAQ </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="side-menu">
                            <div class="side-menu__icon"> <i data-lucide="settings"></i> </div>
                            <div class="side-menu__title"> Settings </div>
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- END: Side Menu -->

            <!-- BEGIN: Content -->
            <div class="content">
                @yield('content')
            </div>
            <!-- END: Content -->

        </div>
        
        <!-- BEGIN: JS Assets-->
        @section('scripts')
            <script src="https://cdn.jsdelivr.net/npm/tw-elements/dist/js/tw-elements.umd.min.js"></script>
            <script src="{{asset('dist/js/moment.min.js')}}"></script>
            <script src="{{asset('dist/js/moment-timezone.min.js')}}"></script>
            <script src="{{asset('dist/js/app.min.js')}}"></script>
            {{-- <script src="{{asset('dist/js/custom.js')}}"></script> --}}
        @show
        <!-- END: JS Assets-->

        @if (session()->has('success'))
            <!-- BEGIN: Notification Content --> 
            <div id="notification-content" class="toastify-content hidden flex"> 
                <div class="font-medium">{{ session('success')}}</div> 
            </div> 
            <!-- END: Notification Content -->

            <script>
                // Show notification 
                Toastify({ 
                node: $("#notification-content").clone().removeClass("hidden")[0], 
                duration: 3000, 
                newWindow: true, 
                close: true, 
                gravity: "bottom", 
                position: "left", 
                backgroundColor: "white", 
                stopOnFocus: true, }).showToast();
            </script>
        @endif

    </body>
</html>