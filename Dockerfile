FROM docker.io/bitnami/laravel:9

# Copy the application code to the container
COPY . .

# Install dependencies
RUN composer install --no-interaction --no-scripts --optimize-autoloader --no-dev

# Run migrations
# Fresh Migration
RUN php artisan migrate:fresh --seed 

# Update Migration
# RUN php artisan migrate

# Generates the encryption keys Passport needs in order to generate access tokens
# RUN php artisan passport:keys

# Start installing dependencies
RUN yarn install

# Run the compiler
RUN yarn run prod

# Start the Laravel development server
CMD php artisan serve --host=0.0.0.0

# Expose port 8000 for the Laravel development server
EXPOSE 8000