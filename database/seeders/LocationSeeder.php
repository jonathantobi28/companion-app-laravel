<?php

namespace Database\Seeders;

use App\Models\Location;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class LocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Location::create([
            'name' => 'Multitravel',
            'address' => 'Kleine Waterstraat 15',
            'phone' => '+597 421163',
        ]);

        Location::create([
            'name' => 'Dutch Embassy',
            'address' => 'Van Roseveltkade 5',
            'phone' => '+597 477211',
        ]);

        Location::create([
            'name' => 'Embassy of India',
            'address' => 'Dr Sophoe Redmond St 239',
            'phone' => '+597 498344',
        ]);

        Location::create([
            'name' => 'Embassy of the United States of America',
            'address' => 'Kristal St 165',
            'phone' => '+597 472900',
        ]);
    }
}
