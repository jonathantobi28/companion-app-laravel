<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use Illuminate\Notifications\Notification;
use YieldStudio\LaravelExpoNotifier\ExpoNotificationsChannel;
use YieldStudio\LaravelExpoNotifier\Dto\ExpoMessage;

class PushNotification extends Notification
{
    use Queueable;

    private $title;
    private $body;

    /**
     * Create a new notification instance.
     */
    public function __construct($title, $body)
    {
        $this->title = $title;
        $this->body = $body;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return [ExpoNotificationsChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toExpoNotification(object $notifiable): ExpoMessage
    {
        return (new ExpoMessage())
            ->to($notifiable->pushTokens->pluck('token')->toArray())
            ->title($this->title)
            ->body($this->body)
            ->channelId('default');
    }

}
