@extends('layout')

@section('title', 'Banners')

@section('breadcrumbs')
    @parent
    <li class="breadcrumb-item active" aria-current="page">Banners</li>
@endsection

@section('content')
<h2 class="intro-y text-lg font-medium mt-10">
    Banners
</h2>
<div class="grid grid-cols-12 gap-6 mt-5 mb-10">
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
        <a href="{{ route('banners.create') }}" class="btn btn-primary shadow-md mr-2">New Banner</a>
        {{-- <div class="hidden md:block mx-auto text-slate-500">Showing 1 to 10 of 5 entries</div> --}}
    </div>
</div>

@if ($banners->isEmpty())
<div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
    <div class="font-normal">No Banners Currently</div>
</div>
@else
    <div class="grid grid-cols-2 gap-4">

        @foreach ($banners as $banner)

        <a href="{{ route('banners.show', $banner->id) }}" class="bg-gray-200 rounded-lg p-4 shadow-md">
            <div class="relative h-full">
                <img src="{{ Storage::url($banner->image_path) }}" alt="Image 1" class="object-cover rounded-lg h-full w-full">
                <div style="position: absolute; top: 0; left: 0; width: 100%; height: 75.5px; background: linear-gradient(180deg, #2A5D57 0%, rgba(217, 217, 217, 0) 100%); border-radius: 10px 10px 0 0"></div>
                <div class="absolute top-0 left-0 mt-2 ml-2">
                    <div class="flex items-center icon">
                        <i data-lucide="eye" class="w-4 h-4 mr-1"></i>
                        <span>{{ $banner->views }}</span>
                    </div>
                </div>
                <div class="absolute top-0 right-0 mt-2 mr-2">
                    <!-- Delete icon -->
                    <button class="flex items-center icon" data-tw-toggle="modal" data-tw-target="#deleteModal" onclick="prepareDeletion(event, {{$banner->id}})"> 
                        <i data-lucide="trash-2" class="w-4 h-4 mr-1"></i> 
                    </button>
                </div>
            </div>
            <!-- Rest of the content -->
        </a>

            {{-- <div class="bg-gray-200 rounded-lg p-4 shadow-md">
                <div class="relative h-full">
                    <img src="{{ Storage::url($banner->image_path) }}" alt="Image 1" class="object-cover rounded-lg h-full w-full">
                        <div class="absolute top-0 left-0 mt-2 ml-2">
                        <!-- Watch or eye icon with dummy numbers -->
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" icon-name="eye" data-lucide="eye" class="lucide lucide-eye icon"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg>
                            <div class="absolute top-0 left-2 p-1" id="icon-view">
                                <p class="icon">{{ $banner->views }}</p>
                            </div>
                        </div>
                    <div class="absolute top-0 right-0 mt-2 mr-2">
                        <!-- Delete icon -->
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" icon-name="trash-2" data-lucide="trash-2" class="lucide lucide-trash-2 icon"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 01-2 2H7a2 2 0 01-2-2V6m3 0V4a2 2 0 012-2h4a2 2 0 012 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                    </div>
                </div>
                <!-- Rest of the content -->
            </div> --}}
        @endforeach

        
    </div>
@endif

<!-- END: Content -->

<!-- BEGIN: Delete Confirmation Modal -->
<div id="deleteModal" class="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body p-0">
                <div class="p-5 text-center">
                    <i data-lucide="x-circle" class="w-16 h-16 text-danger mx-auto mt-3"></i> 
                    <div class="text-3xl mt-5">Are you sure?</div>
                    <div class="text-slate-500 mt-2">
                        Are you sure you want to delete this banner?
                        <br>
                        This action cannot be undone
                    </div>
                </div>
                <div class="px-5 pb-8 text-center">
                    <button type="button" data-tw-dismiss="modal" class="btn btn-outline-secondary w-24 mr-1">Cancel</button>
                    <form method="post" action="" id="deleteForm" class="w-24 inline-flex">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Delete Confirmation Modal --> 
<script>
    function prepareDeletion(event, id){
        event.preventDefault();
        document.getElementById('deleteForm').action =`banners/${id}/delete`;
    }
</script>
@endsection