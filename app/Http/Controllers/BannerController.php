<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use App\Models\TemporaryFile;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(){
        $banners = Banner::all();
        return view('banners.overview', compact('banners'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('banners.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        
        // $formData = $request->all(); // Retrieve form data
        // dd($formData); // Inspect the form data 

        // Validate the incoming request data
        $validator = Validator::make($request->all(), [
            'image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'title' => 'required',
            'date-range' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $data = $validator->validated();

        // Split the date range
        $dateParts = explode(' - ', $data['date-range']);
        $activeFrom = date('Y-m-d', strtotime($dateParts[0]));
        $activeUntil = date('Y-m-d', strtotime($dateParts[1]));

        // Upload the image file
        $imagePath = $data['image']->store('public/banners');

        // Create a new banner
        Banner::create([
            'image_path' => $imagePath,
            'title' => $data['title'],
            'active_from' => $activeFrom,
            'active_until' => $activeUntil,
        ]);

        // Redirect to the banners overview page or show a success message
        return redirect()->route('banners.overview')->with('success', 'Banner created successfully');
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $banner = Banner::findOrFail($id);
        return view('banners.show', compact('banner'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $banner = Banner::findOrFail($id);
        // Format the date range as strings in the desired format
        $banner->dateRange = $banner->active_from . ' - ' . $banner->active_until;
        return view('banners.edit', compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        // Validate the request data
        $request->validate([
            'title' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg|max:2048',
            'date-range' => 'required',
        ]);

        // Retrieve the banner from the database
        $banner = Banner::findOrFail($id);

        // Update the banner properties
        $banner->is_active = $request->has('is_active');
        $banner->title = $request->input('title');

        // Check if a new image is uploaded
        if ($request->hasFile('image')) {
            // Delete the old image if it exists
            if ($banner->image_path) {
                Storage::delete($banner->image_path);
            }

            // Upload and save the new image
            $image = $request->file('image');
            $path = $image->store('banners', 'public');
            $banner->image_path = $path;
        }

        // Update the active_from and active_until dates based on the date range input
        $dateRange = $request->input('date-range');

        // Split the date range
        $dateParts = explode(' - ', $dateRange);
        $activeFrom = date('Y-m-d', strtotime($dateParts[0]));
        $activeUntil = date('Y-m-d', strtotime($dateParts[1]));

        // Set new from and untile date
        $banner->active_from = $activeFrom;
        $banner->active_until = $activeUntil;

        // Save the updated banner
        $banner->save();

        // Redirect back with a success message
        return redirect()->route('banners.show', $banner->id)->with('success', 'Banner updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id){
        $banner = Banner::findOrFail($id);

        // Delete the banner's image if it exists
        if ($banner->image_path) {
            Storage::delete($banner->image_path);
        }

        $banner->delete();

        return redirect()->route('banners.overview')->with('success', 'Banner deleted successfully');
    }

    public function activeBanners(){
        $currentDate = Carbon::now();
        
        $activeBanners = Banner::where('is_active', true)
                            ->whereDate('active_from', '<=', $currentDate)
                            ->whereDate('active_until', '>=', $currentDate)
                            ->get();

        // Modify the image path to include the full web URL
        $activeBanners->transform(function ($banner) {
            $banner->image_path = url(Storage::url($banner->image_path));
            return $banner;
        });
        
        return response()->json($activeBanners);
    }

    public function temporaryUpload(Request $request){
        $file = $request->file('file');
        if (!$file) {
            return response()->json(['error' => 'No file uploaded'], 400);
        }

        // Store the file in the temporary directory
        $temporaryUrl = $file->store('temporary', 'public');

        // Save the temporary file metadata to the database
        TemporaryFile::create([
            'filename' => $temporaryUrl, // The filename of the temporary file
        ]);

        return response()->json(['url' => Storage::url($temporaryUrl)], 200);
    }

    public function deleteTemporaryUpload(Request $request){
        $url = $request->input('url');

        // Delete the temporary file from the server
        $temporaryUrl = str_replace(Storage::url(''), '', $url);
        Storage::disk('public')->delete($temporaryUrl);

        return response()->json(['message' => 'File deleted successfully'], 200);
    }
}
