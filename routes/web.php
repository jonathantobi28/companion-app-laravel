<?php

use App\Http\Controllers\AppointmentController;
use App\Http\Controllers\AppUserController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\BannerController;
use App\Http\Controllers\ChecklistTemplateController;
use App\Http\Controllers\ChecklistController;
use App\Http\Controllers\FlightController;
use App\Http\Controllers\LocationController;
use App\Models\AppUser;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::redirect('/', '/dashboard', 301);

Route::get('/login', function(){
    return view('login');
})->name('login');

Route::post('/login', [UserController::class, 'login'])->name('login.execute');


Route::group(['middleware' => ['auth']], function() {

    Route::get('/dashboard', function(){
        return view('dashboard');
    })->name('dashboard');

    Route::get('/logout', [UserController::class, 'logout'])->name('logout');

    Route::get('/administrators', [UserController::class, 'index'])->name('administrators.overview');
    Route::get('/administrators/create', [UserController::class, 'create'])->name('administrators.create');
    Route::post('/administrators/create', [UserController::class, 'store'])->name('administrators.store');
    Route::get('/administrators/bewerk/{administrator}', [UserController::class, 'edit']);
    Route::patch('/administrators/bewerk/{administrator}', [UserController::class, 'update']);
    Route::delete('/administrators/verwijder/{administrator}', [UserController::class, 'destroy']);

    Route::get('/appusers', [AppUserController::class, 'index'])->name('appusers.overview');
    Route::get('/appusers/search', [AppUserController::class, 'liveSearch'])->name('appusers.search');
    Route::get('/appusers/create', [AppUserController::class, 'create'])->name('appusers.create');
    Route::post('/appusers', [AppUserController::class, 'store'])->name('appusers.store');
    Route::get('/appusers/{id}', [AppUserController::class, 'show'])->name('appusers.show');
    Route::get('/appusers/{id}/edit', [AppUserController::class, 'edit'])->name('appusers.edit');
    Route::put('/appusers/{id}/edit', [AppUserController::class, 'update'])->name('appusers.update');
    Route::patch('/appusers/{id}/deactivate', [AppUserController::class, 'deactivate'])->name('appusers.deactivate');
    Route::patch('/appusers/{id}/activate', [AppUserController::class, 'activate'])->name('appusers.activate');

    Route::get('/banners', [BannerController::class, 'index'])->name('banners.overview');
    Route::get('/banners/create', [BannerController::class, 'create'])->name('banners.create');
    Route::post('/banners/create', [BannerController::class, 'store'])->name('banners.store'); 
    Route::get('/banners/{id}', [BannerController::class, 'show'])->name('banners.show');
    Route::get('banners/{id}/edit', [BannerController::class, 'edit'])->name('banners.edit');
    Route::put('banners/{id}/edit', [BannerController::class, 'update'])->name('banners.update');
    Route::delete('banners/{id}/delete', [BannerController::class, 'destroy'])->name('banners.destroy');
    Route::post('banners/create/temp', [BannerController::class, 'temporaryUpload'])->name('banners.temporary.store');
    Route::delete('banners/delete/temp', [BannerController::class, 'deleteTemporaryUpload'])->name('banners.temporary.delete');

    Route::get('/appointments', [AppointmentController::class, 'index'])->name('appointments.overview');
    Route::post('/appointments', [AppointmentController::class, 'store'])->name('appointments.store');
    Route::get('/appointments/{id}', [AppointmentController::class, 'show'])->name('appointments.show');
    Route::get('appusers/appointments/{id}', [AppointmentController::class, 'showFromAppusers'])->name('appusers.appointments.show');
    Route::post('appusers/appointments/create', [AppointmentController::class, 'storeFromAppusers'])->name('appusers.appointments.store');
    Route::put('appusers/appointments/{id}/edit', [AppointmentController::class, 'updateFromAppusers'])->name('appusers.appointments.update');
    Route::delete('appusers/appointments/{id}/delete', [AppointmentController::class, 'destroyFromAppusers'])->name('appusers.appointments.destroy');

    Route::post('/locations', [LocationController::class, 'store'])->name('locations.store');

    Route::get('/flights', [FlightController::class, 'index'])->name('flights.overview');
    Route::get('/flights/create', [FlightController::class, 'create'])->name('flights.create');
    Route::post('/flights/create', [FlightController::class, 'store'])->name('flights.store');
    Route::get('/flights/{id}', [FlightController::class, 'show'])->name('flights.show');
    Route::get('/flights/{id}/edit', [FlightController::class, 'edit'])->name('flights.edit');
    Route::post('/ticket/temp', [FlightController::class, 'temporaryTicketUpload'])->name('tickets.temporary.store');
    Route::delete('/ticket/delete/temp', [FlightController::class, 'deleteTemporaryTicketUpload'])->name('tickets.temporary.delete');
    Route::get('appusers/flights/{id}', [FlightController::class, 'showFromAppusers'])->name('appusers.flights.show');
    Route::post('appusers/flights/create', [FlightController::class, 'storeFromAppusers'])->name('appusers.flights.store');
    Route::put('appusers/flights/{id}/edit', [FlightController::class, 'updateFromAppusers'])->name('appusers.flights.update');
    Route::delete('appusers/flights/{id}/delete', [FlightController::class, 'destroyFromAppusers'])->name('appusers.flights.destroy');

    Route::get('/checklisttemplates', [ChecklistTemplateController::class, 'index'])->name('checklisttemplates.overview');
    Route::get('/checklisttemplates/create', [ChecklistTemplateController::class, 'create'])->name('checklisttemplates.create');
    Route::post('/checklisttemplates/create', [ChecklistTemplateController::class, 'store'])->name('checklisttemplates.store'); 
    Route::get('/checklisttemplates/{id}/edit', [ChecklistTemplateController::class, 'edit'])->name('checklisttemplates.edit'); 
    Route::put('/checklisttemplates/{id}/edit', [ChecklistTemplateController::class, 'update'])->name('checklisttemplates.update'); 
    Route::delete('/checklisttemplates/{id}/delete', [ChecklistTemplateController::class, 'destroy'])->name('checklisttemplates.destroy'); 

    Route::post('/checklists/create', [ChecklistController::class, 'store'])->name('checklists.store'); 
    Route::get('/checklists/{id}/edit', [ChecklistController::class, 'edit'])->name('checklists.edit'); 
    Route::put('/checklists/{id}/edit', [ChecklistController::class, 'update'])->name('checklists.update'); 
    Route::delete('/checklists/{id}/delete', [ChecklistController::class, 'destroy'])->name('checklists.destroy'); 

});

/**
 * For security reasons we'll use the storage folder
 * for the content
 * Info: Just make sure the laravel project folder is outside
 * the html/apache root folder and is unaccessible by the web client
 *
 */

Route::get('storage/{path}', function ($path) {
    $path = storage_path('app/public/' . $path);

    if (file_exists($path)) return response()->file($path);
    abort(404);
})->where('path', '.*');

