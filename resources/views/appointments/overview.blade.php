@extends('layout')

@section('title', 'Appointments')

@section('breadcrumbs')
    @parent
    <li class="breadcrumb-item active" aria-current="page">Appointments</li>
@endsection

@section('content')
<h2 class="intro-y text-lg font-medium mt-10">
    Appointments
</h2>
{{-- <div class="grid grid-cols-12 gap-6 mt-5 mb-10">
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
        <a href="#" class="btn btn-primary shadow-md mr-2">New Appointment</a>
    </div>
</div> --}}

<div class="grid grid-cols-12 gap-6 mt-5">
    <!-- BEGIN: Data List -->
    <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
        <table class="table table-report -mt-2">
            <thead>
                <tr>
                    <th class="whitespace-nowrap">Date</th>
                    <th class="whitespace-nowrap">Name</th>
                    <th class="whitespace-nowrap">Location</th>
                    {{-- <th class="text-center whitespace-nowrap">ACTIONS</th> --}}
                </tr>
            </thead>
            <tbody>
                @foreach ($appointments as $appointment)
                    <tr class="intro-x">
                        <td>
                            <div class="">
                                <p class="font-medium">{{ date('j M Y', strtotime($appointment->date)) }}</p>
                                <span class="text-slate-500">{{ date('h:i A', strtotime($appointment->time)) }}</span>
                            </div>  
                        </td>
                        <td>
                            <span class="font-medium whitespace-nowrap">{{ $appointment->appUser->name }}</span>   
                        </td>
                        <td>
                            <span class="font-medium whitespace-nowrap">{{ $appointment->location->name }}</span>   
                        </td>
                        {{-- <td class="table-report__action w-56">
                            <div class="flex justify-center items-center">
                                <a class="flex items-center mr-3" href="#"> 
                                    <i data-lucide="eye" class="w-4 h-4 mr-1"></i> View 
                                </a>
                            </div>
                        </td> --}}
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<!-- END: Content -->

@endsection