<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('flights', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('app_user_id');
            $table->string('type');
            $table->unsignedBigInteger('airline_id');

            $table->unsignedBigInteger('departure_city_id');
            $table->date('departure_date');
            $table->time('departure_time');

            $table->unsignedBigInteger('arrival_city_id');
            $table->date('arrival_date');
            $table->time('arrival_time');

            $table->unsignedBigInteger('return_departure_city_id')->nullable();
            $table->date('return_departure_date')->nullable();
            $table->time('return_departure_time')->nullable();

            $table->unsignedBigInteger('return_arrival_city_id')->nullable();
            $table->date('return_arrival_date')->nullable();
            $table->time('return_arrival_time')->nullable();
            
            $table->string('ticket_image_path')->nullable();
            
            $table->foreign('airline_id')->references('id')->on('airlines');
            $table->foreign('departure_city_id')->references('id')->on('cities');
            $table->foreign('arrival_city_id')->references('id')->on('cities');
            $table->foreign('return_departure_city_id')->references('id')->on('cities');
            $table->foreign('return_arrival_city_id')->references('id')->on('cities');

            $table->unsignedBigInteger('appointment_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('flights');
    }
};
