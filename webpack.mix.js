const mix = require("laravel-mix");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .js("resources/src/js/app.js", "public/dist/js")
    .js("resources/src/js/ckeditor-classic.js", "public/dist/js")
    .js("resources/src/js/ckeditor-inline.js", "public/dist/js")
    .js("resources/src/js/ckeditor-balloon.js", "public/dist/js")
    .js("resources/src/js/ckeditor-balloon-block.js", "public/dist/js")
    .js("resources/src/js/ckeditor-document.js", "public/dist/js")
    .css("resources/dist/css/_app.css", "public/dist/css/app.css")
    .css("resources/css/app.css", "public/dist/css/multitravel.css")
    .minify(['public/dist/js/app.js', 'public/dist/css/app.css'])
    .options({
        processCssUrls: false,
    })
    .copyDirectory("resources/src/json", "public/dist/json")
    .copyDirectory("resources/src/fonts", "public/dist/fonts")
    .copyDirectory("resources/src/images", "public/dist/images");
