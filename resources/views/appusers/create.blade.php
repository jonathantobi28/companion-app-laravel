@extends('layout')

@section('title', 'New App User')

@section('breadcrumbs')
    @parent
    <li class="breadcrumb-item"><a href="{{ route('appusers.overview') }}">App Users</a></li>
    <li class="breadcrumb-item active" aria-current="page">New</li>
@endsection

@section('content')
<form id="appuser-form" method="post" action="{{ route('appusers.store') }}" enctype="multipart/form-data">
    @csrf
    <div class="grid grid-cols-12 gap-6 mt-10">
        <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
            <h2 class="intro-y text-lg font-medium">
                Add New App User
            </h2>
            <button class="btn btn-primary shadow-md ml-auto" type="submit">Save</button>
        </div>                      
    </div>
    <div class="pos intro-y grid grid-cols-12 gap-5 mt-5">
        <!-- BEGIN: Post Content -->
        <div class="intro-y col-span-12 box p-5">
            <div class="w-1/2">
                <div>
                    <label for="name" class="block text-gray-700 font-normal mb-2">Fullname</label>
                    <input type="text" id="name" name="name" placeholder="Fullname" class="w-full border-gray-300 rounded-md p-2 text-sm" value="{{ old('name') }}" required>
                    @error('name')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
    
                <div class="mt-5">
                    <label for="email" class="block text-gray-700 font-normal mb-2">E-mail</label>
                    <input type="email" id="email" name="email" placeholder="E-mail" class="w-full border-gray-300 rounded-md p-2 text-sm" value="{{ old('email') }}" required>
                    @error('email')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
        </div>
        <!-- END: Post Content -->
    </div>  
</form>
@endsection