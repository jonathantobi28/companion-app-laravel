@extends('emails.layout')

@section('content')

<h1 style="text-align: center;color:#1d1d1d">Registration Succesfull!</h1>
<p style="text-align: center;color:#1d1d1d">Your account for the Mulitravel Companion was successfully created. Enjoy your travel Companion!</p>

<p><small style="text-align: center;color:#1d1d1d;">If you didn't register for an account, please get in touch straight away</small></p>
@endsection