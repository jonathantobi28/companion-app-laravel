@extends('layout')

@section('title', 'App Users')

@section('breadcrumbs')
    @parent
    <li class="breadcrumb-item active" aria-current="page">App Users</li>
@endsection

@section('content')
<h2 class="intro-y text-lg font-medium mt-10">
    App Users
</h2>
<div class="grid grid-cols-12 gap-6 mt-5 mb-10">
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
        <a href="{{ route('appusers.create') }}" class="btn btn-primary shadow-md mr-2">New App User</a>
        <div class="hidden md:block mx-auto text-slate-500">
            {{ $appusers->links('pagination::tailwind-entries') }}
        </div>
        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            <div id="search-input-container" class="w-56 relative text-slate-500">
                <input type="text" id="searchInput" class="form-control w-56 box pr-10" placeholder="Search...">
                <i id="search-icon" class="w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0" data-lucide="search"></i> 
                <a id="search-clear" href="{{ route('appusers.overview') }}" class="hidden absolute my-auto inset-y-0 mr-3 right-0 flex items-center">
                    <i class="w-4 h-4" data-lucide="x"></i> 
                </a>
            </div>
        </div>
    </div>
</div>

<div class="grid grid-cols-12 gap-6 mt-5">
    <!-- BEGIN: Data List -->
    <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
        <table class="table table-report -mt-2">
            <thead>
                <tr>
                    <th class="whitespace-nowrap">NAME</th>
                    <th class="whitespace-nowrap">EMAIL</th>
                    <th class="text-center whitespace-nowrap">STATUS</th>
                    <th class="text-center whitespace-nowrap">ACTIONS</th>
                </tr>
            </thead>
            <tbody id="appusers-table-body">
                @foreach ($appusers as $appuser)
                    <tr class="intro-x">
                        <td>
                            <span class="font-medium whitespace-nowrap">{{$appuser->name}}</span>   
                        </td>
                        <td>
                            <span class="font-medium whitespace-nowrap">{{$appuser->email}}</span>   
                        </td>
                        <td class="w-40">
                            @if ($appuser->active)
                                <div class="flex items-center justify-center text-success"> 
                                     Active 
                                </div>
                            @else
                                <div class="flex items-center justify-center text-danger"> 
                                    Inactive 
                                </div>
                            @endif
                        </td>
                        <td class="table-report__action w-56">
                            <div class="flex justify-center items-center">
                                <a class="flex items-center mr-3" href="{{ route('appusers.show', $appuser->id) }}"> 
                                    <i data-lucide="eye" class="w-4 h-4 mr-1"></i> View 
                                </a>
                                <a class="flex items-center mr-3" href="{{ route('appusers.edit', $appuser->id) }}"> 
                                    <i data-lucide="check-square" class="w-4 h-4 mr-1"></i> Edit 
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- END: Data List -->
    <!-- BEGIN: Pagination -->
    {{ $appusers->links('pagination::tailwind') }}
    <!-- END: Pagination -->
</div>
<!-- END: Content -->

@endsection

@section('scripts')
    @parent
    <script>
        const searchInput = document.getElementById('searchInput');
        const searchIcon = document.getElementById('search-icon');
        const searchClear = document.getElementById('search-clear')

        searchInput.addEventListener('input', function () {
            const searchTerm = searchInput.value;
            const searchContainer = document.getElementById('appusers-table-body');

            searchIcon.classList.add('hidden');
            searchClear.classList.remove('hidden');
            
            if (searchTerm.trim() !== '') {
                fetch(`/appusers/search?query=${searchTerm}`)
                    .then(response => response.text())
                    .then(data => {
                        searchContainer.innerHTML = data;
                    });
            }
        });
    </script>
@endsection