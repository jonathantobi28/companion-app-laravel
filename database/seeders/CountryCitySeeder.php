<?php

namespace Database\Seeders;
use App\Models\Country;
use App\Models\City;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CountryCitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Country::create(['name' => 'Suriname']);
        Country::create(['name' => 'Nederland']);
        Country::create(['name' => 'United States of America']);
        Country::create(['name' => 'India']);

        $sr = Country::find(1);
        $sr->cities()->createMany([
            ['name' => 'Paramaribo',
            'shortname' => 'PBM'],
        ]);

        $nl = Country::find(2);
        $nl->cities()->createMany([
            ['name' => 'Amsterdam',
            'shortname' => 'AMS'],
        ]);

        $us = Country::find(3);
        $us->cities()->createMany([
            ['name' => 'New York',
            'shortname' => 'NYK'],
        ]);

        $in = Country::find(4);
        $in->cities()->createMany([
            ['name' => 'New Delhi',
            'shortname' => 'NDH'],
        ]);
    }
}
