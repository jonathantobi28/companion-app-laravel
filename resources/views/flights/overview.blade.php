@extends('layout')

@section('title', 'Flights')

@section('breadcrumbs')
    @parent
    <li class="breadcrumb-item active" aria-current="page">Flights</li>
@endsection

@section('content')
<h2 class="intro-y text-lg font-medium mt-10">
    Flights
</h2>
{{-- <div class="grid grid-cols-12 gap-6 mt-5 mb-10">
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
        <a href="{{ route('flights.create') }}" class="btn btn-primary shadow-md mr-2">New Flight</a>
    </div>
</div> --}}

<div class="grid grid-cols-12 gap-6 mt-5">
    <!-- BEGIN: Data List -->
    <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
        <table class="table table-report -mt-2">
            <thead>
                <tr>
                    <th class="whitespace-nowrap">Name</th>
                    <th class="whitespace-nowrap">Airline</th>
                    <th class="whitespace-nowrap">Departure</th>
                    <th class="whitespace-nowrap">Arrival</th>
                    <th class="text-center whitespace-nowrap">ACTIONS</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($flights as $flight)
                    <tr class="intro-x">
                        <td>
                            <span class="font-medium whitespace-nowrap">{{ $flight->app_user->name }}</span> 
                        </td>
                        <td>
                            <div>
                                <div class="font-medium">{{ $flight->airline->name }}</div>
                                <div class="text-slate-500">{{ $flight->type}}</div>
                            </div>   
                        </td>
                        <td>
                            <div>
                                <div class="font-medium">{{ date('j M Y', strtotime($flight->departure_date)) }}</div>
                                <div class="text-slate-500">{{ date('H:i A', strtotime($flight->departure_time)) }}</div>
                                <small class="text-slate-500">{{ $flight->departure_city->name}}</small>
                            </div>
                        </td>
                        <td>
                            <div>
                                <div class="font-medium">{{ date('j M Y', strtotime($flight->arrival_date)) }}</div>
                                <div class="text-slate-500">{{ date('H:i A', strtotime($flight->arrival_time)) }}</div>
                                <small class="text-slate-500">{{ $flight->arrival_city->name}}</small>
                            </div> 
                        </td>
                        <td class="table-report__action w-56">
                            <div class="flex justify-center items-center">
                                <a class="flex items-center mr-3" href="{{ route('flights.show', $flight->id) }}"> 
                                    <i data-lucide="eye" class="w-4 h-4 mr-1"></i> View 
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<!-- END: Content -->

@endsection