<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\TemporaryFile;
use Illuminate\Support\Facades\Storage;

class CleanTemporaryFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:clean-temporary-files';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete temporary files older than 10 days';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        //
        $tenDaysAgo = now()->subDays(10);

        // Query the database for files older than 10 days
        $filesToDelete = TemporaryFile::where('created_at', '<', $tenDaysAgo)->get();

        // Delete the corresponding files from the storage
        foreach ($filesToDelete as $file) {
            Storage::delete($file->filename);
            $file->delete();
        }

        $this->info('Temporary files older than 10 days have been deleted.');
    }
}
