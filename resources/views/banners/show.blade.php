@extends('layout')

@section('title', $banner->title )

@section('breadcrumbs')
    @parent
    <li class="breadcrumb-item"><a href="{{route('banners.overview')}}">Banners</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ $banner->title }}</li>
@endsection

@section('content')
<div class="grid grid-cols-12 gap-6 mt-10 mb-5">
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
        <h2 class="intro-y text-lg font-medium">
            {{ $banner->title }}
        </h2>
        <a href="{{ route('banners.edit', $banner->id) }}" class="btn btn-primary shadow-md ml-auto">Edit</a>
    </div>                      
</div>

<div class="container mx-auto">
    <div class="bg-white rounded-lg shadow-lg p-10">
       
        <div class="container mx-auto flex flex-row justify-between">    
            <div>
                @if ($banner->is_active)
                    <div class="flex items-center justify-center text-success"> 
                        <i data-lucide="check-square" class="w-4 h-4 mr-2"></i> Active 
                    </div>
                @else
                    <div class="flex items-center justify-center text-danger"> 
                        <i data-lucide="check-square" class="w-4 h-4 mr-2"></i> Inactive 
                    </div>
                @endif
            </div>
            <div class="flex items-center justify-between">
                <div class="flex items-center">
                    <!-- eye icon -->
                  <div class="mr-2"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" icon-name="eye" data-lucide="eye" class="lucide lucide-eye icon" style="color: black;"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg></div>
                  <!-- View Rating -->
                  <div>{{ $banner->views }}</div>
                </div>
            </div>
        </div>

        <div class="mt-5">                                                                                                 
            <label for="title" class="block text-gray-700 font-normal font-bold">Title</label>
            <p>{{ $banner->title }}</p>
        </div>
        
        <div class="w-1/2 mt-5">
            <label for="image" class="block text-gray-700 font-bold">Banner Image</label>
            <!-- Banner Image -->
            <img src="{{ Storage::url($banner->image_path) }}" alt="{{ $banner->title }}" class="rounded-none">
        </div>
      
      <!-- Active Period Calendar -->
      <div class="mb-4 mt-5">
        <label for="active-period" class="block text-gray-700 font-bold">Active Period</label>
        <p>{{ date('j M, Y', strtotime($banner->active_from)) }} - {{ date('j M, Y', strtotime($banner->active_until)) }}</p>
      </div>
    </div>
</div>

<div class="mt-10">
    <button class="btn btn-danger mr-2 mb-2" data-tw-toggle="modal" data-tw-target="#deleteModal"> 
        <i data-lucide="trash-2" class="w-4 h-4 mr-2"></i> Delete Banner 
    </button>
</div>


  <!-- BEGIN: Delete Confirmation Modal -->
<div id="deleteModal" class="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body p-0">
                <div class="p-5 text-center">
                    <i data-lucide="x-circle" class="w-16 h-16 text-danger mx-auto mt-3"></i> 
                    <div class="text-3xl mt-5">Are you sure?</div>
                    <div class="text-slate-500 mt-2">
                        Are you sure you want to delete this banner?
                        <br>
                        This action cannot be undone
                    </div>
                </div>
                <div class="px-5 pb-8 text-center">
                    <button type="button" data-tw-dismiss="modal" class="btn btn-outline-secondary w-24 mr-1">Cancel</button>
                    <form method="post" action="{{ route('banners.destroy', $banner->id) }}" id="deleteForm" class="w-24 inline-flex">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Delete Confirmation Modal --> 

@endsection