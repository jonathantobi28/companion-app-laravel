<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="light">
    <!-- BEGIN: Head -->
    <head>
        <meta charset="utf-8">
    </head>
    <!-- END: Head -->

    <body style="background-color: #eeeeee;">
        <h1 style="text-align:center;padding-top:20px;padding-bottom:20px;color:#1d1d1d;font-size:16px">Multitravel Companion App</h1>
        
        <div style="background-color: #ffffff;padding: 40px;">
            @yield('content')
        </div>

        <p style="text-align: center;color: #808080;margin-top:20px"><small >This message was created by the Multiravel Companion System</small></p>
    </body>
</html>