@extends('layout')

@section('title', 'Appointment - ' . $appointment->appUser->name)

@section('breadcrumbs')
    @parent
    <li class="breadcrumb-item"><a href="{{route('appointments.overview')}}">Appointments</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ $appointment->appUser->name }}</li>
@endsection

@section('content')

<div class="grid grid-cols-12 gap-6 mt-5">
    <div class="col-span-12 lg:col-span-8 2xl:col-span-9">
        <div>
            <div class="intro-y flex items-center mt-5 mb-3">
                <h2 class="text-lg font-medium mr-auto">
                    Checklists
                </h2>
                <a href="#" class="ml-auto text-primary truncate flex items-center" data-tw-toggle="modal" data-tw-target="#checklistModal" >
                    <i data-lucide="plus" class="w-4 h-4 mr-1"></i> New Checklist
                </a>
            </div>
            @if ($checklists->isEmpty())
                <div class="p-5 box mb-3">
                    <p>No checklists added</p>
                </div>
            @else
                @foreach ($checklists as $checklist)
                    <a href="{{ route('checklists.edit', $checklist->id) }}" class="py-3 px-5 box flex flex-row justify-between zoom-in mb-3">
                        <div class="">
                            <div class="font-medium">{{ $checklist->name }}</div>
                        </div>
                    </a>
                @endforeach
            @endif
        </div>
        
    </div>

    <div class="col-span-12 lg:col-span-4 2xl:col-span-3">
        <div class="intro-ymt-5 mb-5 lg:mt-0">
            <div class="intro-y flex items-center mt-5 mb-3">
                <h2 class="text-lg font-medium mr-auto">
                    Personal information
                </h2>
            </div>
            <div class="p-5 box">
                <div class="relative flex items-center mb-5">
                    <div class="w-12 h-12 image-fit">
                        @if ($appointment->appUser->thumbnail_URI)
                            <img src="{{ asset( $appointment->appUser->thumbnail_URI) }}" class="rounded-full" alt="User Thumbnail">
                        @else
                            <img src="{{ asset('/dist/images/avatar.png') }}" class="rounded-full" alt="Thumbnail Placeholder">
                        @endif
                    </div>
                    <div class="ml-4 mr-auto">
                        <div class="font-medium text-base">{{ $appointment->appUser->name }}</div>
                        @if ($appointment->appUser->active)
                            <span class="text-success">Active</span>
                        @else
                            <span class="text-danger">Inactive</span>
                        @endif
                    </div>
                </div>
                <div>
                    <div class="font-medium text-center lg:text-left lg:mt-3 mb-1">Contact Details</div>
                    <p>{{ $appointment->appUser->email }}</p>
                </div>
            </div>
        </div>
        
        <div class="intro-ymt-5 lg:mt-0">
            <div class="intro-y flex items-center mt-5 mb-3">
                <h2 class="text-lg font-medium mr-auto">
                    Appointment information
                </h2>
            </div>
            <div class="p-5 box">
                <div class="flex justify-between mb-5">
                    <div>
                        <small class="text-slate-500">Location</small>
                        <div class="font-medium">{{ $appointment->location->name }}</div>
                    </div>
                    {{-- <div>
                        <a href="#" class="btn btn-sm btn-primary shadow-md">Edit</a>
                    </div> --}}
                </div>

                <div class="mb-2 flex justify-between">
                    <div class="text-left">
                        <small class="text-slate-500">Date - Time</small>
                        <div class="font-medium">{{ date('j M Y', strtotime($appointment->date)) }}</div>
                        <div class="text-slate-500">{{ date('H:i A', strtotime($appointment->time)) }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="checklistModal" class="modal modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <form class="modal-content" id="checklist-form" method="post" action="{{ route('checklists.store') }}" enctype="multipart/form-data">
            @csrf
            <!-- BEGIN: Modal Header -->
            <input type="hidden" name="checklistable_type" value="appointment">
            <input type="hidden" name="checklistable_id" value="{{ $appointment->id }}">

            <div class="modal-header">
                <h2 class="font-medium text-base mr-auto">New Checklist</h2> 
            </div> <!-- END: Modal Header -->
            <!-- BEGIN: Modal Body -->
            <div class="modal-body grid grid-cols-12 gap-4 gap-y-3">
                <div class="col-span-12"> 
                    <label for="type" class="form-label">Checklist Template</label> 
                    <select class="form-select form-control block mb-1" aria-label="Checlist Template Select" id="checklist-template-select" name="checklisttemplate_id">
                        @foreach ($checklisttemplates as $checklisttemplate)
                            <option value="{{ $checklisttemplate->id }}">{{ $checklisttemplate->name }}</option>
                        @endforeach
                    </select>
                </div>
                
            </div> <!-- END: Modal Body -->
            <!-- BEGIN: Modal Footer -->
            <div class="modal-footer"> 
                <button type="button" data-tw-dismiss="modal" class="btn btn-outline-secondary w-20 mr-1">Cancel</button> 
                <button type="submit" class="btn btn-primary w-20">Save</button> 
            </div> <!-- END: Modal Footer -->
        </form>
    </div>
</div>

@endsection

@section('scripts')
    @parent
    <script>
        
    </script>
@endsection

