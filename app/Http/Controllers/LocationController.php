<?php

namespace App\Http\Controllers;

use App\Models\Location;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class LocationController extends Controller
{
    //
    public function store(Request $request)
    {

        // Validate the incoming request data
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|unique:locations',
            'address' => 'required|string',
            'phone' => 'required|string',
            'city' => 'required', Rule::exists('cities', 'id'),
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $data = $validator->validated();

        // Create the app user
        $location = Location::create([
            'name' => $data['name'],
            'address' => $data['address'],
            'phone' => $data['phone'],
            'city_id' => $data['city'],
        ]);

        // Return a response or redirect as needed
        if($request->response == 'json'){
            $locationWithTimezone = $location->toArray();
            $locationWithTimezone['timezone'] = $location->city->timezone;
            return response()->json($locationWithTimezone, 200);
        }
        
        return redirect()->route('locations.overview')->with('success', 'Location created successfully');
    }
}
