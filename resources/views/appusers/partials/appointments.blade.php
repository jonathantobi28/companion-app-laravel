@if ($appointments->isEmpty())
    <div class="p-5 box mb-3">
        <p>No upcoming appointments</p>
    </div>
@else
<div class="overflow-y-auto scrollbar-hidden" style="max-height:200px">
    @foreach ($appointments as $appointment)
        <div class="py-3 px-5 box flex flex-row justify-between mb-3" >
            <div>
                <div class="font-medium">{{ date('j M Y', strtotime($appointment->date)) }}</div>
                <div class="text-slate-500">{{ date('h:i A', strtotime($appointment->time)) }}</div>
            </div>
            <div>
                <p class="font-medium">{{ $appointment->location->name }}</p>
                <div class="text-slate-500">{{ $appointment->location->address }}</div>
            </div>
            <div class="flex justify-center items-center">
                <button class="flex items-center mr-3" data-tw-toggle="modal" data-tw-target="#editAppointmentModal" onclick="prepareAppointmentEdit(event, {{$appointment}}, 'upcoming')"> 
                    <i data-lucide="check-square" class="w-4 h-4 mr-1"></i> Edit 
                </button>
                <button class="flex items-center text-danger" data-tw-toggle="modal" data-tw-target="#deleteAppointmentModal" onclick="prepareAppointmentDeletion(event, {{$appointment->id}})"> 
                    <i data-lucide="trash-2" class="w-4 h-4 mr-1"></i> Delete 
                </button>
            </div>
        </div>
    @endforeach
</div>
@endif