@extends('layout')

@section('title', $appuser->name )

<?php
use Carbon\Carbon;
?>

@section('styles')
    @parent
    <style>
        .file-input-container{
            position: relative;
            width: 100%;
            border: 1px dashed #B1B3B6;
            display: flex;
            padding: 2em;
            align-items: flex-end;
            cursor: pointer;
            border-radius: 4px;
            height: 270px;
            justify-content: center;
            overflow: hidden;
            max-width: 220px;
        }
        .file-input-container.dragover{
            border-color: var(--primary);
        }
        .file-input-container .file-preview{
            position: absolute;
            width: 100%;
            height: 100%;
            z-index: 1;
            top: 0;
        }
        .file-input-container .file-preview .preview-item{
            width: 100%;
            height: 100%;
        }
        .file-input-container .file-preview img{
            width: 100%;
            height: 100%;
            object-fit: cover;
        }
        .file-input-container .file-preview .delete-button{
            position: absolute;
            top: 10px;
            right: 10px;
        }
        .file-input-container .file-preview .delete-button svg{
            stroke: #fefefe;
        }
    </style>
@endsection

@section('breadcrumbs')
    @parent
    <li class="breadcrumb-item"><a href="{{route('appusers.overview')}}">App Users</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ $appuser->name }}</li>
@endsection

@section('content')

<div class="grid grid-cols-12 gap-6 mt-5">
    <div class="col-span-12 lg:col-span-8 2xl:col-span-9">
        <div>
            <div class="intro-y mt-5 mb-10 lg:mt-0">
                <div class="intro-y flex items-center mt-5 mb-3">
                    <h2 class="text-lg font-medium mr-auto">
                        Appointments
                    </h2>
                    <a href="#" id="create-appointment" class="ml-auto text-primary truncate flex items-center" >
                        <i data-lucide="plus" class="w-4 h-4 mr-1"></i> New Appointment
                    </a>
                </div>
                <ul class="nav nav-link-tabs" role="tablist">
                    <li id="upcoming-appointments-tab" class="nav-item flex-1" role="presentation"> <button class="nav-link w-full py-2 active" data-tw-toggle="pill" data-tw-target="#upcoming-appointments-tab-content" type="button" role="tab" aria-controls="upcoming-appointments-tab-content" aria-selected="true">Upcoming </button> </li>

                    <li id="past-appointments-tab" class="nav-item flex-1" role="presentation"> <button class="nav-link w-full py-2" data-tw-toggle="pill" data-tw-target="#past-appointments-tab-content" type="button" role="tab" aria-controls="past-appointments-tab-content" aria-selected="false">Past</button> </li>
                </ul>
                <div class="tab-content mt-5">
                    <div id="upcoming-appointments-tab-content" class="tab-pane leading-relaxed active" role="tabpanel" aria-labelledby="upcoming-appointments-tab">
                        @if ($upcomingAppointments->isEmpty())
                            <div class="p-5 box mb-3">
                                <p>No upcoming appointments</p>
                            </div>
                        @else
                        <div class="overflow-y-auto scrollbar-hidden" style="max-height:200px">
                            @foreach ($upcomingAppointments as $appointment)
                            <a href="{{ route('appointments.show', $appointment->id) }}" class="py-3 px-5 box flex flex-row justify-between mb-3" >
                                    <div>
                                        <div class="font-medium">{{ date('j M Y', strtotime($appointment->date)) }}</div>
                                        <div class="text-slate-500">{{ date('h:i A', strtotime($appointment->time)) }}</div>
                                    </div>
                                    <div>
                                        <p class="font-medium">{{ $appointment->location->name }}</p>
                                        <div class="text-slate-500">{{ $appointment->location->address }}</div>
                                    </div>
                                    <div class="flex justify-center items-center">
                                        <button class="flex items-center mr-3 edit-appointment" data-appointment-id="{{$appointment->id}}"> 
                                            <i data-lucide="check-square" class="w-4 h-4 mr-1"></i> Edit 
                                        </button>
                                        <button class="flex items-center text-danger" data-tw-toggle="modal" data-tw-target="#deleteAppointmentModal" onclick="prepareAppointmentDeletion(event, {{$appointment->id}})"> 
                                            <i data-lucide="trash-2" class="w-4 h-4 mr-1"></i> Delete 
                                        </button>
                                    </div>
                                </a>
                            @endforeach
                        </div>
                        @endif
                    </div>

                    <div id="past-appointments-tab-content" class="tab-pane leading-relaxed" role="tabpanel" aria-labelledby="past-appointments-tab">
                        @if ($pastAppointments->isEmpty())
                        <div class="p-5 box mb-3">
                            <p>No past appointments</p>
                        </div>
                        @else
                        <div class="overflow-y-auto scrollbar-hidden" style="max-height:200px">
                            @foreach ($pastAppointments as $appointment)
                            <a href="{{ route('appointments.show', $appointment->id) }}" class="py-3 px-5 box flex flex-row justify-between mb-3" >
                                    <div>
                                        <div class="font-medium">{{ date('j M Y', strtotime($appointment->date)) }}</div>
                                        <div class="text-slate-500">{{ date('h:i A', strtotime($appointment->time)) }}</div>
                                    </div>
                                    <div>
                                        <p class="font-medium">{{ $appointment->location->name }}</p>
                                        <div class="text-slate-500">{{ $appointment->location->address }}</div>
                                    </div>
                                    <div class="flex justify-center items-center">
                                        <button class="flex items-center mr-3 edit-appointment" data-appointment-id="{{$appointment->id}}"> 
                                            <i data-lucide="check-square" class="w-4 h-4 mr-1"></i> Edit 
                                        </button>
                                        <button class="flex items-center text-danger" data-tw-toggle="modal" data-tw-target="#deleteAppointmentModal" onclick="prepareAppointmentDeletion(event, {{$appointment->id}})"> 
                                            <i data-lucide="trash-2" class="w-4 h-4 mr-1"></i> Delete 
                                        </button>
                                    </div>
                                </a>
                            @endforeach
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div>
            <div class="intro-y flex items-center mt-5 mb-3">
                <h2 class="text-lg font-medium mr-auto">
                    Flights
                </h2>
                <a href="#" class="ml-auto text-primary truncate flex items-center" id="create-flight">
                    <i data-lucide="plus" class="w-4 h-4 mr-1"></i> New Flight
                </a>
            </div>
            <ul class="nav nav-link-tabs" role="tablist">
                <li id="upcoming-flights-tab" class="nav-item flex-1" role="presentation"> <button class="nav-link w-full py-2 active" data-tw-toggle="pill" data-tw-target="#upcoming-flights-tab-content" type="button" role="tab" aria-controls="upcoming-flights-tab-content" aria-selected="true">Upcoming </button> </li>

                <li id="past-flights-tab" class="nav-item flex-1" role="presentation"> <button class="nav-link w-full py-2" data-tw-toggle="pill" data-tw-target="#past-flights-tab-content" type="button" role="tab" aria-controls="past-flights-tab-content" aria-selected="false">Past</button> </li>
            </ul>
            <div class="tab-content mt-5">
                <div id="upcoming-flights-tab-content" class="tab-pane leading-relaxed active" role="tabpanel" aria-labelledby="upcoming-flights-tab">
                    @if ($upcomingFlights->isEmpty())
                    <div class="p-5 box mb-3">
                        <p>No upcoming flights scheduled</p>
                    </div>
                    @else
                        <div class="overflow-y-auto scrollbar-hidden" style="max-height:300px">
                            @foreach ($upcomingFlights as $flight)
                                <a href="{{ route('flights.show', $flight->id) }}" class="py-3 px-5 box flex flex-row justify-between mb-3">
                                    <div>
                                        <small class="text-slate-500">Airline</small>
                                        <div class="font-medium">{{ $flight->airline->name }}</div>
                                        <div class="text-slate-500">{{ $flight->type}}</div>
                                    </div> 
            
                                    <div class="text-left">
                                        <small class="text-slate-500">Departure</small>
                                        <div class="font-medium">{{ date('j M Y', strtotime($flight->departure_date)) }}</div>
                                        <div class="text-slate-500">{{ date('H:i A', strtotime($flight->departure_time)) }}</div>
                                        <small class="text-slate-500">{{ $flight->departure_city->name}}</small>
                                    </div> 
            
                                    <div class="text-left">
                                        <small class="text-slate-500">Arrival</small>
                                        <div class="font-medium">{{ date('j M Y', strtotime($flight->arrival_date)) }}</div>
                                        <div class="text-slate-500">{{ date('H:i A', strtotime($flight->arrival_time)) }}</div>
                                        <small class="text-slate-500">{{ $flight->arrival_city->name}}</small>
                                    </div>
            
                                    <div class="flex justify-center items-center">
                                        <button class="flex items-center mr-3 edit-flight" data-flight-id="{{$flight->id}}"> 
                                            <i data-lucide="check-square" class="w-4 h-4 mr-1"></i> Edit
                                        </button>
                                        <button class="flex items-center text-danger" data-tw-toggle="modal" data-tw-target="#deleteFlightModal" onclick="prepareFlightDeletion(event, {{$flight->id}})"> 
                                            <i data-lucide="trash-2" class="w-4 h-4 mr-1"></i> Delete 
                                        </button>
                                    </div>
                                </a>
                            @endforeach
                        </div>
                    @endif
                </div>

                <div id="past-flights-tab-content" class="tab-pane leading-relaxed" role="tabpanel" aria-labelledby="past-flights-tab">
                    @if ($pastFlights->isEmpty())
                    <div class="p-5 box mb-3">
                        <p>No past flights scheduled</p>
                    </div>
                    @else
                        <div class="overflow-y-auto scrollbar-hidden" style="max-height:300px">
                            @foreach ($pastFlights as $flight)
                                <a href="{{ route('flights.show', $flight->id) }}" class="py-3 px-5 box flex flex-row justify-between mb-3">
                                    <div>
                                        <small class="text-slate-500">Airline</small>
                                        <div class="font-medium">{{ $flight->airline->name }}</div>
                                        <div class="text-slate-500">{{ $flight->type}}</div>
                                    </div> 
            
                                    <div class="text-left">
                                        <small class="text-slate-500">Departure</small>
                                        <div class="font-medium">{{ date('j M Y', strtotime($flight->departure_date)) }}</div>
                                        <div class="text-slate-500">{{ date('H:i A', strtotime($flight->departure_time)) }}</div>
                                        <small class="text-slate-500">{{ $flight->departure_city->name}}</small>
                                    </div> 
            
                                    <div class="text-left">
                                        <small class="text-slate-500">Arrival</small>
                                        <div class="font-medium">{{ date('j M Y', strtotime($flight->arrival_date)) }}</div>
                                        <div class="text-slate-500">{{ date('H:i A', strtotime($flight->arrival_time)) }}</div>
                                        <small class="text-slate-500">{{ $flight->arrival_city->name}}</small>
                                    </div>
            
                                    <div class="flex justify-center items-center">
                                        <button class="flex items-center mr-3 edit-flight" data-flight-id="{{$flight->id}}"> 
                                            <i data-lucide="check-square" class="w-4 h-4 mr-1"></i> Edit
                                        </button>
                                        <button class="flex items-center text-danger" data-tw-toggle="modal" data-tw-target="#deleteFlightModal" onclick="prepareFlightDeletion(event, {{$flight->id}})"> 
                                            <i data-lucide="trash-2" class="w-4 h-4 mr-1"></i> Delete 
                                        </button>
                                    </div>
                                </a>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
        </div>

        {{-- Checklists --}}
        {{-- <div>
            <div class="intro-y flex items-center mt-5 mb-3">
                <h2 class="text-lg font-medium mr-auto">
                    Checklists
                </h2>
                <a href="#" class="ml-auto text-primary truncate flex items-center" data-tw-toggle="modal" data-tw-target="#checklistModal" >
                    <i data-lucide="plus" class="w-4 h-4 mr-1"></i> New Checklist
                </a>
            </div>
            @if ($checklists->isEmpty())
                <div class="p-5 box mb-3">
                    <p>No checklists added</p>
                </div>
            @else
                @foreach ($checklists as $checklist)
                    <div class="py-3 px-5 box flex flex-row justify-between">
                        <div class="">
                            <div class="font-medium">{{ $checklist->name }}</div>
                        </div>

                        <div class="flex justify-center items-center">
                            <a class="flex items-center mr-3" href="{{ route('appusers.show', $appuser->id) }}"> 
                                <i data-lucide="eye" class="w-4 h-4 mr-1"></i> View 
                            </a>
                        </div>
                    </div>
                @endforeach
            @endif
        </div> --}}
        
    </div>

    <div class="col-span-12 lg:col-span-4 2xl:col-span-3">
        <div class="intro-ymt-5 lg:mt-0">
            <div class="intro-y flex items-center mt-5 mb-3">
                <h2 class="text-lg font-medium mr-auto">
                    Personal information
                </h2>
            </div>
            <div class="p-5 box">
                <div class="relative flex items-center mb-5">
                    <div class="w-12 h-12 image-fit">
                        @if (auth()->user()->thumbnail_URI)
                            <img src="{{ asset( auth()->user()->thumbnail_URI) }}" class="rounded-full" alt="User Thumbnail">
                        @else
                            <img src="{{ asset('/dist/images/avatar.png') }}" class="rounded-full" alt="Thumbnail Placeholder">
                        @endif
                    </div>
                    <div class="ml-4 mr-auto">
                        <div class="font-medium text-base">{{ $appuser->name }}</div>
                        @if ($appuser->active)
                            <span class="text-success">Active</span>
                        @else
                            <span class="text-danger">Inactive</span>
                        @endif
                    </div>
                </div>
                <div>
                    <div class="font-medium text-center lg:text-left lg:mt-3 mb-1">Contact Details</div>
                    <p>{{ $appuser->email }}</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="mt-10">
    @if ($appuser->active)
        <button class="btn btn-danger mr-2 mb-2" data-tw-toggle="modal" data-tw-target="#deactivateModal"> 
            <i data-lucide="user-x" class="w-4 h-4 mr-2"></i> Deactivate User 
        </button>
    @else
        <button class="btn btn-success text-white mr-2 mb-2" data-tw-toggle="modal" data-tw-target="#activateModal"> 
            <i data-lucide="user-check" class="w-4 h-4 mr-2"></i> Activate User 
        </button>
    @endif
</div>


  <!-- BEGIN: Deactivate Confirmation Modal -->
<div id="deactivateModal" class="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body p-0">
                <div class="p-5 text-center">
                    <i data-lucide="x-circle" class="w-16 h-16 text-danger mx-auto mt-3"></i> 
                    <div class="text-3xl mt-5">Are you sure?</div>
                    <div class="text-slate-500 mt-2">
                        Are you sure you want to deactivate this user?
                    </div>
                </div>
                <div class="px-5 pb-8 text-center">
                    <button type="button" data-tw-dismiss="modal" class="btn btn-outline-secondary w-24 mr-1">Cancel</button>
                    <form method="post" action="{{ route('appusers.deactivate', $appuser->id) }}" id="deactivateForm" class="w-24 inline-flex">
                        @csrf
                        @method('PATCH')
                        <button type="submit" class="btn btn-danger">Deactivate</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Deactivate Confirmation Modal --> 


  <!-- BEGIN: Activate Confirmation Modal -->
  <div id="activateModal" class="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body p-0">
                <div class="p-5 text-center">
                    <i data-lucide="check-circle" class="w-16 h-16 text-success mx-auto mt-3"></i> 
                    <div class="text-3xl mt-5">Are you sure?</div>
                    <div class="text-slate-500 mt-2">
                        Are you sure you want to activate this user?
                    </div>
                </div>
                <div class="px-5 pb-8 text-center">
                    <button type="button" data-tw-dismiss="modal" class="btn btn-outline-secondary w-24 mr-1">Cancel</button>
                    <form method="post" action="{{ route('appusers.activate', $appuser->id) }}" id="activateForm" class="w-24 inline-flex">
                        @csrf
                        @method('PATCH')
                        <button type="submit" class="btn btn-success text-white">Activate</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Activation Confirmation Modal --> 

  <!-- BEGIN: Appointment Modal -->
  <div id="appointment-modal" class="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <form class="modal-content" id="appointment-form" method="post" action="{{ route('appointments.store') }}" enctype="multipart/form-data">
            @csrf
            <!-- BEGIN: Modal Header -->
            <input type="hidden" name="app_user" value="{{ $appuser->id }}">

            <div class="modal-header">
                <h2 class="font-medium text-base mr-auto">New Appointment</h2> 
            </div> <!-- END: Modal Header -->
            <!-- BEGIN: Modal Body -->
            <div class="modal-body grid grid-cols-12 gap-4 gap-y-3">
                <div class="col-span-12"> 
                    <label for="location" class="form-label">Location</label> 
                    <select class="form-select form-control block mb-1" aria-label="Location select" id="location-select" name="location" required>
                        @foreach ($locations as $location)
                            <option value="{{ $location->id }}" data-timezone="{{ $location->city->timezone }}" >{{ $location->name}}</option>
                        @endforeach
                    </select>  
                    <a href="javascript:;" data-tw-toggle="modal" data-tw-target="#location-modal" class="text-secondary underline">Add location</a>
                </div>
                <div class="col-span-12"> 
                    <label for="date" class="form-label">Date</label> 
                    <input type="text" class="form-control block" data-single-mode="true" name="date" required autocomplete="off" readonly>  
                </div>
                <div class="col-span-12"> 
                    <label for="date" class="form-label">Time</label> 
                    <div class="relative timepicker" id="timepicker-appointment-add">
                        <input type="text" class="form-control block rounded border bg-transparent px-3 py-[0.32rem] leading-[1.6] outline-none [&:not([data-te-input-placeholder-active])]:placeholder:opacity-0" id="timepicker-appointment-add-input" name="time" required autocomplete="off" readonly data-te-toggle='appointmentAddDatePicker'/>
                    </div>
                </div>
                <div class="col-span-12">
                    <?php
                        $timezone = $locations[0]->city->timezone;
                        $offsetInSeconds = Carbon::now($timezone)->getOffset();

                        $offsetInHours = $offsetInSeconds / 3600;

                        $formattedOffset = $offsetInHours < 0 ? 'GMT' . $offsetInHours : 'GMT+' . $offsetInHours;
                    ?>
                    <small class="text-slate-300 text-small" id="new-appointment-timezone">Times in {{ $timezone }} ({{ $formattedOffset }})</small>
                </div>
            </div> <!-- END: Modal Body -->
            <!-- BEGIN: Modal Footer -->
            <div class="modal-footer"> 
                <button type="button" data-tw-dismiss="modal" class="btn btn-outline-secondary w-20 mr-1">Cancel</button> 
                <button type="submit" class="btn btn-primary w-20">Save</button> 
            </div> <!-- END: Modal Footer -->
        </form>
    </div>
</div>
<!-- END: Appointment Modal -->

<div id="location-modal" class="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{ route('locations.store') }}" enctype="multipart/form-data" method="post" id="location-form">
                @csrf
                <div class="modal-header">
                    <h2 class="font-medium text-base mr-auto">New Location</h2> 
                </div>
                <div class="modal-body grid grid-cols-12 gap-4 gap-y-3">
                    <input type="hidden" name="response" value="json">

                    <div class="col-span-12"> 
                        <label for="city" class="form-label">City</label> 
                        <select class="form-select form-control block mb-1" aria-label="City select" id="city-select" name="city" required>
                            @foreach ($cities as $city)
                                <option value="{{ $city->id }}">{{ $city->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-span-12"> 
                        <label for="name" class="form-label">Name</label> 
                        <input type="text" class="form-control block" data-single-mode="true" name="name" placeholder="Embassy of Suriname" required value="{{ old('name') }}" autocomplete="off">
                        <span class="text-danger hidden" id="location-name-error"></span>
                    </div>

                    <div class="col-span-12"> 
                        <label for="address" class="form-label">Address</label> 
                        <input type="text" class="form-control block" data-single-mode="true" name="address" placeholder="Kleine Water St 44" required value="{{ old('address') }}" autocomplete="off">
                    </div>

                    <div class="col-span-12"> 
                        <label for="phone" class="form-label">Phone</label> 
                        <input type="tel" class="form-control block" data-single-mode="true" name="phone" placeholder="+597 481-223" required value="{{ old('phone') }}" autocomplete="off">
                    </div>
                </div>
                <div class="modal-footer"> 
                    <button type="button" data-tw-dismiss="modal" class="btn btn-outline-secondary w-20 mr-1">Cancel</button> 
                    <button type="submit" class="btn btn-primary w-20">Save</button> 
                </div>
            </form>
            
        </div>
    </div>
</div>

<div id="flight-modal" class="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <form class="modal-content" id="flight-form" method="post" action="" enctype="multipart/form-data">
            @csrf
            <!-- BEGIN: Modal Header -->
            <input type="hidden" name="app_user" value="{{ $appuser->id }}">

            <div class="modal-header">
                <h2 class="font-medium text-base mr-auto">New Flight</h2> 
            </div> <!-- END: Modal Header -->
            <!-- BEGIN: Modal Body -->
            <div class="modal-body grid grid-cols-12 gap-4 gap-y-3">
                <div class="col-span-12"> 
                    <label for="flight_type" class="form-label">Flight Type</label> 
                    <select class="form-select form-control block mb-1" aria-label="Flight Type select" id="flight-type-select" name="flight_type" required>
                        <option value="one-way">One Way</option>
                        <option value="round-trip">Round Trip</option>
                    </select>
                </div>

                <div class="col-span-12"> 
                    <label for="airline" class="form-label">Airline</label> 
                    <select class="form-select form-control block mb-1" aria-label="Airline select" id="airline-select" name="airline" required>
                        @foreach ($airlines as $airline)
                            <option value="{{ $airline->id }}">{{ $airline->name }}</option>
                        @endforeach
                    </select>
                </div>
                
                <div class="col-span-12" id="departing-flight-fields">
                    <p class="font-medium mt-2 mb-3">Departing Flight</p>
                    <div class="grid grid-cols-12 gap-4 gap-y-3">
                        <div class="col-span-6"> 
                            <div class="col-span-12 mb-3">
                                <label for="departure_city" class="form-label">From</label>
                                <select name="departure_city" id="departure_city" data-placeholder="Select Departing City" class="form-select form-control block mb-1" required>
                                    @foreach ($countries as $country)
                                        <optgroup label="{{ $country->name }}">
                                            @foreach ($country->cities as $city)
                                                <option value="{{ $city->id }}" data-timezone="{{ $city->timezone }}">{{ $city->name }}</option>
                                            @endforeach
                                        </optgroup>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-span-12 mb-3"> 
                                <label for="departure_date" class="form-label">Departure Date</label> 
                                <input type="text" class="form-control block" data-single-mode="true" name="departure_date" required autocomplete="off" readonly>  
                            </div>

                            <div class="col-span-12">
                                <label for="departure_time" class="form-label">Departure Time</label> 
                                <div class="relative timepicker" id="departure_time">
                                    <input type="text" class="form-control block rounded border bg-transparent px-3 py-[0.32rem] leading-[1.6] outline-none [&:not([data-te-input-placeholder-active])]:placeholder:opacity-0" name="departure_time" required autocomplete="off" readonly data-te-toggle='departureTimePicker'/>
                                </div>
                            </div>

                            <div class="col-span-12">
                                <?php
                                    $departureTimezone = $countries[0]->cities[0]->timezone;
                                    $departureOffsetInSeconds = Carbon::now($timezone)->getOffset();
            
                                    $departureOffsetInHours = $departureOffsetInSeconds / 3600;
            
                                    $departureFormattedOffset = $departureOffsetInHours < 0 ? 'GMT' . $departureOffsetInHours : 'GMT+' . $departureOffsetInHours;
                                ?>
                                <small class="text-slate-300 text-small" id="departure-timezone">Times in {{ $departureTimezone }} ({{ $departureFormattedOffset }})</small>
                            </div>
                            
                        </div>

                        <div class="col-span-6"> 
                            <div class="col-span-12 mb-3">
                                <label for="arrival_city" class="form-label">To</label>
                                <select name="arrival_city" id="arrival_city" data-placeholder="Select Arrival City" class="form-select form-control block mb-1" required>
                                    @foreach ($countries as $country)
                                        <optgroup label="{{ $country->name }}">
                                            @foreach ($country->cities as $city)
                                                <option value="{{ $city->id }}" data-timezone="{{ $city->timezone }}">{{ $city->name }}</option>
                                            @endforeach
                                        </optgroup>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-span-12 mb-3"> 
                                <label for="arrival_date" class="form-label">Arrival Date</label> 
                                <input type="text" class="form-control block" data-single-mode="true" name="arrival_date" required autocomplete="off" readonly>  
                            </div>

                            <div class="col-span-12">
                                <label for="arrival_time" class="form-label">Arrival Time</label> 
                                <div class="relative timepicker" id="arrival_time">
                                    <input type="text" class="form-control block rounded border bg-transparent px-3 py-[0.32rem] leading-[1.6] outline-none [&:not([data-te-input-placeholder-active])]:placeholder:opacity-0" name="arrival_time" required autocomplete="off" readonly data-te-toggle='arrivalTimePicker'/>
                                </div>
                            </div>
                            <div class="col-span-12">
                                <small class="text-slate-300 text-small" id="arrival-timezone"></small>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-span-12 hidden" id="return-flight-fields">
                    <p class="font-medium mt-2 mb-3">Return Flight</p>
                    <div class="grid grid-cols-12 gap-4 gap-y-3">
                        <div class="col-span-6"> 
                            <div class="col-span-12 mb-3">
                                <label for="return_departure_city" class="form-label">From</label>
                                <select name="return_departure_city" id="return_departure_city" data-placeholder="Select Departing City" class="form-select form-control block mb-1" disabled>
                                    @foreach ($countries as $country)
                                        <optgroup label="{{ $country->name }}">
                                            @foreach ($country->cities as $city)
                                                <option value="{{ $city->id }}" data-timezone="{{ $city->timezone }}">{{ $city->name }}</option>
                                            @endforeach
                                        </optgroup>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-span-12 mb-3"> 
                                <label for="return_departure_date" class="form-label">Departure Date</label> 
                                <input type="text" class="form-control block" data-single-mode="true" name="return_departure_date" autocomplete="off" readonly>  
                            </div>

                            <div class="col-span-12">
                                <label for="return_departure_time" class="form-label">Departure Time</label> 
                                <div class="relative timepicker" id="return_departure_time">
                                    <input type="text" class="form-control block rounded border bg-transparent px-3 py-[0.32rem] leading-[1.6] outline-none [&:not([data-te-input-placeholder-active])]:placeholder:opacity-0" name="return_departure_time" autocomplete="off" readonly data-te-toggle='returnDepartureTimePicker'/>
                                </div>
                            </div>
                            <div class="col-span-12">
                                <small class="text-slate-300 text-small" id="return-departure-timezone"></small>
                            </div>
                        </div>

                        <div class="col-span-6"> 
                            <div class="col-span-12 mb-3">
                                <label for="return_arrival_city" class="form-label">To</label>
                                <select name="return_arrival_city" id="return_arrival_city" data-placeholder="Select Arrival City" class="form-select form-control block mb-1" disabled>
                                    @foreach ($countries as $country)
                                        <optgroup label="{{ $country->name }}">
                                            @foreach ($country->cities as $city)
                                                <option value="{{ $city->id }}" data-timezone="{{ $city->timezone }}">{{ $city->name }}</option>
                                            @endforeach
                                        </optgroup>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-span-12 mb-3"> 
                                <label for="return_arrival_date" class="form-label">Arrival Date</label> 
                                <input type="text" class="form-control block" data-single-mode="true" name="return_arrival_date" autocomplete="off" readonly>  
                            </div>

                            <div class="col-span-12">
                                <label for="return_arrival_time" class="form-label">Arrival Time</label> 
                                <div class="relative timepicker" id="return_arrival_time">
                                    <input type="text" class="form-control block rounded border bg-transparent px-3 py-[0.32rem] leading-[1.6] outline-none [&:not([data-te-input-placeholder-active])]:placeholder:opacity-0" name="return_arrival_time" autocomplete="off" readonly data-te-toggle='returnArrivalTimePicker'/>
                                </div>
                            </div>
                            <div class="col-span-12">
                                <?php
                                    $returnArrivalTimezone = $countries[0]->cities[0]->timezone;
                                    $returnArrivalOffsetInSeconds = Carbon::now($timezone)->getOffset();
            
                                    $returnArrivalOffsetInHours = $returnArrivalOffsetInSeconds / 3600;
            
                                    $returnArrivalFormattedOffset = $returnArrivalOffsetInHours < 0 ? 'GMT' . $returnArrivalOffsetInHours : 'GMT+' . $returnArrivalOffsetInHours;
                                ?>
                                <small class="text-slate-300 text-small" id="return-arrival-timezone">Times in {{ $returnArrivalTimezone }} ({{ $returnArrivalFormattedOffset }})</small>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-span-12">
                    <label for="ticket" class="form-label">Ticket</label> 
                    <input type="file" id="image-input" name="ticket" class="hidden" value="{{ old('ticket') }}" accept="image/*">
                    <div id="image-dropzone" class="file-input-container">
                        <div id="preview-container" class="file-preview"></div>
                        <span class="text-slate-500">Drop file here or click to upload</span>
                    </div>
                    @error('ticket')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                
            </div> <!-- END: Modal Body -->
            <!-- BEGIN: Modal Footer -->
            <div class="modal-footer"> 
                <button type="button" data-tw-dismiss="modal" class="btn btn-outline-secondary w-20 mr-1">Cancel</button> 
                <button type="submit" class="btn btn-primary w-20">Save</button> 
            </div> <!-- END: Modal Footer -->
        </form>
    </div>
</div>

<!-- BEGIN: Delete Confirmation Modal -->
<div id="deleteAppointmentModal" class="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body p-0">
                <div class="p-5 text-center">
                    <i data-lucide="x-circle" class="w-16 h-16 text-danger mx-auto mt-3"></i> 
                    <div class="text-3xl mt-5">Are you sure?</div>
                    <div class="text-slate-500 mt-2">
                        Are you sure you want to delete this appointment?
                        <br>
                        This action cannot be undone
                    </div>
                </div>
                <div class="px-5 pb-8 text-center">
                    <button type="button" data-tw-dismiss="modal" class="btn btn-outline-secondary w-24 mr-1">Cancel</button>
                    <form method="post" action="" id="deleteAppointmentForm" class="w-24 inline-flex">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Delete Confirmation Modal --> 

<!-- BEGIN: Delete Flight Confirmation Modal -->
<div id="deleteFlightModal" class="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body p-0">
                <div class="p-5 text-center">
                    <i data-lucide="x-circle" class="w-16 h-16 text-danger mx-auto mt-3"></i> 
                    <div class="text-3xl mt-5">Are you sure?</div>
                    <div class="text-slate-500 mt-2">
                        Are you sure you want to delete this flight?
                        <br>
                        This action cannot be undone
                    </div>
                </div>
                <div class="px-5 pb-8 text-center">
                    <button type="button" data-tw-dismiss="modal" class="btn btn-outline-secondary w-24 mr-1">Cancel</button>
                    <form method="post" action="" id="deleteFlightForm" class="w-24 inline-flex">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Delete Flight Confirmation Modal --> 


<div id="toastify-notification" class="toastify-content hidden flex"> 
    <div class="font-medium" id="inner-content"></div> 
</div> 

@endsection

@section('scripts')
    @parent
    <script>
        function formatDate(dateString) {
            const parts = dateString.split('-');
            const year = parseInt(parts[0]);
            const month = parseInt(parts[1]) - 1; // JavaScript months are 0-based
            const day = parseInt(parts[2]);

            const date = new Date(Date.UTC(year, month, day));

            const options = {
                day: 'numeric',
                month: 'short',
                year: 'numeric',
                timeZone: 'UTC', // Set the desired time zone here
            };
            
            const formatter = new Intl.DateTimeFormat('en-US', options);
            return formatter.format(date);
        }

        function convertTimeTo12HourFormat(timeString) {
            // Split the time string into hours and minutes
            const [hours, minutes] = timeString.split(':');

            // Convert hours to 12-hour format
            const formattedHours = hours % 12 || 12;

            // Determine if it's AM or PM
            const period = hours >= 12 ? 'PM' : 'AM';

            // Combine hours, minutes, and period
            const formattedTime = `${formattedHours}:${minutes} ${period}`;

            return formattedTime;
        }

        function prepareAppointmentDeletion(event, id){
            event.preventDefault();
            document.getElementById('deleteAppointmentForm').action =`appointments/${id}/delete`;
        }

        function prepareFlightDeletion(event, id){
            event.preventDefault();
            document.getElementById('deleteFlightForm').action =`flights/${id}/delete`;
        }

        function showToast(text){
            // console.log('showing toast');

            var toastifyNotifcation = document.getElementById('toastify-notification');
            toastifyNotifcation.querySelector('#inner-content').innerHTML = text;

            Toastify({ 
                node: $("#toastify-notification").clone().removeClass("hidden")[0], 
                duration: 3000, 
                newWindow: true, 
                close: true, 
                gravity: "bottom", 
                position: "left", 
                style: {
                    backgroundColor: "white", 
                },
                stopOnFocus: true, 
            }).showToast();
        }

        function disableOption(selectItem, dependedItem, influencedItem){
            if(selectItem.value == dependedItem.value){
                selectItem.selectedIndex = -1;
                influencedItem.selectedIndex = -1;
            }

            selectItem.options.forEach(option => {
                if (option.value === dependedItem.value) {
                    option.disabled = true;
                }else{
                    option.disabled = false;
                }
            });

            influencedItem.options.forEach(option => {
                if (option.value === dependedItem.value) {
                    option.disabled = true;
                }else{
                    option.disabled = false;
                }
            });                

            // update items in tomselect
            // selectItem.options.forEach(option => {
            //     var data = {
            //         text: option.text,
            //         value: option.value,
            //         disabled: false
            //     }
                
            //     // disable option to be disabled in tomselect
            //     if(dependedItem.value == option.value){
            //         data['disabled'] = true;

            //         // clear item if item that needs to be disabled is selected
            //         selectItem.tomselect.removeItem(option.value);
            //         selectItem.tomselect.setValue('');
            //         // influencedItem.tomselect.removeItem(option.value);
            //         // influencedItem.tomselect.setValue('');

            //         // clear inputs for field validations
            //         selectItem.selectedIndex = -1;
            //         // influencedItem.selectedIndex = -1;
            //     }

            //     selectItem.tomselect.updateOption(option.value, data);
            //     // influencedItem.tomselect.updateOption(option.value, data);
            // })
        }

        function getTime(timePickerInput){
            return timePickerInput.querySelector('[data-te-timepicker-input]').value;
        }

        function clearTime(timePickerInput){
            timePickerInput.querySelector('[data-te-timepicker-input]').value = '';
        }

        function setTime(timePickerInput, time){
            timePickerInput.querySelector('[data-te-timepicker-input]').value = time;
        }

        function prepareAppointmentEdit(event, appointment){
            // console.log('preparing appointment edit ...');
            event.preventDefault();

            const editAppointmentModalInstance = tailwind.Modal.getOrCreateInstance(document.querySelector("#editAppointmentModal"));;
            const editAppointmentModalEl = document.getElementById("editAppointmentModal");

            appointmentEditForm = document.getElementById('appointment-edit-form');
            appointmentEditForm.action = `appointments/${appointment.id}/edit`;

            appointmentEditForm.querySelector('select[name=location]').value = appointment.location_id;

            var dateInput = appointmentEditForm.querySelector('input[name=date]');

            var currentDate = new Date();

            const datePicker = new Litepicker({ 
                element: dateInput,
                autoApply: false,
                showWeekNumbers: true,
                singleMode: true,
                format: "D MMM, YYYY",
                minDate: currentDate.setDate(currentDate.getDate() + 3),
            });

            datePicker.setDate(formatDate(appointment.date));

            var editAppointmentTimeInput = appointmentEditForm.querySelector('#timepicker-appointment-edit');

            const editAppointmentTimePicker = new te.Timepicker(editAppointmentTimeInput, { 
                format12: true,
                defaultTime: convertTimeTo12HourFormat(appointment.time)
            });

            var submitButton = appointmentEditForm.querySelector('button[type=submit]');

            const handleSubmit = function(event){
                event.preventDefault();

                // Disable the submit button
                submitButton.disabled = true;

                var form = event.target; // Get the form element
                var formData = new FormData(form); // Create a new FormData object

                // Create a new XMLHttpRequest object
                var xhr = new XMLHttpRequest();
                xhr.open('POST', form.action); // Set the request method and URL

                // // Set up the onload and onerror event handlers
                xhr.onload = function() {
                    if (xhr.status === 200) {
                    // Handle a successful response
                    
                    var response = JSON.parse(xhr.responseText);
                    
                    if(response.success){

                        showToast(response.success);
                        
                        setTimeout(() => {
                            window.location.reload();
                        }, 2000);
                    }

                    submitButton.disabled = false;
                    editAppointmentModalInstance.hide();

                    } else {
                        // Handle an error response
                        var response = JSON.parse(xhr.responseText);
                        // console.log(response);

                        submitButton.disabled = false;
                    }
                };

                xhr.onerror = function() {
                    // Handle an error during the request
                    alert("something went wrong, please try again");
                    submitButton.disabled = false;
                    editAppointmentModalInstance.hide();
                };

                // // Send the request with the form data
                xhr.send(formData);

                //Avoids default form submit
                return false;
            }

            appointmentEditForm.addEventListener('submit', handleSubmit);

            editAppointmentModalEl.addEventListener('hide.tw.modal', function(e){
                datePicker.destroy();
                timePicker.dispose();
                appointmentEditForm.removeEventListener('submit', handleSubmit);
            });

        }

        document.addEventListener('DOMContentLoaded', function() {
            const csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
            var minimalDate = new Date();
            minimalDate.setDate(minimalDate.getDate() + 3);

            const locationForm = document.getElementById('location-form');
            const locationModal = tailwind.Modal.getOrCreateInstance(document.querySelector("#location-modal"));

            locationForm.addEventListener('submit', function(event) {
                event.preventDefault();

                const submitButton = locationForm.querySelector('button[type="submit"]');
                
                document.getElementById('location-name-error').classList.add('hidden');

                // Disable the submit button
                submitButton.disabled = true;

                var form = event.target; // Get the form element
                var formData = new FormData(form); // Create a new FormData object

                // Create a new XMLHttpRequest object
                var xhr = new XMLHttpRequest();
                xhr.open('POST', form.action); // Set the request method and URL

                // Set up the onload and onerror event handlers
                xhr.onload = function() {
                    if (xhr.status === 200) {
                    // Handle a successful response
                    var response = JSON.parse(xhr.responseText);
                    // console.log(response);
                    var locationSelect = document.getElementById('location-select');
                    var option = document.createElement("option");
                    option.text = response.name;
                    option.value = response.id;
                    option.selected = true;
                    option.setAttribute('data-timezone', response.timezone); 
                    locationSelect.appendChild(option);
                    locationModal.hide();

                    } else {
                        // Handle an error response
                        var response = JSON.parse(xhr.responseText);
                        // console.log(response);

                        if(response.name){
                            var locationNameError = document.getElementById('location-name-error');
                            locationNameError.innerHTML = response.name;
                            locationNameError.classList.remove("hidden");
                            submitButton.disabled = false;
                        }
                    }
                };

                xhr.onerror = function() {
                    // Handle an error during the request
                    alert("something went wrong, please try again");
                    submitButton.disabled = false;
                    locationModal.hide();
                };

                // Send the request with the form data
                xhr.send(formData);

                //Avoids default form submit
                return false;
            });

            // Appointment Settings 
            const createAppointmentButton = document.getElementById('create-appointment');

            var appointmentForm = document.querySelector('#appointment-form');
            const appointmentModal = tailwind.Modal.getOrCreateInstance(document.querySelector("#appointment-modal"));

            var appointmentFormState = 'create';

            var appointmentDateInput = appointmentForm.querySelector('input[name=date]');
            var appointmentLocationSelect = appointmentForm.querySelector('select#location-select');
            var appointmentTimezone = appointmentForm.querySelector('small#new-appointment-timezone');

            var appointmentDatePicker = new Litepicker({ 
                element: appointmentDateInput,
                autoApply: false,
                showWeekNumbers: true,
                singleMode: true,
                format: "D MMM, YYYY",
                minDate: minimalDate,
            });
            var appointmentTimeInput = document.querySelector("#timepicker-appointment-add");
            var appointmentTimePicker = new te.Timepicker(appointmentTimeInput, {format12: true});

            function setAppointmentTimezone(){
                const selectedOption = appointmentLocationSelect.options[appointmentLocationSelect.selectedIndex];
                const selectedTimezone = selectedOption.getAttribute('data-timezone');

                // Get the offset using the Intl API
                const formatter = new Intl.DateTimeFormat('en', { timeZone: selectedTimezone, timeZoneName: 'short'});
                const parts = formatter.formatToParts(new Date());
                // console.log(parts);
                const timeZonePart = parts.find(part => part.type === 'timeZoneName').value;
                const offset = timeZonePart.replace(/GMT/, 'UTC');
                
                //update the display
                appointmentTimezone.textContent = `Times in ${selectedTimezone} (${offset})`;
            }

            appointmentLocationSelect.addEventListener('change', setAppointmentTimezone);

            function prepareAppointmentCreation(){
                // console.log('reset appointment form');
                appointmentFormState = 'create';

                // reset values
                appointmentLocationSelect.value = '1';
                appointmentDatePicker.setOptions({
                    minDate: minimalDate
                });
                appointmentDatePicker.clearSelection();
                clearTime(appointmentTimeInput);
                setAppointmentTimezone();

                appointmentModal.show();
            }

            function prepareAppointmentForm(appointment){
                // console.log('preparing appointment edit ...');
                // console.log(appointment);
                appointmentFormState = 'edit';

                appointmentForm.action = `appointments/${appointment.id}/edit`;

                appointmentLocationSelect.value = appointment.location_id;
                appointmentDatePicker.setDate(formatDate(appointment.date));
                setTime(appointmentTimeInput, convertTimeTo12HourFormat(appointment.time));
                setAppointmentTimezone();

                appointmentModal.show();
            }

            function prepareAppointmentEdit(appointmentID){

                fetch(`appointments/${appointmentID}`)
                    .then( (response) => {
                        if(!response.ok){
                            alert('something went wrong, please refresh the page and try again');
                        }
                        return response.json();
                    })
                    .then( (data) => {
                        // console.log(data);
                        prepareAppointmentForm(data.appointment);
                    })
                    .catch( (error) => {
                        alert('something went wrong, please refresh the page and try again');
                        console.error('Error:', error);
                    });

                appointmentModal.show();
            }

            createAppointmentButton.addEventListener('click', prepareAppointmentCreation);

            const editAppointmentButtons = document.querySelectorAll('.edit-appointment');

            editAppointmentButtons.forEach((button) => {
                button.addEventListener('click', (event) => {
                    event.preventDefault();

                    const appointmentID = button.dataset.appointmentId;
                    // console.log(appointmentID);

                    prepareAppointmentEdit(appointmentID);
                })
            });

            appointmentForm.addEventListener('submit', function(event){
                event.preventDefault();

                const submitButton = appointmentForm.querySelector('button[type="submit"]');

                // Disable the submit button
                submitButton.disabled = true;

                var form = event.target; // Get the form element
                // console.log(form);

                if(appointmentFormState == 'create'){
                    form.action = 'appointments/create';
                }

                var formData = new FormData(form); // Create a new FormData object

                // for( const entry of formData.entries()){
                //     const [key, value] = entry;
                //     console.log(`Key: ${key}, Value: ${value}`);
                // }

                if(appointmentFormState == 'edit'){
                    formData.append('_method', 'PUT');
                }

                // Create a new XMLHttpRequest object
                var xhr = new XMLHttpRequest();
                xhr.open(form.method, form.action); // Set the request method and URL

                xhr.setRequestHeader('X-CSRF-TOKEN', csrfToken);

                // // Set up the onload and onerror event handlers
                xhr.onload = function() {
                    if (xhr.status === 200) {
                    // Handle a successful response
                    
                    var response = JSON.parse(xhr.responseText);
                    
                    if(response.success){
                        showToast(response.success);
                        
                        setTimeout(() => {
                            window.location.reload();
                        }, 2000);
                    }

                    submitButton.disabled = false;
                    appointmentModal.hide();

                    } else {
                        // Handle an error response
                        var response = JSON.parse(xhr.responseText);
                        console.log(response);

                        alert("something went wrong, please try again");
                        submitButton.disabled = false;
                    }
                };

                xhr.onerror = function() {
                    // Handle an error during the request
                    alert("something went wrong, please try again");
                    submitButton.disabled = false;
                    appointmentModal.hide();
                };

                // // Send the request with the form data
                xhr.send(formData);

                //Avoids default form submit
                return false;
            });

            // Flight Settings
            const createFlightButton = document.getElementById('create-flight');

            const flightForm = document.querySelector('#flight-form');
            const flightModal = tailwind.Modal.getOrCreateInstance(document.querySelector("#flight-modal"));

            var flightFormState = 'create';
            var flightTypeSelect = flightForm.querySelector('select[name=flight_type]');
            var airlineSelect = flightForm.querySelector('select[name=airline]');
            var returnFlightFields = flightForm.querySelector('#return-flight-fields');
            var departureCitySelect = flightForm.querySelector('select[name=departure_city]');
            var arrivalCitySelect = flightForm.querySelector('select[name=arrival_city]');
            var returnDepartureCitySelect = flightForm.querySelector('select[name=return_departure_city]');
            var returnArrivalCitySelect = flightForm.querySelector('select[name=return_arrival_city]');

            var departureDateInput = flightForm.querySelector('input[name=departure_date]');
            var arrivalDateInput = flightForm.querySelector('input[name=arrival_date]');
            var returnDepartureDateInput = flightForm.querySelector('input[name=return_departure_date]');
            var returnArrivalDateInput = flightForm.querySelector('input[name=return_arrival_date]');

            var departureTimeInput = document.querySelector("#departure_time");
            var arrivalTimeInput = document.querySelector("#arrival_time");
            var returnDepartureTimeInput = document.querySelector("#return_departure_time");
            var returnArrivalTimeInput = document.querySelector("#return_arrival_time");
            var departureDatePicker = new Litepicker({ 
                element: departureDateInput,
                autoApply: false,
                showWeekNumbers: true,
                singleMode: true,
                format: "D MMM, YYYY",
                minDate: minimalDate,
            });
            var arrivalDatePicker = new Litepicker({ 
                element: arrivalDateInput,
                autoApply: false,
                showWeekNumbers: true,
                singleMode: true,
                format: "D MMM, YYYY",
                minDate: minimalDate,
            });
            var returnDepartureDatePicker = new Litepicker({ 
                element: returnDepartureDateInput,
                autoApply: false,
                showWeekNumbers: true,
                singleMode: true,
                format: "D MMM, YYYY",
                minDate: minimalDate,
            });
            var returnArrivalDatePicker = new Litepicker({ 
                element: returnArrivalDateInput,
                autoApply: false,
                showWeekNumbers: true,
                singleMode: true,
                format: "D MMM, YYYY",
                minDate: minimalDate,
            });
            var departureTimePicker = new te.Timepicker(departureTimeInput, { format12: true });
            var arrivalTimePicker = new te.Timepicker(arrivalTimeInput, { format12: true });
            var returnDepartureTimePicker = new te.Timepicker(returnDepartureTimeInput, { format12: true});
            var returnArrivalTimePicker = new te.Timepicker(returnArrivalTimeInput, { format12: true });

            function setFlightTimezone(citySelect, timezoneDisplayID, dependantTimezoneDisplayID, clear = false){
                var timezoneDisplay = flightForm.querySelector(`small#${timezoneDisplayID}`);
                var dependantTimezoneDisplay = flightForm.querySelector(`small#${dependantTimezoneDisplayID}`);

                if(clear){
                    timezoneDisplay.textContent = ``;
                    dependantTimezoneDisplay.textContent = ``;

                    return;
                }

                const selectedOption = citySelect.options[citySelect.selectedIndex];
                const selectedTimezone = selectedOption.getAttribute('data-timezone');

                // Get the offset using the Intl API
                const formatter = new Intl.DateTimeFormat('en', { timeZone: selectedTimezone, timeZoneName: 'short'});
                const parts = formatter.formatToParts(new Date());
                console.log(parts);
                const timeZonePart = parts.find(part => part.type === 'timeZoneName').value;
                const offset = timeZonePart.replace(/GMT/, 'UTC');
                
                //update the displays
                timezoneDisplay.textContent = `Times in ${selectedTimezone} (${offset})`;
                dependantTimezoneDisplay.textContent = `Times in ${selectedTimezone} (${offset})`;
            }

            disableOption(arrivalCitySelect, departureCitySelect, returnDepartureCitySelect);

            departureCitySelect.addEventListener('change', () => {
                disableOption(arrivalCitySelect, departureCitySelect, returnDepartureCitySelect);
                returnArrivalCitySelect.value = departureCitySelect.value;
                setFlightTimezone(departureCitySelect, 'departure-timezone', 'return-arrival-timezone');
            });

            arrivalCitySelect.addEventListener('change', () => {
                returnDepartureCitySelect.value = arrivalCitySelect.value;
                setFlightTimezone(arrivalCitySelect, 'arrival-timezone', 'return-departure-timezone');
            });

            // set minimal date for arrival datepicker so user cannot select date earlier than departure date
            departureDatePicker.on('selected', (date) => {
                var selectedDate = date.dateInstance;

                arrivalDatePicker.setOptions({
                    minDate: selectedDate
                });

                // change date for arrival if departure is later
                if(arrivalDatePicker.getDate()){
                    const departureDate = new Date(selectedDate);
                    const arrivalDate = new Date(arrivalDatePicker.getDate().dateInstance);

                    if(departureDate > arrivalDate){
                        arrivalDatePicker.clearSelection();
                    }
                }

                // change date for return departure if departure is later
                if(returnDepartureDatePicker.getDate()){
                    const departureDate = new Date(selectedDate);
                    const returnDepartureDate = new Date(returnDepartureDatePicker.getDate().dateInstance);

                    if(departureDate > returnDepartureDate){
                        returnDepartureDatePicker.clearSelection();
                    }
                }

                // change date for return arrival if departure is later
                if(returnArrivalDatePicker.getDate()){
                    const departureDate = new Date(selectedDate);
                    const returnArrivalDate = new Date(returnArrivalDatePicker.getDate().dateInstance);

                    if(departureDate > returnArrivalDate){
                        returnArrivalDatePicker.clearSelection();
                    }
                }
                
            });

            // set minimal date for return datepickers so user cannot select date earlier than arrival date
            arrivalDatePicker.on('selected', (date) => {
                var selectedDate = date.dateInstance;
                selectedDate.setDate(selectedDate.getDate() + 1);

                returnDepartureDatePicker.setOptions({
                    minDate: selectedDate
                });

                returnArrivalDatePicker.setOptions({
                    minDate: selectedDate
                });

                // change date for return dates if arrival is later
                if(returnDepartureDatePicker.getDate()){
                    const arrivalDate = new Date(selectedDate);
                    const returnDepartureDate = new Date(returnDepartureDatePicker.getDate().dateInstance)

                    if(arrivalDate > returnDepartureDate){
                        returnDepartureDatePicker.clearSelection()
                    }
                }

                if(returnArrivalDatePicker.getDate()){
                    const arrivalDate = new Date(selectedDate);
                    const returnArrivalDate = new Date(returnArrivalDatePicker.getDate().dateInstance)

                    if(arrivalDate > returnArrivalDate){
                        returnArrivalDatePicker.clearSelection()
                    }
                }

                // check if departure date is selected
                if(departureDatePicker.getDate()){
                    if(departureDatePicker.getDate().toDateString() == arrivalDatePicker.getDate().toDateString()){
                        arrivalTimePicker.update({ minTime: getTime(departureTimeInput) }); 
                    }
                }
            });

            // set minimal date for return arrival datepicker so user cannot select date earlier than return departure date
            returnDepartureDatePicker.on('selected', (date) => {
                var selectedDate = date.dateInstance;

                returnArrivalDatePicker.setOptions({
                    minDate: selectedDate
                });

                if(returnArrivalDatePicker.getDate()){
                    const returnDepartureDate = new Date(selectedDate);
                    const returnArrivalDate = new Date(returnArrivalDatePicker.getDate().dateInstance)

                    if(returnDepartureDate > returnArrivalDate){
                        returnArrivalDatePicker.clearSelection()
                    }
                }
            });

            departureTimeInput.addEventListener("input.te.timepicker", (input) => {
                // if departure date and arrival date are the same change minTime to value of departure time

                // ignore if no departure date is selected
                if(!departureDatePicker.getDate()){
                    return false;
                }

                // ignore if no arrival date is selected
                if(!arrivalDatePicker.getDate()){
                    return false;
                }

                if(departureDatePicker.getDate().toDateString() == arrivalDatePicker.getDate().toDateString()){
                    arrivalTimePicker.update({ minTime: input.target.value }); 
                }
            });

            returnDepartureTimeInput.addEventListener("input.te.timepicker", (input) => {
                // if return departure date and return arrival date are the same change minTime to value of return departure time

                // ignore if no retur departure date is selected
                if(!returnDepartureDatePicker.getDate()){
                    return false;
                }

                // ignore if no return arrival date is selected
                if(!returnArrivalDatePicker.getDate()){
                    return false;
                }

                if(returnDepartureDatePicker.getDate().toDateString() == returnArrivalDatePicker.getDate().toDateString()){
                    returnArrivalTimePicker.update({ minTime: input.target.value });   
                }
            });

            const fileContainer = document.getElementById('image-dropzone');
            const fileInput = document.getElementById('image-input');
            const previewContainer = document.getElementById('preview-container');
            let temporaryFileUrl = null;
            let prevTempURL = null;

            function showReturnFields(){
                //set return departure fields required
                returnDepartureDateInput.setAttribute('required', true);
                returnDepartureTimeInput.setAttribute('required', true);

                //set return arrival fields required
                returnArrivalDateInput.setAttribute('required', true);
                returnArrivalTimeInput.setAttribute('required', true);

                // show return flight fields
                returnFlightFields.classList.remove('hidden');
            }

            function hideReturnFields(){
                //remove return departure fields required
                returnDepartureDateInput.removeAttribute('required');
                returnDepartureTimeInput.removeAttribute('required');

                //remove return arrival fields required
                returnArrivalDateInput.removeAttribute('required');
                returnArrivalTimeInput.removeAttribute('required');

                // hide  return flight fields
                returnFlightFields.classList.add('hidden');
            }

            // Function to toggle visibility of return flight fields
            function toggleReturnFlightFields() {
                flightTypeSelect.value === 'round-trip' ? showReturnFields() : hideReturnFields();
            }

            // Add event listener to flight type select
            flightTypeSelect.addEventListener('change', toggleReturnFlightFields);

            fileContainer.addEventListener('click', () => {
                fileInput.click();
            });

            fileContainer.addEventListener('dragenter', (e) => {
                e.preventDefault();
                fileContainer.classList.add('dragover');
            });

            fileContainer.addEventListener('dragover', (e) => {
                e.preventDefault();
            });

            fileContainer.addEventListener('dragleave', () => {
                fileContainer.classList.remove('dragover');
            });

            fileContainer.addEventListener('drop', (e) => {
                e.preventDefault();
                fileContainer.classList.remove('dragover');
                // Handle the dropped file
                const file = e.dataTransfer.files[0];
                handleFile(file);
            });

            fileInput.addEventListener('change', (e) => {
                e.preventDefault();
                // Handle the selected file
                const file = e.target.files[0];
                handleFile(file);
            });

            function handlePreview(fileURL, storageURL){
                // Display the preview using the temporary URL
                const filePreview = document.createElement('div');
                filePreview.classList.add('preview-item');
                filePreview.innerHTML = `
                    <img src="${storageURL}" class="preview-image">
                    <button class="delete-button"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="lucide lucide-trash-2"><path d="M3 6h18"/><path d="M19 6v14c0 1-1 2-2 2H7c-1 0-2-1-2-2V6"/><path d="M8 6V4c0-1 1-2 2-2h4c1 0 2 1 2 2v2"/><line x1="10" x2="10" y1="11" y2="17"/><line x1="14" x2="14" y1="11" y2="17"/></svg></button>
                `;
                previewContainer.innerHTML = '';
                previewContainer.appendChild(filePreview);

                const deleteButton = filePreview.querySelector('.delete-button');
                deleteButton.addEventListener('click', (e) => {
                    e.stopPropagation();
                    handleDelete(fileURL);

                    fileInput.value = '';
                    previewContainer.innerHTML = '';
                });
            }

            function handleFile(file) {
                handleDelete(temporaryFileUrl);

                // Perform desired actions with the file
                if (file) {
                    const reader = new FileReader();
                    reader.onload = (event) => {
                        // Create a new FileList object with the selected or dropped file
                        const fileList = new DataTransfer();
                        fileList.items.add(file);

                        // Set the newly created FileList object as the value of the file input
                        fileInput.files = fileList.files;
                    }
                    reader.readAsDataURL(file);

                    const formData = new FormData();
                    formData.append('file', file);

                    // Make an AJAX request to upload the file and get the temporary URL
                    fetch("{{route('tickets.temporary.store')}}", {
                        method: 'POST',
                        body: formData,
                        headers: {
                            'X-CSRF-TOKEN': csrfToken,
                        },
                    })
                    .then((response) => response.json())
                    .then((data) => {
                        // Store the temporary URL
                        temporaryFileUrl = data.url;

                        // Display the preview using the temporary URL
                        const filePreview = document.createElement('div');
                        filePreview.classList.add('preview-item');
                        filePreview.innerHTML = `
                            <img src="${temporaryFileUrl}" class="preview-image">
                            <button class="delete-button"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="lucide lucide-trash-2"><path d="M3 6h18"/><path d="M19 6v14c0 1-1 2-2 2H7c-1 0-2-1-2-2V6"/><path d="M8 6V4c0-1 1-2 2-2h4c1 0 2 1 2 2v2"/><line x1="10" x2="10" y1="11" y2="17"/><line x1="14" x2="14" y1="11" y2="17"/></svg></button>
                        `;
                        previewContainer.innerHTML = '';
                        previewContainer.appendChild(filePreview);

                        const deleteButton = filePreview.querySelector('.delete-button');
                        deleteButton.addEventListener('click', (e) => {
                            e.stopPropagation();
                            handleDelete(temporaryFileUrl);

                            // Remove the preview and reset the temporary URL to make it feel instant
                            temporaryFileUrl = null;
                            fileInput.value = '';
                            previewContainer.innerHTML = '';
                        });
                    })
                    .catch((error) => {
                        console.error('Error uploading file:', error);
                    });
                }
            }

            async function handleDelete(file){

                if(file){
                    try {
                        const response = await fetch("{{route('tickets.temporary.delete')}}", {
                            method: 'DELETE',
                            body: JSON.stringify({ url: file }),
                            headers: {
                            'Content-Type': 'application/json',
                            'X-CSRF-TOKEN': csrfToken,
                            },
                        });
                        
                    } catch (error) {
                        console.error('An error occurred while deleting the file:', error);  
                    }
                }    
            }

            function prepareFlightCreation(){
                // console.log('reset flight form');
                flightFormState = 'create';

                flightTypeSelect.value = 'one-way';
                airlineSelect.value = '1';

                // reset departure values
                departureCitySelect.value = '1';
                departureDatePicker.setOptions({
                    minDate: minimalDate
                });
                departureDatePicker.clearSelection();
                clearTime(departureTimeInput);
                
                // reset arrival values
                arrivalCitySelect.value = '';
                arrivalDatePicker.setOptions({
                    minDate: minimalDate
                });
                arrivalDatePicker.clearSelection();
                clearTime(arrivalTimeInput);

                // reset return departure values
                returnDepartureCitySelect.value = '';
                returnDepartureDatePicker.setOptions({
                    minDate: minimalDate
                });
                returnDepartureDatePicker.clearSelection();
                clearTime(returnDepartureTimeInput);

                // reset return arrival values
                returnArrivalCitySelect.value = '1';
                returnArrivalDatePicker.setOptions({
                    minDate: minimalDate
                });
                returnArrivalDatePicker.clearSelection();
                clearTime(returnArrivalTimeInput);

                setFlightTimezone(departureCitySelect, 'departure-timezone', 'return-arrival-timezone');
                setFlightTimezone(arrivalCitySelect, 'arrival-timezone', 'return-departure-timezone', true);

                // hide return fields
                hideReturnFields();

                flightModal.show();
            }

            function prepareFlightForm(flight){
                // console.log('preparing flight edit ...');
                // console.log(flight);
                flightFormState = 'edit';

                flightForm.action = `flights/${flight.id}/edit`;

                flightTypeSelect.value = flight.type;
                airlineSelect.value = flight.airline_id;

                if(flight.type == 'one-way'){
                    hideReturnFields();
                }

                // set departure city value and disable option in other fields, set return arrival field also
                departureCitySelect.value = flight.departure_city_id;
                disableOption(arrivalCitySelect, departureCitySelect, returnArrivalCitySelect);
                returnArrivalCitySelect.value = flight.departure_city_id;
                setFlightTimezone(departureCitySelect, 'departure-timezone', 'return-arrival-timezone');

                // set departure date and set minimum date for other fields
                departureDatePicker.setDate(formatDate(flight.departure_date));
                arrivalDatePicker.setOptions({
                    minDate: formatDate(flight.departure_date)
                });

                // set departure time and set minimum time if departure and arrival date are the same
                setTime(departureTimeInput, convertTimeTo12HourFormat(flight.departure_time));

                if(flight.departure_date == flight.arrival_date){
                    arrivalTimePicker.update({ minTime: convertTimeTo12HourFormat(flight.departure_time) })
                }

                // set arrival city value set return departure field also
                arrivalCitySelect.value = flight.arrival_city_id;
                returnDepartureCitySelect.value = flight.arrival_city_id;
                setFlightTimezone(arrivalCitySelect, 'arrival-timezone', 'return-departure-timezone');

                // set arrival date and set minimum date for other fields
                arrivalDatePicker.setDate(formatDate(flight.arrival_date));
                returnDepartureDatePicker.setOptions({
                    minDate: formatDate(flight.arrival_date)
                });

                returnArrivalDatePicker.setOptions({
                    minDate: formatDate(flight.arrival_date)
                });

                // set arrival time
                setTime(arrivalTimeInput, convertTimeTo12HourFormat(flight.arrival_time));

                // set ticket image if available
                if(flight.ticket_image_path){
                    handlePreview(flight.ticket_image_path, flight.storage_ticket_image_path);
                }

                if(flight.type == 'round-trip'){
                    // set return departure date and set minimum date for other fields
                    returnDepartureDatePicker.setDate(formatDate(flight.return_departure_date));
                    returnArrivalDatePicker.setOptions({
                        minDate: formatDate(flight.departure_date)
                    });

                    // set return departure time and set minimum time if return departure and arrival date are the same
                    setTime(returnDepartureTimeInput, convertTimeTo12HourFormat(flight.return_departure_time));
                    if(flight.return_departure_date == flight.return_arrival_date){
                        returnArrivalTimePicker.update({ minTime: convertTimeTo12HourFormat(flight.return_departure_time) })
                    }

                    // set return arrival date and set minimum date for other fields
                    returnArrivalDatePicker.setDate(formatDate(flight.return_arrival_date));

                    // set return arrival time
                    setTime(returnArrivalTimeInput, convertTimeTo12HourFormat(flight.return_arrival_time));

                    showReturnFields();
                }

                flightModal.show();
            }

            function prepareFlightEdit(flightId){

                fetch(`flights/${flightId}`)
                    .then( (response) => {
                        if(!response.ok){
                            alert('something went wrong, please refresh the page and try again');
                        }
                        return response.json();
                    })
                    .then( (data) => {
                        // console.log(data);
                        prepareFlightForm(data.flight);
                    })
                    .catch( (error) => {
                        alert('something went wrong, please refresh the page and try again');
                        console.error('Error:', error);
                    });

                // flightModal.show();
            }

            createFlightButton.addEventListener('click', prepareFlightCreation);

            const editFlightButtons = document.querySelectorAll('.edit-flight');

            editFlightButtons.forEach((button) => {
                // console.log(button);

                button.addEventListener('click', (event) => {
                    event.preventDefault();

                    const flightID = button.dataset.flightId;
                    // console.log(flightID);

                    prepareFlightEdit(flightID);
                })
            });

            flightForm.addEventListener('submit', function(event){
                event.preventDefault();

                const submitButton = flightForm.querySelector('button[type="submit"]');

                // Disable the submit button
                submitButton.disabled = true;

                var form = event.target; // Get the form element
                console.log(form);

                if(flightFormState == 'create'){
                    form.action = 'flights/create';
                }

                // remove disabled attribute from selects so it is sent to the server
                returnDepartureCitySelect.disabled = false;
                returnArrivalCitySelect.disabled = false;

                var formData = new FormData(form); // Create a new FormData object

                // for( const entry of formData.entries()){
                //     const [key, value] = entry;
                //     console.log(`Key: ${key}, Value: ${value}`);
                // }

                if(flightFormState == 'edit'){
                    formData.append('_method', 'PUT');
                }

                // Create a new XMLHttpRequest object
                var xhr = new XMLHttpRequest();
                xhr.open(form.method, form.action); // Set the request method and URL

                xhr.setRequestHeader('X-CSRF-TOKEN', csrfToken);

                // // Set up the onload and onerror event handlers
                xhr.onload = function() {
                    if (xhr.status === 200) {
                    // Handle a successful response
                    
                    var response = JSON.parse(xhr.responseText);
                    
                    if(response.success){
                        showToast(response.success);
                        
                        setTimeout(() => {
                            window.location.reload();
                        }, 2000);
                    }

                    submitButton.disabled = false;
                    flightModal.hide();

                    } else {
                        // Handle an error response
                        var response = JSON.parse(xhr.responseText);
                        console.log(response);

                        alert("something went wrong, please try again");
                        submitButton.disabled = false;
                    }
                };

                xhr.onerror = function() {
                    // Handle an error during the request
                    alert("something went wrong, please try again");
                    submitButton.disabled = false;
                    flightModal.hide();
                };

                // // Send the request with the form data
                xhr.send(formData);

                //Avoids default form submit
                return false;
            });

        });
        
    </script>
@endsection

