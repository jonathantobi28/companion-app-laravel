<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="light">
    <!-- BEGIN: Head -->
    <head>
        <meta charset="utf-8">
        <link href="{{ asset('favicon.ico') }}" rel="shortcut icon">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Login - Backoffice Companion App</title>
        
        <!-- BEGIN: CSS Assets-->
        <link rel="stylesheet" href="{{asset('dist/css/app.min.css')}}" />
        <link rel="stylesheet" href="{{asset('dist/css/multitravel.css')}}" />
        <!-- END: CSS Assets-->
    </head>
    <!-- END: Head -->

    <body class="login">
        <div class="container sm:px-10">
            <div class="block xl:grid grid-cols-2 gap-4">
                <!-- BEGIN: Login Info -->
                <div class="hidden xl:flex flex-col min-h-screen">
                    <div class="flex flex-col justify-between h-full py-10" style="padding-bottom: 10rem">
                        <img class="-intro-x w-1" src="{{asset('/dist/images/logo_multitravel.png')}}" width="200">
                        <div class="-intro-x text-white leading-tight -mt-10 flex flex-col">
                            <span class="font-medium text-4xl">Backoffice</span>
                            <span class="font-medium text-sm">Manage (almost) everything about the Companion App</span>
                        </div>
                    </div>
                </div>
                <!-- END: Login Info -->
                <!-- BEGIN: Login Form -->
                <div class="h-screen xl:h-auto flex py-5 xl:py-0 my-10 xl:my-0">
                    <div class="my-auto mx-auto xl:ml-20 bg-white dark:bg-darkmode-600 xl:bg-transparent px-5 sm:px-8 py-8 xl:p-0 rounded-md shadow-md xl:shadow-none w-full sm:w-3/4 lg:w-2/4 xl:w-auto">
                        <h2 class="intro-x font-bold text-2xl xl:text-3xl text-center xl:text-left">
                            Sign In
                        </h2>
                        <form method="post" action="{{ route('login.execute') }}">
                            @csrf

                            <div class="intro-x mt-8">
                                <input type="email" name="email" id="email" class="intro-x login__input form-control py-3 px-4 block" placeholder="Email" required>
                                <input type="password" name="password" class="intro-x login__input form-control py-3 px-4 block mt-4" placeholder="Password">
                            </div>

                            <div class="intro-x flex text-slate-600 dark:text-slate-500 text-xs sm:text-sm mt-4">
                                <div class="flex items-center mr-auto">
                                    <input id="remember-me" type="checkbox" class="form-check-input border mr-2">
                                    <label class="cursor-pointer select-none" for="remember-me">Remember me</label>
                                </div>
                                <a href="">Forgot Password?</a> 
                            </div>

                            <div class="intro-x mt-5 xl:mt-8 text-center xl:text-left">
                                <button type="submit" class="btn btn-primary py-3 px-4 w-full xl:w-32 xl:mr-3 align-top">Login</button>
                            </div>
                            @if ($errors->has('email'))
                                <p class="text-danger mt-3">{{ $errors->first('email') }}</p>
                            @endif
                        </form>
                    </div>
                </div>
                <!-- END: Login Form -->
            </div>
        </div>
        
        <!-- BEGIN: JS Assets-->
        @section('scripts')
            <script src="{{asset('dist/js/app.min.js')}}"></script>
        @show
        <!-- END: JS Assets-->
    </body>
</html>