@extends('layout')

@section('title', 'Administrators')

@section('breadcrumbs')
    @parent
    <li class="breadcrumb-item active" aria-current="page">Administrators</li>
@endsection

@section('content')
<h2 class="intro-y text-lg font-medium mt-10">
    Administrators
</h2>
<div class="grid grid-cols-12 gap-6 mt-5">
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">
        {{-- <a href="#" class="btn btn-primary shadow-md mr-2">New Administrator</a> --}}
        {{-- <div class="hidden md:block mx-auto text-slate-500">Showing 1 to 10 of 5 entries</div> --}}
    </div>
    <!-- BEGIN: Data List -->
    <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
        <table class="table table-report -mt-2">
            <thead>
                <tr>
                    <th class="whitespace-nowrap">NAME</th>
                    <th class="whitespace-nowrap">EMAIL</th>
                    <th class="text-center whitespace-nowrap">STATUS</th>
                    {{-- <th class="text-center whitespace-nowrap">ACTIONS</th> --}}
                </tr>
            </thead>
            <tbody>
                @foreach ($administrators as $administrator)
                    <tr class="intro-x">
                        <td>
                            <span class="font-medium whitespace-nowrap">{{$administrator->name}}</span>   
                        </td>
                        <td>
                            <span class="font-medium whitespace-nowrap">{{$administrator->email}}</span>   
                        </td>
                        <td class="w-40">
                            @if ($administrator->active)
                                <div class="flex items-center justify-center text-success"> 
                                    Active 
                                </div>
                            @else
                                <div class="flex items-center justify-center text-danger"> 
                                    Inactive 
                                </div>
                            @endif
                        </td>
                        {{-- <td class="table-report__action w-56">
                            <div class="flex justify-center items-center">
                                <a class="flex items-center mr-3" href="administrators/edit/{{$administrator->id}}"> 
                                    <i data-lucide="check-square" class="w-4 h-4 mr-1"></i> Edit 
                                </a>
                                <button class="flex items-center text-danger" href="#"> 
                                    <i data-lucide="trash-2" class="w-4 h-4 mr-1"></i> Delete 
                                </button>
                            </div>
                        </td> --}}
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- END: Data List -->
    <!-- BEGIN: Pagination -->
    {{-- <div class="intro-y col-span-12 flex flex-wrap sm:flex-row sm:flex-nowrap items-center">
        <nav class="w-full sm:w-auto sm:mr-auto">
            <ul class="pagination">
                <li class="page-item">
                    <a class="page-link" href="#"> <i class="w-4 h-4" data-lucide="chevrons-left"></i> </a>
                </li>
                <li class="page-item">
                    <a class="page-link" href="#"> <i class="w-4 h-4" data-lucide="chevron-left"></i> </a>
                </li>
                <li class="page-item"> <a class="page-link" href="#">...</a> </li>
                <li class="page-item"> <a class="page-link" href="#">1</a> </li>
                <li class="page-item active"> <a class="page-link" href="#">2</a> </li>
                <li class="page-item"> <a class="page-link" href="#">3</a> </li>
                <li class="page-item"> <a class="page-link" href="#">...</a> </li>
                <li class="page-item">
                    <a class="page-link" href="#"> <i class="w-4 h-4" data-lucide="chevron-right"></i> </a>
                </li>
                <li class="page-item">
                    <a class="page-link" href="#"> <i class="w-4 h-4" data-lucide="chevrons-right"></i> </a>
                </li>
            </ul>
        </nav>
        <select class="w-20 form-select box mt-3 sm:mt-0">
            <option>10</option>
            <option>25</option>
            <option>35</option>
            <option>50</option>
        </select>
    </div> --}}
    <!-- END: Pagination -->
</div>
<!-- END: Content -->

<!-- BEGIN: Delete Confirmation Modal -->
<div id="deleteModal" class="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body p-0">
                <div class="p-5 text-center">
                    <i data-lucide="x-circle" class="w-16 h-16 text-danger mx-auto mt-3"></i> 
                    <div class="text-3xl mt-5">Are ?</div>
                    <div class="text-slate-500 mt-2">
                        Wilt u deze vacature echt verwijderen
                        <br>
                        Deze actie kunt u niet ongedaan maken
                    </div>
                </div>
                <div class="px-5 pb-8 text-center">
                    <button type="button" data-tw-dismiss="modal" class="btn btn-outline-secondary w-24 mr-1">Annuleer</button>
                    <form method="post" action="" id="deleteForm" class="w-24 inline-flex">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Verwijder</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END: Delete Confirmation Modal --> 
<script>
    function prepareDeletion(id){
        document.getElementById('deleteForm').action = 'vacatures/verwijder/'+ id;
    }
</script>
@endsection