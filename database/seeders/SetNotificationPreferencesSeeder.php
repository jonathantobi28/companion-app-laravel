<?php

namespace Database\Seeders;

use App\Models\AppUser;
use App\Models\NotificationPreference;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SetNotificationPreferencesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $users = AppUser::all();

        foreach ($users as $user) {
            $preferences = $user->notificationPreference;

            if (!$preferences) {
                $preferences = new NotificationPreference(['app_user_id' => $user->id]);
            }

            $preferences->pref_one_week = true;
            $preferences->pref_four_days = true;
            $preferences->pref_one_day = true;
            $preferences->pref_two_hours = true;
            $preferences->save();
        }
    }
}
