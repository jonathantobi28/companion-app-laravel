<?php

use App\Http\Controllers\AppointmentController;
use App\Http\Controllers\AppUserController;
use App\Http\Controllers\BannerController;
use App\Http\Controllers\ChecklistController;
use App\Http\Controllers\FlightController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\PushTokenController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', [AppUserController::class, 'login']);
Route::post('/register', [AppUserController::class, 'register']);
Route::post('/guest', [AppUserController::class, 'guest']);
Route::post('/password-reset/request-code', [AppUserController::class, 'sendResetCode']);
Route::post('/password-reset/verify', [AppUserController::class, 'verifyResetCode']);
Route::post('/password-reset', [AppUserController::class, 'resetPassword']);

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::get('banners/active', [BannerController::class, 'activeBanners']);

    Route::get('appointments/upcoming', [AppointmentController::class, 'upcomingAppointments']);
    Route::get('appointments/past', [AppointmentController::class, 'pastAppointments']);
    Route::get('appointments/requestdata', [AppointmentController::class, 'getAppointmentRequestData']);
    Route::post('appointments/request', [AppointmentController::class, 'requestAppointment']);
    Route::get('appointments/{id}', [AppointmentController::class, 'getAppointmentWithDetails']);
  
    Route::get('flights/upcoming', [FlightController::class, 'upcomingFlights']);
    Route::get('flights/past', [FlightController::class, 'pastFlights']);
    Route::get('flights/{id}', [FlightController::class, 'getFlightWithDetails']);

    Route::get('checklists/upcoming', [ChecklistController::class, 'upcomingChecklists']);
    Route::post('checklists/{id}', [ChecklistController::class, 'updateChecklistData']);

    Route::post('/registertoken', [PushTokenController::class, 'store']);
    Route::get('/preferences', [AppUserController::class, 'getPreferences']);
    Route::patch('/preferences', [AppUserController::class, 'updatePreferences']);

    Route::get('/notifications', [NotificationController::class, 'getNotifications']);
    Route::post('/notifications/read/{id}', [NotificationController::class, 'setNotificationRead']);
});