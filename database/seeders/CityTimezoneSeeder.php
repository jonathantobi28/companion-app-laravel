<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CityTimezoneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $cityTimezones = [
            ['name' => 'Paramaribo', 'timezone' => 'America/Paramaribo'],
            ['name' => 'Amsterdam', 'timezone' => 'Europe/Amsterdam'],
            ['name' => 'New York', 'timezone' => 'America/New_York'],
            ['name' => 'New Delhi', 'timezone' => 'Asia/Kolkata'],
        ];

        foreach ($cityTimezones as $cityTimezone) {
            DB::table('cities')->where('name', $cityTimezone['name'])->update(['timezone' => $cityTimezone['timezone']]);
        }
    }
}
