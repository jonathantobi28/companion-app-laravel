<?php

use Illuminate\Support\Facades\Log;

if (!function_exists('sendAndStoreNotification')) {
    function sendAndStoreNotification($user, $title, $body, $type) {

        Log::info('sendAndStoreNotification function called');

        // Store in database
        \App\Models\Notification::create([
            'app_user_id' => $user->id,
            'title' => $title,
            'body' => $body,
            'type' => $type
        ]);

        // Send push notification
        if (app()->environment('staging', 'production')) {
            \Illuminate\Support\Facades\Notification::send($user, new App\Notifications\PushNotification($title, $body));
        }
    }
}


?>