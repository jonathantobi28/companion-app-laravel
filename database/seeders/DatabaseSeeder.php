<?php

namespace Database\Seeders;

use App\Models\User;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        User::truncate();

        User::create([
            'name' => 'Multitravel',
            'username' => 'multitravel',
            'email' => 'it@multitravel.com',
            'password' => bcrypt('Traveling2023!'),
            'active' => 1
        ]);

        $this->call(AppUserSeeder::class);
        $this->call(LocationSeeder::class);
        $this->call(CountryCitySeeder::class);
        $this->call(AirlineSeeder::class);

    }
}
