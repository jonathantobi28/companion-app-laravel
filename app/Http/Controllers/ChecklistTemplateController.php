<?php

namespace App\Http\Controllers;

use App\Models\ChecklistTemplate;
use App\Models\City;
use App\Models\Location;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class ChecklistTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(){
        return view('checklisttemplates.overview', [
            'checklisttemplates' => ChecklistTemplate::paginate(20)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $locations = Location::all();
        $cities = City::all();

        return view('checklisttemplates.create', compact('cities', 'locations'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|string|max:255',
            'items' => 'required|string',
            'condition' => 'required|string|in:none,all_flights,specific_departure_city,all_appointments,specific_location',
            'condition_value' => 'nullable|numeric',
        ]);

        // Ensure condition_value is set if condition is not 'all_flights' or 'all_appointments'
        if (in_array($validatedData['condition'], ['specific_departure_city', 'specific_location']) && !$validatedData['condition_value']) {
            return response()->json(['message' => 'Condition value is required when condition is specific'], 422);
        }

        // Additional validation for condition_value based on the condition
        if ($request->condition === 'specific_departure_city' && !DB::table('cities')->find($request->condition_value)) {
            return response()->json(['message' => 'Invalid city ID provided'], 422);
        }

        if ($request->condition === 'specific_location' && !DB::table('locations')->find($request->condition_value)) {
            return response()->json(['message' => 'Invalid location ID provided'], 422);
        }

        ChecklistTemplate::create([
            'name' => $validatedData['name'],
            'items' => $validatedData['items'],
            'condition' => $validatedData['condition'],
            'condition_value' => $validatedData['condition_value'] ?? null,
        ]);

        return response()->json(['message' => 'Checklist template added successfully'], 200);
        
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $locations = Location::all();
        $cities = City::all();
        $checklisttemplate = ChecklistTemplate::findOrFail($id);

        $checklisttemplate->items = json_decode($checklisttemplate->items, true);
        return view('checklisttemplates.edit', compact('checklisttemplate', 'cities', 'locations'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required|string|max:255',
            'items' => 'required|string',
            'condition' => 'required|string|in:none,all_flights,specific_departure_city,all_appointments,specific_location',
            'condition_value' => 'nullable|numeric',
        ]);

        // Ensure condition_value is set if condition is not 'all_flights' or 'all_appointments'
        if (in_array($validatedData['condition'], ['specific_departure_city', 'specific_location']) && !$validatedData['condition_value']) {
            return response()->json(['message' => 'Condition value is required when condition is specific'], 422);
        }

        // Additional validation for condition_value based on the condition
        if ($request->condition === 'specific_departure_city' && !DB::table('cities')->find($request->condition_value)) {
            return response()->json(['message' => 'Invalid city ID provided'], 422);
        }

        if ($request->condition === 'specific_location' && !DB::table('locations')->find($request->condition_value)) {
            return response()->json(['message' => 'Invalid location ID provided'], 422);
        }

        $checklisttemplate = ChecklistTemplate::findOrFail($id);

        $checklisttemplate->name = $validatedData['name'];
        $checklisttemplate->items = $validatedData['items'];
        $checklisttemplate->condition = $validatedData['condition'];
        $checklisttemplate->condition_value = $validatedData['condition_value'] ?? null;
        $checklisttemplate->save();

        // Redirect back with a success message
        return response()->json(['message' => 'Checklist template added successfully'], 200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id){
        $checklisttemplate = ChecklistTemplate::findOrFail($id);

        $checklisttemplate->delete();

        return redirect()->route('checklisttemplates.overview')->with('success', 'Template deleted successfully');
    }
}
